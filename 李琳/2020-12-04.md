## 函数定义和调用

## 定义函数
+ function是函数定义。
+ juzi是一个函数名。
+ (i)里放函数参数,多个参数用,分隔。例:(i,a,o)
+ {...}之间的代码是函数体,可以放若干条语句,也可以不放语句。


```
function juzi(i){
    if(i>=0){
        return i;
    }else{
        return -i;
    }
}
console.log(juzi(20));//返回20
console.log(juzi(-30));//返回30 负负得正

```

+ return返回结果,如果没有return,则返回undefined。

```
function a (x){
    if(x>=0){
        
    }else{

    }
}
console.log(a(30));//返回undefined
```

+ 第二种定义方法：
function (x){...}是一个匿名函数,所以它赋值给了a,通过变量a调用该函数。
```
var a =function (x){
    if (x<=0){
        return x;
    }else{
        return -x;
    }
}
console.log(a(30)); //返回-30
```

## 调用函数

+ 调用并进行输出

```
console.log(a(30));
```

## arguments
+ arguments是一个类似Array但不是Array的一个对象。
+ argument包含length属性和索引元素之外没有任何Array属性。
+ argument是给函数传递参数的,并只在函数内部起作用。

```
function juzi(a) {
    console.log('a = '+a);
    //i表示索引从0开始
    for (var i=0;i<arguments.length;i++){
        console.log('arg '+i+' = '+arguments[i]);//返回
    }
}
console.log(juzi(2,4,6,8,10));
```

+ 函数不定义任何参数也可以获得参数值。

```
function juzi() {
    if (arguments.length === 0) {
        return 0;
    }
    var x = arguments[0];
    return x >= 0 ? x : -x;
}
console.log(juzi());//0
console.log(juzi(24));//24
console.log(juzi(-24));//24(负负得正)
```

## rest参数
+ rest参数只能写在最后，前面要用...标识。
+ 没有传入参数会直接接收一个空的Array,而不是undefied。
```
function juzi(a, b, ...rest) {
    console.log('a = ' + a);
    console.log('b = ' + b);
    console.log(rest);
}

juzi(10,20,30,33,50);

```
![avatar](./imgs/44.png)

## return语句
```
function juzi(){
    return {name:'juzi'};
}
console.log(juzi());//{ name: 'juzi' }
```
+ 把return语句拆成两行会导致输出结果为undefied。
```
function apple(){
    return
    {name:'apple'};
}
console.log(apple());//undefined
```

```
练习
定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14
```

```
'use strict';

function area_of_circle(r, pi) {
    var abs = 3.14;
    if(arguments.length>1){
        abs=pi;
    }
     return abs*r*r;
}
// 测试:
if (area_of_circle(2) === 12.56 && area_of_circle(2, 3.1416) === 12.5664) {
    console.log('测试通过');
} else {
    console.log('测试失败');
}//测试通过
```

```
小明是一个JavaScript新手，他写了一个max()函数，返回两个数中较大的那个。
```

```
'use strict';

function max(a, b) {
    if (a > b) {
        return  a;
    } else {
        return  b;
    }

}
console.log(max(15, 20));
```
## 变量作用域与解构赋值
+ 在JavaScript中，用var申明的变量实际上是有作用域的。
+ 不同函数内部的同名变量互相独立，互不影响。
+ JavaScript的函数在查找变量时从自身函数定义开始，从“内”向“外”查找。如果内部函数定义了与外部函数重名的变量，则内部函数的变量将“屏蔽”外部函数的变量。

## 变量提升
+ 在函数内部首先申明所有变量。