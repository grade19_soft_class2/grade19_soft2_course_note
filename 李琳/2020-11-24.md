# 这是一个屯膘的季节！！！

## null 和 undefined 的区别
+ 相同点：if判断语句中，两者都会被转换成false。

+ null和undefined两者相等，但不全等。
```
    console.log(null == undefined) //true
    console.log(null === undefined) //false
```
![avatar](./imgs/12.png)
+ 只var(声明变量)但并没有给其赋值，返回undefined。

+ 通常是未定义未赋值返回为undefined。

+ 关于undefined 的数值运算
```
    console.log(100+undefined)
    console.log(200-undefined)
    console.log(300*undefined)
    console.log(400/undefined)
```
返回NaN(Not a Number , 非数)
![avatar](./imgs/14.png)

+ null在数值运算中自动转换为0，例：
```
    console.log(100+null);
    console.log(200-null);
    console.log(300*null);
    console.log(null/400);//分母不为0
```
![avatar](./imgs/13.png)

## 变量名
+ 变量名可以用大小写英文、数字、$和_的组合，但不能用数字开头，也可以使用中文，但尽量不要用。

![avatar](./imgs/15.png)

## 对象
```
    var a={
        id:08,
        name:'橘子',
        age:19,
    };
    console.log(a.name);
    console.log(a);
```
![avatar](./imgs/11.png)