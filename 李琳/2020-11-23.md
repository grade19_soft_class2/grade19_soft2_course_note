# 瓦的天呐！！！ 今天学了Js！！！ 喜大普奔！！！

## JavaScript的亿点点小细节

+ JavaScript并不强制要在每个语句后加;但出于程序媛的优良习惯，还是要加上;比较妥当。

+ JavaScript是一种区分大小写的敏感语言，弄错大小写程序会报错或运行不正常。例:console只能是console不能是 Console,这是两个不同的变量名。

+ JavaScript中使用变量，函数名以及所有的标识符时，都必须采取一致的字符大小写形式。

+ JavaScript不区分整数和浮点数，统一用Number表示。

+ JavaScript不能使用int i = 1这样的形式，只能使用 var i = 1; 或 let i = 1; 。

## console.log()的使用方法
+ console.log( )方法用于在控制台输出信息。
```
var i = {id:'吃饭',name:'好饿'};
console.log(i);
```
![avatar](./imgs/09.png)

```
i=function(key){
            console.log(key);
        }
        i('今天中午吃炒白果!'); 
```
没吃到炒白果 吃了炒面 居然不卖炒白果！

![avatar](./imgs/10.png)

## JavaScript的注释  
### 快捷键:ctrl + / (多行单行注释)
```
    //    var i={
    //         'id':'吃饭',
    //         'name':'好饿'
    //     }
    //     console.log(i);
```
### 快捷键:shift + alt + a (块注释)
```
     /*   var i={
            'id':'吃饭',
            'name':'好饿'
        }
        console.log(i);
     */
```



    
