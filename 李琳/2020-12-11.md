## 高阶函数
+ JavaScript中的函数可以指向变量，变量可指向函数，函数中的参数可以接收变量，一个函数接收另一个函数作为参数，叫高阶函数。
```
例子:
```
```
function juzi(x,y,fn){
    return fn(x)+fn(y);//fn此时是一个函数
}
console.log(juzi(-90,90,Math.abs));
```
## map
+ 是一个高阶函数。
+ 用Array的map()方法获取一个新的Array。
```
例子:
```
```
 function juzi(i){
     return i + i;
 }
 var arr =[1,3,5,7,9];
 var result = arr.map(juzi);
 console.log(result); //返回 [2, 6, 10, 14, 18]
```
+ 用map()可把数字转为字符串。
```
例子:
```
```
 var arr1 = [1,2,3,4,5,6];
 console.log(arr1.map(String));//返回["1", "2", "3", "4", "5", "6"]
```

## reduce 
+ reduce()把结果和下一个元素进行累计计算。
```
例子:求和
```
```
 var arr2 = [2,4,6,8,10];
 var a = arr2.reduce(function(x,y){
     return x + y;
 });
console.log(a);//返回30
```
```
例子:求积
```
```
var  arr = [2,4,6,8,9];
 var a =arr.reduce(function (x,y){
     return x*10 + y;
 });
 console.log(a);//返回24689
```


## 练习 
+ 利用reduce求积。
```
function product(arr) {
  function num(x,y){
        return x*y;
    }
return arr.reduce(num);
}

// 测试：
if (product([1, 2, 3, 4]) === 24 && product([0, 1, 2]) === 0 && product([99, 88, 77, 66]) === 44274384) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');//返回测试成功！
}
```
+  不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数。
```
   function string2int(s){
    var arrStr=s.split('');
    var arrInt=arrStr.map(function (i){
    return +i;
    });
    return arrInt.reduce(function (x,y){
    return x*10+y;
    });
}
 
// 测试:
if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
    if (string2int.toString().indexOf('parseInt') !== -1) {
        alert('请勿使用parseInt()!');
    } else if (string2int.toString().indexOf('Number') !== -1) {
        alert('请勿使用Number()!');
    } else {
        alert('测试通过!');
    }
}
else {
    alert('测试失败!');
}
//返回测试通过！
```
+  请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。

```
function normalize(arr) {
    function name(word){
        var list = '';
        for(var i=0;i<word.length;i++){
            if(i===0){
              list +=word[i].toUpperCase();  
            } else {
                 list +=word[i].toLowerCase();
            }   
        }
    return list;
}
return arr.map(name);
}
console.log((normalize(['adam', 'LISA', 'barT'])));//返回['Adam', 'Lisa', 'Bart']
```
+ 小明希望利用map()把字符串变成整数。
```
方法一:
```
```
 var arr = ['1','2','3'];
 var r ;
 r = arr.map(Number);
 console.log(r);//返回[1,2,3]

//Number()把对象值转为数字。
```

```
方法二:
```
```
 var num = arr.map(function(x){
     return parseInt(x);
 });
 console.log(num);

//parseInt(string, radix)解析一个字符串并返回指定基数的十进制整数
```

## filter
+ 过滤掉Array的某些元素，然后返回剩下的元素。
+ filter()在于实现一个筛选函数。
```
例子：删除偶数只保留奇数。
```
```
 var arr = [2,23,4,56,34,56,3,5,88,66];
 var r = arr.filter(function(x){
     return x % 2 !== 0;
 });
 console.log(r);//返回[23, 3, 5]
```

## 回调函数
+ filter()接收的回调函数，可以有多个参数。回调函数还可以接收另外两个参数，表示元素的位置和数组本身。

```
 var arr = ['橘子','苹果','香蕉'];
 var r = arr.filter(function(element,index,self){
     console.log(element);
     console.log(index);
     console.log(self);
     return true;
 });
 console.log(r);
```
![avatar](./imgs/48.png)
+ 利用filter()去除Array中的重复元素。
```
例子：
```
```
 var arr = ['apple','peach','apple','orange','banana','peach'];
 var r = arr.filter(function(element,index,self){
     return self.indexOf(element)===index;
 });
 console.log(r.toString());//返回apple,peach,orange,banana
```
## 练习
+ 请尝试用filter()筛选出素数：

```
'use strict'

function get_primes(arr) {
    function isPrime(num){ //判断一个数是否是素数
            if(num==1){
                return false;//1不是素数,返回false
            } 
            else{
                for (var i = 2; i < num; i++) {
                    if (num%i==0){ 
                        return false; 
                    }
                }; 
                return true; 
            }  
        }
        var primes=arr.filter(isPrime);//通过自定义的isPrime函数来过滤出数组中的素数
    
        return primes;
    }
// 测试：
    var
    x,
    r,
    arr = [];
for (x = 1; x < 100; x++) {
    arr.push(x);
}
r = get_primes(arr);
if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
    console.log('测试通过!');
} else {
    console.log('测试失败: ' + r.toString());
}

```