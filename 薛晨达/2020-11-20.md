# List泛型集合

怎样创建泛型集合？

主要利用System.Collections.Generic命名空间下面的List<T>泛型类创建集合，语法如下：

    List<T> ListOfT = new List<T>();
其中的"T"就是所要使用的类型，既可以是简单类型，如string、int，也可以是用户自定义类型。

<br>

# List的基础、常用方法

(1)、声明：   
①、List<T> mList = new List<T>();  
T为列表中元素类型，现在以string类型作为例子

    List<string> mList = new List<string>();
②、List<T> testList =new List<T> (IEnumerable<T> collection);

以一个集合作为参数创建List：

    string[] temArr = { "Ha", "Hunter", "Tom", "Lily", "Jay", "Jim", "Kuku", "Locu" };
    List<string> testList = new List<string>(temArr);
(2)、添加元素:

①、 添加一个元素

　　语法： List. Add(T item)  

    List<string> mList = new List<string>();
    mList.Add("John");
②、 添加一组元素

　　语法： List. AddRange(IEnumerable<T> collection)   

    List<string> mList = new List<string>();
    string[] temArr = { "Ha","Hunter", "Tom", "Lily", "Jay", "Jim", "Kuku",  "Locu" };
    mList.AddRange(temArr);
③、在index位置添加一个元素

　　语法： Insert(int index, T item); 

    List<string> mList = new List<string>();
    mList.Insert(1, "Hei");
④、遍历List中元素

语法：

foreach (T element in mList)  //T的类型与mList声明时一样  
{  
    Console.WriteLine(element);  
}  
例：
```
List<string> mList = new List<string>();
...//省略部分代码
foreach (string s in mList)
{
    Console.WriteLine(s);
}
```
(3)、删除元素:

①、删除一个值

　　语法：List. Remove(T item)

    mList.Remove("Hunter");
②、 删除下标为index的元素

　　语法：List. RemoveAt(int index);   

    mList.RemoveAt(0);
③、 从下标index开始，删除count个元素

　　语法：List. RemoveRange(int index, int count);

    mList.RemoveRange(3, 2);
(4)、判断某个元素是否在该List中：

语法：List. Contains(T item)   返回值为：true/false

```
if (mList.Contains("Hunter"))
{
    Console.WriteLine("There is Hunter in the list");
}
else
{
    mList.Add("Hunter");
    Console.WriteLine("Add Hunter successfully.");
}
```
(5)、给List里面元素排序：

　　语法： List. Sort ()   默认是元素第一个字母按升序

    mList.Sort();