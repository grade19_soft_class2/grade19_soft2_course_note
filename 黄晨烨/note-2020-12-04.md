## 笔记
### 1.函数的定义和调用
 + 函数的定义
    ```
    1. 第一种定义函数方式
    函数定义 函数名称（参数，参数，参数）
    {
    函数体（多条语句）
    }

    ```
    ```
    2. 第二种定义函数方式
    var 变量名=函数定义 函数名称（参数，参数，参数）
    {

    }
    ```
+ 展示
    ```
    1.function abs(x) {
        if(x>0)
        {
            return x+"为正数";
        }
        else{
            return x+ "为负数";
        }
    }
    console.log(abs(4));//4为正数
    console.log(abs(-8));//-8为负数
    2.var subject=function major(x) {
        if(x=="语文")
        {
            return'优秀';
        }
        else
        {
            return '未知，请耐心等带老师评卷'
        }
    }
    console.log(subject("语文"));//优秀
    console.log(subject("英语"));//未知，请耐心等带老师评卷

    ```
+ 注意：在函数体内部的语句执行的时候，一旦执行到return时，函数就执行完毕，并将结果返回；如果没有return语句，函数执行完毕就会返回结果，返回的结果为undefined。
    ```
    function classRoom(str)
    {
        console.log("终于有人来陪我啦哈哈哈哈");
    }
    var one=(str)=>
    {
        console.log(str);
        var next="到达教室的孩子";
        console.log(next);

    }
    one("这是第一个");//这是第一个到达教室的孩子
    one(classRoom());//终于有人来陪我啦哈哈哈哈 undefined 到达教室的孩子

    ```
### 2.arguments
+ 定义：它只在函数内部起作用，并且永远指向当前函数的调用者传入的所有参数
+ 实际运用：
  + 利用argument获得调用者传入的所有参数，函数不定义任何参数也还是可以拿到参数的值；
    ```
    
    'user strict'
    function arr(x) {
        
        for(var i=0;i<arguments.length;i++)
        {
            console.log('arg ' + i + ' = ' + arguments[i]);
        }
    }
    arr(10,20,30)
    ```
  + 判断传入参数的个数
    ```

    function number(a,b,c) {
        if(arguments.length===2)
        {   
            c=b;
            b=null;
            
        }
        console.log(a);
        console.log(b);
        console.log(c)
    }

    number(10,50,20);
    ```
### 3.···rest参数（参数非常多的时候以数组的形式交给变量rest）
        ```
        function number(a,b,c,...rest) {
            if(arguments.length===2)
            {   
                c=b;
                b=null;
                
            }
            console.log(a);
            console.log(b);
            console.log(c)
            console.log(rest);
        }

        number(10,50,20,45,78,69.2,23,70,36);

        // 10
        // 50
        // 20
        // (6) [45, 78, 69.2, 23, 70, 36]

        ```
### 4.小心return语句
### 5.练习
```
1.定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14

function rea_of_circle(r,pi) {
    if(pi!=='number')
    {
        var pi=3.14;
        var s=pi*r*r;
    }
    else
    {
        var s=pi*r*r;
    }
  
    return s;
   
}
 console.log(rea_of_circle(4));//50.24
 console.log(rea_of_circle(4,3.1415926));//50.24


```
```
//  小明是一个JavaScript新手，他写了一个max()函数，返回两个数中较大的那个：
function max(a,b) {
    for(var i=0;i<arguments.length;i++)
    {
        if(arguments.length===2)
        {
            if(a>b)
            {
                return a;
            }
            else
            {
                return b;
            }
        }
    }
}
console.log(max(5,9));
```
