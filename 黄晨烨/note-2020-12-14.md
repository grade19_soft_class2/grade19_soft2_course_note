## 排序算法
+ 对数字，字母排序倒序，可以通过x和y进行比较，x>y，return -1;x < y,return 1;x=y,return 0。
 ```
  // 正序
    var number=[12,4,7,1,10,6,89];
    var result=number.sort(function(x,y)
    {
        if(x>y){
            return 1;
        }
        if(x<y){
            return -1;
        }
        if(x=y)
        {
            return 0;
        }
    })
    console.log(result);//[1, 4, 6, 7, 10, 12, 89]
 ```
 ```
 //  倒序
    var res=[12,15,63,6,8,90];
    var respond=res.sort(function(x,y)
    {
        if(x>y){
            return -1;
        }
        if(x<y)
        {
        return 1;
        }
        if(x=y)
        {
            return 0;
        }


    })
    console.log(respond);//[90, 63, 15, 12, 8, 6]
 ```
 ```
 //字母排序
    var fruit=['apple','pear','Orange','Watermealon'];
    var result=fruit.sort(function(s1,s2){
        x1=s1.toLowerCase();
        x2=s2.toLowerCase();
        if(x1>x2)
        {
            return 1;
        }
        if(x1<x2)
        {
            return -1;
        }
        if(x1=x2)
        {
            return 0;
        }
    }
    )
    console.log(result);
 ```
+ 注意：运用sort()对Array直接进行修改，它返回的结果仍然是当前的Array。(对象是一样的)
```
    var arr=[45,14,2,3];
    var result=arr.sort(function(x,y)
    {
        if(x>y)
        {
            return 1;
        }
        if(x<y)
        {
            return -1;
        }
        if(x=y)
        {
            return 0;
        }
    });
    var judge=arr===result;
    console.log(result);//[2, 3, 14, 45]
    console.log(judge);//true（arr和result结果是一样的）
```
### every
+ every()方法可以判断数组的所有元素是否满足测试条件。(返回结果为false或者true)
 ```
   1. 每个元素的长度是否都大于零
    var fruit=['apple','pear','watermealon'];

    console.log(
        fruit.every(function(s){
        return s.length>0

    })
    );//ture
    2.每个元素是否都为大写
    console.log(
        fruit.every(function (s)
        {
            return s.toUpperCase()===s;
        }) 
    );//false
     3.每个元素是否都为小写
    console.log(
        fruit.every(function(s){
            return s.toLowerCase()===s;
        })
    );//true
 ```
### find
+ find()方法用于查找符合条件的第一个元素，如果找到了，返回这个元素，否则，返回undefined：
```
    var fruit=['apple','PEAR','watermealon'];

    console.log(fruit.find(function(s){
        return s.toLocaleUpperCase()===s;
    }));//PEAR

    console.log(fruit.find(function (s){
    return s.toLowerCase()===s;
    })//apple
    );
```
## finIndex(索引)
+ findIndex()和find()类似，也是查找符合条件的第一个元素，不同之处在于findIndex()会返回这个元素的索引，如果没有找到，返回-1：
```
    var fruit=['apple','PEAR','watermealon'];
    console.log(fruit.findIndex(function (s){
    return s.toLowerCase()===s;
    })
    );//0
```
## froeach(常用于遍历数组，传入的参数不需要返回值)
```
  var fruit=['apple','PEAR','watermealon'];
  fruit.forEach(console.log);
```
