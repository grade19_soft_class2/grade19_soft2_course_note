## 1.箭头函数
+ 其实是函数关系式的简写，用=>代替function 函数名（）{}
```
function x(i){
    return i*i;
}
var res=x(3);
console.log(res);
```
```
'use strict'
var fn=x=>x*x;
console.log(fn(3));

  x=>{
      if(x>0){
          return x*x;
      }
      else{
          return -x*x;
      }
  }
  console.log(x(4));
```
+ 参数不是一个，就需要用括号（）括起来；返回一个对象，是单表达式的话，也要用（）括起来。
 + 无参数:
```
  var result=()=>30.14;
  console.log(result());
```
 + 有两个参数：
 ```
  var result=(x,y)=>x*10+y;
  console.log(result(4,12));
  ```
 + 可变参数参数：
 ```
   var result=(x,y,...rest)=>{
      var i,sum=x+y;
      for(var i=0;i<rest.length;i++){
          sum+=rest[i];
      }
      return sum;
  }
  console.log(result(2,5,9));
 ```
 + 返回一个对象
 ```
 x => ({ foo: x })
 ```
## 2.this(箭头函数会修复this的指向，this总是指向词法作用域，也就是外层调用者，就不需要像匿名函数那样来一个var that=this 指定方向)
+ 

```
 用call、apply调用箭头函数，无法绑定this,传入的第一个参数被忽略，我们可以重新传入一个新的参数
'use strict'
var animal={
    name:'汤圆',
    birth:2019,
    getAge:function(year){
        var b=this.birth;
        var fn=(y)=> y-this.birth;
        return fn.call({birth:2020},year)
   
    }
}
console.log(animal.getAge(2022)); 
```
+ 练习
```
请使用箭头函数简化排序时传入的函数：

'use strict'
var arr = [10, 20, 1, 2];
arr.sort((x, y) => {
    ???
});
console.log(arr); // [1, 2, 10, 20]
```
```
'use strict'
var arr=[10,20,1,2];
arr.sort((x,y)=>{
    if(x>y)
    {
        return 1;
    }
    if(x<y){
        return -1;
    }
    return 0;
})
 console.log(arr);
```
## 3.生成器（generator）
+ 格式：generator由function*定义，并且，除了return语句，还可以用yield返回多次
```
function*fib(max){
    var a=0,b=1,n=0;
    while(n<max){
        yield a;
        [a,b]=[b,a+b];
        n++
    }
    return;
}
for(var x of fib(10)){
    console.log(x);
}
```
## 4.标准对象(typeof操作符获取对象的类型，它总是返回一个字符串)
```
typeof 123; // 'number'
typeof NaN; // 'number'
typeof 'str'; // 'string'
typeof true; // 'boolean'
typeof undefined; // 'undefined'
typeof Math.abs; // 'function'
typeof null; // 'object'
typeof []; // 'object'
typeof {}; // 'object'
```
## 5.包装对象（包装对象用new创建）


