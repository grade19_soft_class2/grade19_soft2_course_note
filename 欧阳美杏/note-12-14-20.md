## sort
### 排序算法

```
正序排序

var arr = [1,10,33,22];

arr.sort(function(k,l){
    if(k<l){
        return -1;
    }
    if(k>l){
        return 1;
    }
    return 0;
});
  console.log(arr);//[1,10,22,33]

```
```
倒叙排序
var arr = [1,10,33,22];

arr.sort(function(k,l){
    if(k<l){
        return 1;
    }
    if(k>l){
        return -1;
    }
    return 0;
});
  console.log(arr);//[33,22,10,1]

```  

```
忽略大小写，根据字母排序
var arr = ['Baidu','apple','XY'];

arr.sort(function(x1,x2){
   let k1=x1.toUpperCase();
   let k2=x2.toUpperCase(); 
    if(k1<k2){
      return -1;
    }
    if(k1>k2){
        return 1;
    }
    return 0 ;
});
  console.log(arr);

```
### sort()方法是直接修改的原数组，因此返回的依旧是原数组

## every
### 这个东西可以判断数组里的元素是否符合条件

```
var arr=['Apple','XY','Baidu'];

console.log(arr.every(function (x){
    return x.length>0;
}));
返回turn，即所有元素的长度都大于0，符合条件

var arr=['Apple','XY','Baidu'];

console.log(arr.every(function(x){

    return x.toUpperCase() === x;
} ));
返回false,因为没有一个元素全部都是大写，所以不符合条件

```

## find
### 这个方法可以查找数组中符合要求的第一个元素,没有则返回undefined

```
var arr=['Apple','XY','Baidu'];

console.log(arr.find(function(x){

    return x.toUpperCase() === x;
} ));

返回XY,因为只有XY全部都是大写

```

## findindex
### 这个方法和find()方法差不多，但是它不会返回元素，只会返回索引

```
var arr=['Apple','XY','Baidu'];

console.log(arr.findIndex(function(x){

    return x.toUpperCase() === x;
} ));
返回1，都是大写的XY的索引是1

```
## forEach
### 这个方法常用于遍历数组，把每个元素依次作用在传入的函数，依旧是不会返回新的数组，传入的函数也不需要返回值

```
var arr=['Apple','XY','Baidu'];

arr.forEach(console.log);

```


