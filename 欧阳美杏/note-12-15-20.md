## 闭包
### 比如说定义了一个K，然后把东西打包打包塞到K里，k就成了个闭包

```
在对数组的求和中，可以返回求和的函数

function count(arr){
    var fool = function(){
        return arr.reduce(function(k,l){
            return k+l;
        });
    }
    return fool;

}

var f=count([1,3,7,14]);
console.log(f());//25

当调用count()时，每次返回都会是一个新的函数，但就算是相同的参数形成的相同的函数，var f 和var f1之间，f===f1,返回false，它们之间调用的结果互不影响

```

```
在这里，var和let的定义很不一样

function count(){
    var arr = [];
    for(var i = 1;i<=3;i++){//这里的var,相当于把var i=1提出来放在for外面
        arr.push(function(){
            return i*i
        });
    }
    return arr;
}
var fool = count();
var k1 = fool[0];
var k2 = fool[1];
var k3 = fool[2];

console.log(k1());//16
console.log(k2());//16
console.log(k3());//16

let

function count(){
    var arr = [];
    for(let i = 1;i<=3;i++){//它只在这一块区域里面，不会跑出去
        arr.push(function(){
            return i*i
        });
    }
    return arr;
}
var fool = count();
var k1 = fool[0];
var k2 = fool[1];
var k3 = fool[2];

console.log(k1());//1
console.log(k2());//4
console.log(k3());//9

```



