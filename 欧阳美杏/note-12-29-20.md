## Promise

### 在JS中，所有代码都是单线程执行

```
'use strict'

function callback() {
    console.log('Done');
}
console.log('before setTimeout()');
setTimeout(callback, 1000); // 1秒钟后调用callback函数
console.log('after setTimeout()');

可以看到一秒后，一个Done跳了出来

before setTimeout()
after setTimeout()
Done

异步操作会触发接下来某个时间点的函数来调用
```
```
生成一个0-2之间的随机数，如果小于1，则等待一段时间后返回成功，否则返回失败

function test(resolve, reject) {
    var timeOut = Math.random() * 2;
    log('set timeout to: ' + timeOut + ' seconds.');
    setTimeout(function () {
        if (timeOut < 1) {
            log('call resolve()...');
            resolve('200 OK');
        }
        else {
            log('call reject()...');
            reject('timeout in ' + timeOut + ' seconds.');
        }
    }, timeOut * 1000);
}

new Promise(test).then(function (result) {
    console.log('成功：' + result);
}).catch(function (reason) {
    console.log('失败：' + reason);
});

Promise最大的好处就是，代码和结果分开
```