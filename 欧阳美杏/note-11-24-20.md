## 看来凡人终究是体会不到笔记一起交的快乐

### 1. 变量
```
 变量名可以是数字、大小写英文、_、和$的组合，但是数字不能在开头，变量名也不能是JS的关键字，以及不要用中文，避免给自己找麻烦。

 ```

 ### 2. null和undefined

 ```
 null表示一个空的值，但它不是0.
 undefined是一个未定义的值.

 ```
 ### 3.对象

 ```
  var person={
     name:"xy",
     age:20,
     city:guangzhou,
  }

  定义了三个键，每个键叫做对象的属性，person的name属性是'xy',诸如此类.

 ``` 

 ### 4.全局变量

 ```
  一个页面内的全局变量
<script>
  var a = 100;
  function b(){
     a = 10;
     console.log(a);
     console.log(this.a);//this指针指向的是函数的调用者
     var a;
     console.log(a);

  }
   b();
   输出10；100；10；
</script>    

一般来说，没有var的就是全局变量。

 
