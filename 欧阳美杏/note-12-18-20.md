## 标准对象
### 在JS的世界里，一切都是对象

```
因为就算都是对象也还是会有不一样的对象，所以为了区分类型，我们用typeof操作获取对象的类型，它总是会返回一个字符串

console.log(typeof 'str');//string
console.log(typeof 123);//number
console.log(typeof true);//boolean
console.log(typeof undefined);//undefined
console.log(typeof Math.abs);//function
console.log(typeof null);//object

```
## 包装对象
### number boolean string都有包装对象，在JS中 字符串区分string类型和它的包装类型，包装对象用new创建

```
'use strict'
console.log(typeof 123);
console.log(typeof true);
console.log(typeof 'str');

生成新的类型
var k = new Number(123);
console.log(k);
var l = new Boolean(true);
console.log(l);
var x = new String('str');
console.log(x);

上面的输出会和之前一样，但是重新输出它的类型就会知道类型已经不一样了
console.log(typeof new Number(123));//object
console.log(typeof new Boolean(true));//object
console.log(typeof new String('str'));//object

所以说，使用包装对象就会出现像上面一样的情况，尤其是string类型

```
```
如果没有写new，就会是这样的

var k = Number('123');
console.log(k);//123

var l = Boolean('true');
console.log(l);//true

var z = Boolean('false');
console.log(z);//true

var z2 = Boolean('');
console.log(z2);//false

var x = String('str');
console.log(x);//str

```
```
总结：

不要使用new Number()、new Boolean()、new String()创建包装对象；

用parseInt()或parseFloat()来转换任意类型到number；

用String()来转换任意类型到string，或者直接调用某个对象的toString()方法；

通常不必把任意类型转换为boolean再判断，因为可以直接写if (myVar) {...}；

typeof操作符可以判断出number、boolean、string、function和undefined；

判断Array要使用Array.isArray(arr)；

判断null请使用myVar === null；

判断某个全局变量是否存在用typeof window.myVar === 'undefined'；

函数内部判断某个变量是否存在用typeof myVar === 'undefined'。

```
```
不是所有对象都有toString()方法的，null和undefined没有，null还会伪装成object类型

另外还要注意number对象调用toString()的时候

console.log(123..toString());//123
console.log((123).toString());//123

```
## Date
### 在JS中，Date对象用来表示日期和时间

```
在JS里，月份从0开始，即0是一月，1是二月，以此类推

var now = new Date();//获取当前系统时间
console.log(now);//Fri Dec 18 2020 16:45:06 GMT+0800 (中国标准时间)
console.log(now.getFullYear());//这个是年份，2020
console.log(now.getMonth());//这个是月份，11，代表12月
console.log(now.getDate());//这个是18，表示18号
console.log(now.getDay());//这个是5，星期五

```

```
创建一个指定日期和时间的Date对象

var k =new Date(2020//年,11//月,18//日,16//时,52//分,30//秒,123);
console.log(k);//Fri Dec 18 2020 16:52:30 GMT+0800 (中国标准时间)

```
```
第二种创建指定日期和时间的方法是解析一个符合ISO 8601格式的字符串

var k1 = Date.parse('2020-12-18T16:52:30.875+08:00');
console.log(k1);

使用Date.parse()时传入的字符串使用实际月份01~12，转换为Date对象后getMonth()获取的月份值为0~11

返回一个时间戳，有时间戳就可以很简单的转换成一个Date

时间戳是一个自增的整数，只要所在电脑时间是准确的，那么无论在世界上哪一台电脑，它们生成的时间戳都是一样的

所以，时间戳可以精确地表示一个时刻，并且与时区无关

var k3 = new Date(1608281550875);
console.log(k3);//Fri Dec 18 2020 16:52:30 GMT+0800 (中国标准时间)
console.log(k3.getMonth());//11,11月

```

## 时区

### 即可以是本地时间，也可以是调整后的UTC时间

```
'use strict'

var k = new Date(1608281550875);
console.log(k.toUTCString());//Fri, 18 Dec 2020 08:52:30 GMT
console.log(k.toLocaleString());//2020/12/18 下午4:52:30

```

