## 操作DOM

```
更新：更新该DOM节点的内容，相当于更新了该DOM节点表示的HTML内容；

遍历：遍历该DOM节点下的子节点，以便进一步操作；

添加：在该DOM节点下新增一个子节点，相当于动态增加了一个HTML节点；

删除：将该节点从HTML中删除，相对于删掉了该DOM节点的内容以及它包含的所以子节点；

在操作一个DOM节点前，我们需要通过各种方式先拿到一个DOM节点，最常用document.getElementByld()和document.getElementsByTagName()
以及CSS选择器document.getElementsByClassName()
```
```
'use strict'

let test = document.getElementById('drink-menu');
console.log(test);

```
```
第二种方法通过使用querySelector()和querySelectorAll()

let q = document.querySelector('#q1');
console.log(q);

```

## 更新DOM

### 拿到一个DOM节点后，可以对它进行更新，可以直接修改节点的文本，方法有两种

```
第一种，修改innerHTML属性，这个方式可以修改一个DOM节点的文本内容，还可以直接通过HTML片段修改DOM节点内部的子树

let q = document.querySelector('#q1');
console.log(q);

q.innerHTML = '世界第一可爱拉姆达 <span style="color:pink">世界第一可爱拉姆达</span>';

输出的就是一行粉色的拉姆达啦

```

```
第二种是修改innerText或textContent属性,这样可以自动对字符串进行HTML编码，保证无法设置任何HTML标签

//获取<p id="p-id">...</p>
var p = document.getElementById('p-id');
// 设置文本:
p.innerText = '<script>alert("Hi")</script>';
// HTML被自动编码，无法设置一个<script>节点:
// <p id="p-id">&lt;script&gt;alert("Hi")&lt;/script&gt;</p>

```
### 两者的区别在于读取属性是，innerText不返回隐藏元素的文本，而textContent返回所有文本

```
修改CSS也是经常需要的操作，DOM节点的style属性对应所有的CSS，可以直接获取或设置。因为CSS允许font-size这样的名称，但它并非JavaScript有效的属性名，所以需要在JavaScript中改写为驼峰式命名fontSize：

// 获取<p id="p-id">...</p>
var p = document.getElementById('p-id');
// 设置CSS:
p.style.color = '#ff0000';
p.style.fontSize = '20px';
p.style.paddingTop = '2em';

```