**### Map和Set**
+ 当不确定键值对键的名称时，就需要用Map
```
var arr = new Map();
var key = {},
    Func = function(){
       // return 
    },
    keyString = "壁画";

arr.set(keyString, "敦煌'");
arr.set(key, "哈登");
arr.set(Func, "詹姆斯");

console.log(arr.get(Func));
```
```
结果：詹姆斯
```
**### Set**

1. add(value)：添加某个值，返回 Set 结构本身。
2. delete(value)：删除某个值，返回一个布尔值，表示删除是否成功。
3. has(value)：返回一个布尔值，表示该值是否为Set的成员。
4. clear()：清除所有成员，没有返回值。
5. Set的每一个元素都是唯一的(数据结构类似数组，但所有成员的值唯一)
6. Set是构造函数，用来生成 Set数据结构，使用 add方法来添加新成员
```
let a = new Set();
[1,2,2,1,3,4,5,4,5].forEach(x=>a.add(x));
for(let k of a){
    console.log(k)
};
```
```
// 1 2 3 4 5
```