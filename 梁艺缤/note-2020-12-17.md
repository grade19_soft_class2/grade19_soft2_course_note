##**闭包**

###**函数作用返回值**
```
大概是为了引出闭包设计
```
```
function a(){
    alert('大黄');
    function abs(){
        alert('詹姆斯')
    }
    abs();
}
console.log(a());
```
```
function a(){
    alert('ff');
    return function(){
        alert('kkk');
    }
}
console.log(newFun=a());//FF
console.log(newFun);
```

总结：js函数作为返回值可以实现内层函数在外层函数之外被调用

###**闭包**

1. 闭包就是可以读取其他函数内部变量的函数(定义在一个函数内部的函数)

2. 变量的作用域：局部变量和全局变量

3. 声明变量的时候，一定要有用var命令 

4. 所以js创建一个计数器
```
function bert(jus){
    var gg = jus || 0;
    return{
        ger:function(){
            gg += 2
            return gg;
        }
    }
}

var c = bert();

console.log(c.ger());//2
console.log(c.ger());//4
console.log(c.ger());//6
```
+ //增加
```
function bert(x){
    var arr = x|0;
    var ver ={
        age2:function(){
            return arr+=1;
        }
    }
    return ver;
}

var conutters = bert();

console.log(conutters.age2());
console.log(conutters.age2());
```

