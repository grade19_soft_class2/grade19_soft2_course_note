# 2020-11-19日份的课堂笔记  

# 扩展方法
## 扩展方法可以用新方法扩展现有类型改变原始类型的定义。  
## 扩展方法是静态类的静态方法，其中this修饰符应用于第一个参数。  
## 第一个参数的类型将是扩展的类型。  
### 例如:

    public static class StringExtension {
        public static bool IsCapitalized (this string s) {
            if (string.IsNullOrEmpty(s))
                return false;
            return char.IsUpper (s[0]);
        }
     }
## IsCapitalized扩展方法可以被调用，就像它是一个字符串上的实例方法，如下所示： 

    Console.WriteLine ("Javascript".IsCapitalized());  
## 一个扩展方法调用被转换回一个普通的静态方法调用：  

    Console.WriteLine (StringExtension.IsCapitalized ("Javascript"));
## 扩展方法与实例方法  
## 任何兼容的实例方法优先于扩展方法。  
## 在以下示例中，Main的MyMethod方法将始终优先 - 即使在使用int类型的参数x调用时：  

    class Main {
    public void MyMethod (object x) { } 
    }

    static class Extensions {
    public static void MyMethod (this Main t, int x) { }
    }  
## 在这种情况下调用扩展方法的唯一方法是通过正常的静态语法：  

    Extensions.MyMethod(...)
## 扩展方法与扩展方法  
## 如果两个扩展方法具有相同的签名，则扩展方法必须作为普通静态方法调用，以消除调用方法的歧义。  
## 如果一个扩展方法具有更多特定的参数，则更具体的方法优先。  
## 以下代码显示了如何使用它：  

    static class StringExtension {
        public static bool IsCapitalized (this string s) {...}
    }
    static class ObjectHelper {
        public static bool IsCapitalized (this object s) {...}
    }
## 以下代码调用StringExtension的IsCapitalized方法：  

    bool test1 = "Javascript".IsCapitalized();
## 要调用ObjectHelper的IsCapitalized方法，我们必须明确指定：  

    bool test2 = (ObjectHelper.IsCapitalized ("Javascript"));  



# Lambda
## lambda运算符：所有的lambda表达式都是用新的lambda运算符 " => "。运算符将表达式分为两部分，左边指定输入参数，右边是lambda的主体。
## lambda表达式：  

    1.一个参数：param=>expr
    2.多个参数：（param-list）=>expr
## 比如：  

    namespace lambda
    {
        public class Person
        {
            public string Name { get; set; }
            public int Age  {  get;set; }    
        }
        class Program
        {

            public static List<Person> PersonsList()
            {
                List<Person> persons = new List<Person>();
                for (int i = 0; i < 7; i++)
                {
                    Person p = new Person() { Name = i + "儿子", Age = 8 - i, };
                    persons.Add(p);                
                }
                return persons;
            }

            static void Main(string[] args)
            {
                List<Person> persons = PersonsList();
                persons = persons.Where(p => p.Age > 6).ToList();       //所有Age>6的Person的集合
                Person per = persons.SingleOrDefault(p => p.Age == 1);  //Age=1的单个people类
                persons = persons.Where(p => p.Name.Contains("儿子")).ToList();   //所有Name包含儿子的Person的集合
            }
        }
    }
# 委托   

    //委托  逛超市
    delegate int GuangChaoshi(int a);
    static void Main(string[] args)
    {
        GuangChaoshi gwl = JieZhang;
        Console.WriteLine(gwl(10) + "");   //打印20，委托的应用
        Console.ReadKey();
    }
    
    //结账
    public static int JieZhang(int a)
    {
        return a + 10;
    }  
***
# 表达式  

    //委托  逛超市
    delegate int GuangChaoshi(int a);
    static void Main(string[] args)
    {          
       // GuangChaoshi gwl = JieZhang;
        GuangChaoshi gwl = p => p + 10;
        Console.WriteLine(gwl(10) + "");   //打印20，表达式的应用
        Console.ReadKey();
    }  
## 其实表达式（p => p + 10;）中的 p 就代表委托方法中的参数，而表达式符号右边的 p+10，就是委托方法中的返回结果。
##  1.多参数的  

    //委托  逛超市
    delegate int GuangChaoshi(int a,int b);
    static void Main(string[] args)
    {            
        GuangChaoshi gwl = (p,z) => z-(p + 10);
        Console.WriteLine(gwl(10,100) + "");   //打印80，z对应参数b，p对应参数a
        Console.ReadKey();
    }
## Lambda主体运算  

    /// <summary>
    /// 委托  逛超市
    /// </summary>
    /// <param name="a">花费</param>
    /// <param name="b">付钱</param>
    /// <returns>找零</returns>
    delegate int GuangChaoshi(int a,int b);
    static void Main(string[] args)
    {
        GuangChaoshi gwl = (p, z) =>
        {
            int zuidixiaofei = 10;
            if (p < zuidixiaofei)
            {
                return 10;
            }
            else
            {
                return z - p;
            }
       
        };
        Console.WriteLine(gwl(10,100) + "");   //打印80，z对应参数b，p对应参数a
        Console.ReadKey();
    }
# Func<T>委托
## T 是参数类型，这是一个泛型类型的委托。  

    static void Main(string[] args)
            {
                Func<int, string> gwl = p => p + 10 + "--返回类型为string";            
                Console.WriteLine(gwl(10) + "");   //打印‘20--返回类型为string’，z对应参数b，p对应参数a
                Console.ReadKey();
            }
### 这里的p为int 类型参数， 然而Lambda主体返回的是string类型的。  

    static void Main(string[] args)
    {
        Func<int, int, bool> gwl = (p, j) =>
            {
                if (p + j == 10)
                {
                    return true;
                }
                return false;
            };
        Console.WriteLine(gwl(5,5) + "");   //打印‘True’，z对应参数b，p对应参数a
        Console.ReadKey();
    }
### 说明：p为int类型，j为int类型，返回值为bool类型。
### Func<T>的用法：多个参数，前面的为委托方法的参数，最后一个参数，为委托方法的返回类型。