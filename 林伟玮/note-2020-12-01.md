# 今天只讲了一道题，但是有很多种解法
### 练习：
+ Array提供了一种顺序存储一组元素的功能，并可以按索引来读写。
+ 在新生欢迎会上，你已经拿到了新同学的名单，请排序后显示：欢迎XXX，XXX，XXX和XXX同学！：

`

          'use strict';
           var arr = ['小明', '小红', '大军', '阿黄'];
           console.log('???');
`


'use strict';

+ 1.
`


           var arr = ['小明','小红','大军','阿黄'];
           console.log(`欢迎${arr.slice(0,arr.length-1)}和${arr[arr.length-1]}同学`);
`
+ 2.
            
            
            var arr = ['小明','小红','大军','阿黄'];
            var newArr = [];
            newArr.push('欢迎');
            for(var i = 0;i<arr.length;i++){
                if(i<arr.length-2){
                    newArr.push(arr[i]);
                    newArr.push(',');
                   }else if(i===arr.length-2){
                        newArr.push(arr[i]);
                   }else if(i===arr.length-1){
                     newArr.push('和');
                     newArr.push(arr[i]);
                  }                       
            }
            newArr.push('同学');
            console.log(newArr);
            console.log(newArr .join(''));


+ 3.
`

            var arr = ['小明','小红','大军'];
            var get = ['阿黄'];
            console.log(`欢迎${arr}和${get}同学`);

`

+ 4. 
`

            var arr =['小明','小红','大军','阿黄'];
            console.log(`欢迎${arr[0]}、${arr[1]}、${arr[2]}和${arr[3]}同学`);

`            