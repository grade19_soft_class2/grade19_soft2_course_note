# 大二第一学期课堂笔记

### Description
课堂笔记 2020-12-07

### Content

#### 变量作用域
1. 在JavaScript中，用var声明的变量实际上是有作用域的。
2. 不同函数内部的同名变量互相独立，互不影响。

```
function foo() {
    var x = 1;
    x = x + 1;
}

x = x + 2; // ReferenceError! 无法在函数体外引用变量x
```
+ JS函数可以嵌套，内部函数可以访问外部函数定义的变量，反过来则不行。
```
function pvm(figure,fuc){
    var figure = 3366;
    
    var fuc=(qbt,rbt)=>{
        console.log(qbt);
        console.log(rbt);
        console.log(figure);    //内部可以访问外部figure  3366
        var x = 555;
    }
    fuc('rerw',6);

    // console.log(x); //发生报错   外部不能访问内部  x is not defined

    var yy = 'Player ACE';
    console.log(yy);
}
pvm();
```
+ 如果两个函数的函数名重名怎么办？来测试一下：
```
function pvm(i,j){
    var i = 'tf';
    var j = 8;
    console.log(i);
    console.log(j);
}
pvm();
```
- 结果为：重名函数定义的变量会覆盖原来函数定义的全部变量!!<br>

+ 如果内部函数和外部函数的变量名重名怎么办？来测试一下：
```
function tpc(kfc,mdl){
    var kfc = '肯德基';
    var mdl = '麦当劳';    

    function me(kfc) {
        var kfc = 666;
        console.log(kfc);   //666
    }

    console.log(kfc);   //肯德基
    console.log(mdl);   //麦当劳

    me();
    
}
tpc();
```
+ 说明JavaScript的函数在查找变量时从自身函数定义开始，从“内”向“外”查找;
- 如果内部函数定义了与外部函数重名的变量,同名变量互相独立，互不影响。

#### 一、变量提升

+ JavaScript的函数定义有个特点，它会先扫描整个函数体的语句，把所有声明的变量“提升”到函数顶部。
- 我们在函数内部定义变量时，要严格遵守“在函数内部首先声明所有变量”这一规则。
```
function exp(i,j){
    // var i = '斯米马赛-_-|||   ' + j;
    // console.log(i);
    // var j = '退货退货';
    //以上内容在JS中相当于如下内容
    var j; // 提升变量j的声明，此时j为undefined
    var i = '斯米马赛-_-|||   ' + j;
    console.log(i);         //斯米马赛-_-|||   undefined
    j = '退货退货';
}
exp();
```

#### 二、全局作用域

+ JavaScript实际上只有一个全局作用域。任何变量（函数也视为变量），如果没有在当前函数作用域中找到，就会继续往上查找，最后如果在全局作用域中也没有找到，则报ReferenceError错误。
- JavaScript默认有一个全局对象window，全局作用域的变量实际上被绑定到window的一个属性。

```
console.log(window);

var role = '玛卡巴卡';
console.log(role);    //玛卡巴卡
console.log(window.role); //玛卡巴卡
//直接访问全局变量role和访问window.role是完全一样的。
```

+ 以变量方式定义的顶层函数也被视为是一个全局变量，并绑定到window对象。
```
function book(){
    console.log('nice!!!');
    function novel(){
        console.log('奈斯！！！');
    }
    novel();
}
book(); //nice!!!   奈斯！！！
window.book();  //nice!!!   奈斯！！！
// window.novel();  发生报错，内部函数novel不是全局变量，未绑定到window上。
```

+ alert()函数其实也是window的一个变量
```
window.alert('number one'); //页面提示：   number one

var bl = window.alert;
window.alert = function(){
    alert('无法显示-_-|||');    //没有反应
}

window.alert = bl;
alert('又可显示了');    //页面提示：        又可显示了
```

#### 三、名字空间

+ 全局变量会绑定到window上,当全局变量名发生重名时，会造成命名冲突，且很难发现。
- 减少冲突的一个方法是把自己的所有变量和函数全部绑定到一个全局变量中。
```
var master = {};    //设置一个唯一的全局变量（名字空间）master

master.title = '拼夕夕';  //把其他变量（函数）都放入master中
master.fnc = function (){
    return '老少皆宜';
};
```

#### 四、局部作用域

+ JS的变量作用域是在函数内部，而在for循环等语句块中是无法定义具有局部作用域的变量的。
```
function rtt(){    
    for(var i=0;i<20;i++){        

    }
    i +=1;  //仍可引用变量i
    console.log(i); //21
};
rtt();
```

+ 用ES6引入了新的关键字let替代var声明的变量作用域为块级作用域，在块级作用域外的语句无法访问let声明的变量。
```
function ppt(){
    for(let i=0;i<20;i++){  //i的作用域为它所在的for循环

    }
    // i +=1;  //不可引用变量i
    // console.log(i); //发生报错  i is not defined
}
ppt();
```

#### 五、常量const
+ const与let都具有块级作用域
```
function eee(){
    for(var i=0;i<20;i++){
        const PI = 3.14;    //PI的作用域为它所在的for循环
    }
    console.log((i));   //20
    console.log(PI);    //发生报错  PI is not defined
}
eee();
```