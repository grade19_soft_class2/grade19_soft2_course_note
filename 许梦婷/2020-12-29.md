今日份课堂笔记

2020-12-29 星期二 天气晴

练习
对于一个已有的HTML结构：
```
Scheme
JavaScript
Python
Ruby
Haskell
<!-- HTML结构 -->
<ol id="test-list">
    <li class="lang">Scheme</li>
    <li class="lang">JavaScript</li>
    <li class="lang">Python</li>
    <li class="lang">Ruby</li>
    <li class="lang">Haskell</li>
</ol>
```
按字符串顺序重新排序DOM节点：
```
'use strict';
// sort list:

// 测试:
'use strict'

let t =document.getElementById('test-list');

let els=t.children;

let arr=[];

for(let i=0;i<els.length;i++){
    arr.push(els[i].textContent)
}

console.log(arr);
arr.sort();

console.log(arr);

while(els.length>0){
    t.removeChild(els[0]);
}

for(let i=0;i<arr.length;i++){
    let x=document.createElement('li')
    x.className='lang'
    x.textContent=arr[i];
    t.appendChild(x);
}
;(function () {
    var
        arr, i,
        t = document.getElementById('test-list');
    if (t && t.children && t.children.length === 5) {
        arr = [];
        for (i=0; i<t.children.length; i++) {
            arr.push(t.children[i].innerText);
        }
        if (arr.toString() === ['Haskell', 'JavaScript', 'Python', 'Ruby', 'Scheme'].toString()) {
            console.log('测试通过!');
        }
        else {
            console.log('测试失败: ' + arr.toString());
        }
    }
    else {
        console.log('测试失败!');
    }
})();
```
## Promise

Promise有各种开源实现，在ES6中被统一规范，由浏览器直接支持。先测试一下你的浏览器是否支持Promise：
```
'use strict';

new Promise(function () {});
// 直接运行测试:
console.log('支持Promise!');
```

一个最简单的Promise例子：生成一个0-2之间的随机数，如果小于1，则等待一段时间后返回成功，否则返回失败
```
'use strict'

let test=function(resolv,reject){
    let timeOut=Math.random()*2;
    console.log('set timeout to:' + timeOut +'seconds');
    setTimeout(()=>{
        if(timeOut<1){
            console.log('执行resolve方法');
            resolv('成功');
        }
        else{
            console.log('执行reject方法');
            resolv('失败');
        }
    },timeOut*1000)
}
let p =new Promise(test);
p.then((x)=>{
    console.log('对了'+x);
}).catch(x=>{
    console.log('错了'+x);
})

测试结果：
如果小于1就返回
执行resolve方法
对了成功
如大于1就返回
执行reject
错了失败

```
异步计算：
```
function multiply(x){
    return new Promise((resolve,reject)=>{
        console.log('这里是异步乘法运算，在多少秒后返回结果');
        setTimeout(resolve,1500,x*x)
    })
};
function add(x){
    return new Promise((resolve,reject)=>{
        console.log('这里是异步乘法运算，在多少秒后返回结果');
        setTimeout(resolve,800,x+x)
    })
};
let p=new Promise((resolve,reject)=>{
    console.log('任务开始');
    resolve(123);
})

p.then(multiply)
 .then(add)
 .then(multiply)
 .then(add)
 .then( (x) =>{
    console.log('执行完成');
});
```