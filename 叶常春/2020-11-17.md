# 今天讲了git合并冲突的问题
+ 先克隆原仓库
+ 再克隆已合并好的仓库
+ 再把已经修改过仓库的文件删除，从原仓库复制过去
+ 最后push 就ok啦
 
# 委托 
+ 自定义委托 
```
deleget void Say();''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' function void SayHello() { }; Say s=SayHello();
```

+ 内置委托 func action
```
func 代表有一个返回值 func<bool> abc=()=>true;
action 代表没有返回值的方法 action<string> bbc=()=>{};
```