# 今天是js的第七单元 天气阴 有点小冷

## 变量作用域 
+ 在Js中var申明的变量是有作用域的。变量在函数内部声明，该变量的作用域是整个函数体，在函数体外不能应用该变量。代码如下：
```
'use strict';

function foo() {
    var x = 1;
    x += 1;
}
console.log(x+1); 
```
 
+ 不同的函数里声明名称相同的变量并不会产生影响
```
function foo() {
    var x = 1;
    x += 1;
    console.log(x);
}
foo();

function foo2() {
    var x = 2;
    x += 2;
    console.log(x);  
}
foo2();
```

+ Javascript里的函数时可以嵌套的，内部函数可以访问上一级函数声明的变量反过来则不行
```
   function foo() {
    var x = 2001121;
    var num2 = j-x; // error：变量j is not defined
    console.log(num2); 
    function foo2() {
        var j = 20020704;
        var num = j-x;
        console.log(num); //没问题
    }
}
foo();
```
```
function foo() {
    var x = 2001121;
    function foo2() {
        var j = 20020704;
        var num = j-x;
        console.log(num); //没问题
    }
    foo2();
}
foo();
```

+ 如果内部函数和外部函数变量名一样会发生什么？
```
function foo() {
    var x = 1;
    function foo2() {
        var x = 'A';
        console.log('x in foo2 =' + x); //'A'
    }
    console.log('x in foo =' + x);    // 1 
    foo2();
}
foo();
```
+ 结论：说明JavaScript的函数在查找变量时是从内向外找的，如果定义了相同名称的变量，内部函数的变量将会“屏蔽”外部函数的变量.
+ 谢邀，人在教室，刚上机，发现了一个有意思的点：JavaScript执行嵌套函数时会先执行外部函数再执行内部函数

![1](./img/2020-12-07-01.png)

## 变量提升
+ JavaScript的函数定义有一个特点，它会先扫描函数体的所有语句，把声明的变量提升到顶部
```
function foo() {
    var x = 'A';
    console.log('x + y =' + x + ',' + y);
    var y = 'B';
}
foo();
```

+ 为什么y是undefined呢，因为JavaScript只是把变量提升到顶部而已，但变量的值却不会提升，JavaScript引擎看到的语句相当于:
```
function foo() {
    var y;
    var x = '五八五八,'+y;
    console.log(x);
    var y = '忘了我吧';
}
foo();
```

+ 由于JavaScript这一“怪异”的特性，我们解决的方法是将函数体所有用得到的变量用一个var在函数顶部声明
```
function foo() {
    var x = 1   // 1
        y = x + 1 // 2
        h,t; // undefind
        //其它语句
    for(var i = 0;i < 1000; i++){
        //......
    }
}
foo();
```

## 全局作用域
+ 不在任何函数体内定义的变量就具有全局作用域，JavaScript有一个默认的全局对象window，全局变量都被绑定到了这个对象身上
```
var course = 12;
alert(course);
alert(window.course);
```
因此直接访问全局变量跟通过window对象调用全局变量是一样的

+ 以 var方式定义的函数实际上也是个全局变量，因此顶层函数的定义也被绑定到了window对象身上
```
function foo() {
    alert("foo")
}
foo(); //直接访问
window.foo(); //通过window调用
```

+ 我们每次直接调用的alert()函数其实也是window的变量
```
window.alert('调用window.alert()');
//alert()函数保存到另一个变量
var old_alert = window.alert;
//给alert赋一个新函数
window.alert = function(){}
alert('无法用alert()显示了');

//恢复alert
window.alert = old_alert;
alert('又可以显示了');
```
这说明JavaScript实际上只有一个全局作用域。任何变量（函数也视为变量），如果没有在当前函数作用域中找到，就会继续往上查找，最后如果在全局作用域中也没有找到，则报ReferenceError错误

## 名字空间
+ 全局变量会绑定到window上，不同的JavaScript文件如果使用了相同的全局变量，或者定义了相同名字的顶层函数，都会造成命名冲突，并且很难被发现。 减少冲突的一个方法是把自己的所有变量和函数全部绑定到一个全局变量中
```
var MYApp = {};  //第一个js文件
MYApp.foo = function() {
    console.log('MYApp');
}
console.log(window); 


var MYApp2={};  //第二个js文件
MYApp2.foo = function () {
    console.log('MYApp2');
}
```
结果就是

![1](./img/2020-12-07-02.png)

## 局部作用域
+ 由于JavaScript的变量作用域实际上是函数内部，我们在for循环等语句块中是无法定义具有局部作用域的变量的：
```
'use strict';

function foo() {
    for (var i=0; i<100; i++) {
        //
    }
    i += 100; // 仍然可以引用变量i
}
```
+ 为了解决块级作用域，ES6引入了新的关键字let，用let替代var可以申明一个块级作用域的变量：
```
'use strict';

function foo() {
    var sum = 0;
    for (let i=0; i<100; i++) {
        sum += i;
    }
    // SyntaxError:
    i += 1;
}
```

## 常量
+ 由于var和let申明的是变量，如果要申明一个常量，在ES6之前是不行的，我们通常用全部大写的变量来表示“这是一个常量，不要修改它的值”：
```
var PI = 3.14;
```
+ ES6标准引入了新的关键字const来定义常量，const与let都具有块级作用域：
```
'use strict';

const PI = 3.14;
PI = 3; // 某些浏览器不报错，但是无效果！
PI; // 3.14
```

## 完事儿 还有最后一点点比较难的部分应该是留给明天收尾 今天就这样吧^^
# 每日一言：很喜欢辉宝（黄勇辉）说的一句话《我向你敬礼，涩撸特》