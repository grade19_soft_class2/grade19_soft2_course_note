# 今天承接上节课 把闭包讲干净 但是讲课之前 先讲了之前讲的很多题目（原因竟是！！！胡哥记得讲过了，其实是在一班讲过了{可恶哇}） 天气⛅ 天气更冷了（穿三件依旧瑟瑟发抖）

## 题目
+ 请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']
```
// 请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。
// 输入：['adam', 'LISA', 'barT']，
// 输出：['Adam', 'Lisa', 'Bart']
  
function normailize(arr){
    function changeword(word){
        var newword='';
        for(var i=0;i<word.length;i++){
            if(i===0){
                newword+=word[i].toUpperCase();
            }else{
                newword+=word[i].toLowerCase();
            }
        }
        return newword;
    }
    return arr.map(changeword);
}
// 测试:
if (normailize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}
```

+ 小明希望利用map()把字符串变成整数，他写的代码很简洁：
```
'use strict';

var arr = ['1', '2', '3'];
var r;
r = arr.map(parseInt);

console.log(r);

//更改后的代码
var arr = ['1','2','3'];
var r;
    r=arr.map(function (x){
        return parseInt(x);
    })
      
console.log(r);
```

+ 请尝试用filter()筛选出素数
```
function get_primes(arr){
    return  arr.filter(function (x){
        var flag = true;
        if(x < 2){
            flag = false;
        }else{
            for(var i = 2;i < x; i++){
                if(x % i === 0){
                    flag = false;
                    break;
                }else{
                    flag = true;
                }
            }
            return flag;
        }
    })
}

var
    x,
    r,
    arr = [];
for (x = 1; x < 100; x++) {
    arr.push(x);
}
r = get_primes(arr);
if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
    console.log('测试通过!');
} else {
    console.log('测试失败: ' + r.toString());
}
```

+ 不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数：
```
function string2int(s){
    var s_length = s.length;
    if(s_length===1){
        return s * 1;
    }else{
        var arr = s.split("");
        var res = arr.reduce(function (x,y){
            return x *10 + y *1;
        });
        return res;
    }
}
// 测试:
if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
    if (string2int.toString().indexOf('parseInt') !== -1) {
        console.log('请勿使用parseInt()!');
    } else if (string2int.toString().indexOf('Number') !== -1) {
        console.log('请勿使用Number()!');
    } else {
        console.log('测试通过!');
    }
}
else {
    console.log('测试失败!');
}
```

## 闭包2.0（关于闭包还有一些东西）
+ 注意到返回的函数在其定义内部引用了局部变量arr，所以，当一个函数返回了一个函数后，其内部的局部变量还被新函数引用，所以，闭包用起来简单，实现起来可不容易。

另一个需要注意的问题是，返回的函数并没有立刻执行，而是直到调用了f()才执行。我们来看一个例子：
```
function count(){
    var arr = [];
    for(var i = 1; i <= 3; i++){
        arr.push(function (){
            return i * i;
        })
    }
    return arr;
}
var result = count();
var f1 = result[0];
var f2 = result[1];
var f3 = result[2];
console.log(f1()); //16
console.log(f2()); //16
console.log(f3()); //16
```
为什么都是16呢？我们预期的不应该是 1 4 9 吗

我们先看看 i在循环中的值到底是什么

![1](./img/2020-12-17-01.png)

原因就在于返回的函数引用了变量i，但它并非立刻执行。等到3个函数都返回时，它们所引用的变量i已经变成了4，因此最终结果为16。
返回闭包时牢记的一点就是：返回函数不要引用任何循环变量，或者后续会发生变化的变量

+ i在循环中的值为4；因为 i这个变量是用var修饰的，用var声明变量这个变量不具有块级作用域。也就是说Js引擎看到的其实是这样的:
```
function count(){
    var arr = [];
    var i = 4;
    for(var i = 1; i <= 3; i++){
        arr.push(function (){
            console.log(i);
            return i * i;
        })
    }
    return arr;
}
```
解决方法：用let声明变量i

+ 如果一定要引用循环变量怎么办？方法是再创建一个函数，用该函数的参数绑定循环变量当前的值，无论该循环变量后续如何更改，已绑定到函数参数的值不变：
```
function count() {
    var arr = [];
    for (var i=1; i<=3; i++) {
        arr.push((function (n) {
            return function () {
                return n * n;
            }
        })(i));
    }
    return arr;
}
var result = count();
var f1 = result[0];
var f2 = result[1];
var f3 = result[2];
console.log(f1());
console.log(f2());
console.log(f3());
```

## 芜湖~ 再次起飞！

![2](./img/2020-12-17-02.jpg)

# 每日一言：很喜欢湖工大的朋友被室友打了之后室友在辅导员面前的死不悔改发言，《屁股与王者荣耀》