# 昨天刚刚好没有提交，但是我已经预习惹，就不会很慌。
## 2020年11月23日
根据我的了解，和上课内容的理解
+ 数据类型
var 作为一个定义的数据类型，基本上哪一种的高级编程语言都可以拿来使用var。可以定义一个变量，可以在一个变量中使用亦或者是在部分变量使用，
其次可以是定义一个以单引号的字符串或者双引号的字符串来进行定义。

##  布尔类型
bool 作为判断语句，但是在JavaScript里面，那里面的比较运算符还是在继续打一遍，不然还是会记不住der。
Ⅰ：&&与运算，两边的算式都是对的，返回值才能是对的，返回值为true
Ⅱ：||或运算，左右两边其中一个是对的，就返回true。
！:非运算，左右两边的true改为false，false改为true，单目运算符：也就是只接受一个操作数的运算
例子：
 ```
! true; // 结果为false
! false; // 结果为true
! (2 > 5); // 结果为true
  ```
## 比较运算符
，需要对Javascript里面的NaN===NaN //false,这个是比较奇怪的判断，我不等于我自己，不过很好理解
能够判断NaN的判别式只有isNaN(NaN);//具有唯一性且是这么判断的。
然后在判断的比较字符串类型中，JavaScript具有两种比较运算符，Ⅰ：==  Ⅱ :  ===
要特别注意相等运算符==。这个是会自动转换数据类型，好家伙，如果是单精度的浮点类型和双精度的浮点类型的比较是吧，差不多得了。
那 === 还不如这种的类型呢，三等于多好，直接错了就返回false，不一样就是不一样。数据一样才能比较
那么建议使用===，且会比==好很多。
比较任意浮点类型的比较
```
1 / 3 === (1 - 2 / 3); // false
这个类型的比较计算机不能比较无线循环，只能在精确到他们差的绝对值，或者是阈值内进行比较。
```
## 补充
JavaScript也是注意大小写，然后还有单行注释//，还有块注释/*...*/。就是这么简单
