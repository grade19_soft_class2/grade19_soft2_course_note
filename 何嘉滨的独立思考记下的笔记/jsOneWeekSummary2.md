# 又到了我一周的js内容周总结,很抱歉,我这周虽然很放松,但是我找回我状态了. 12/7日补,早上6:55.

# 本周js的内容有:js对象,js的map()和set()函数 循环类以及函数及其方法体. 

## 划重点:本周js的重点在于函数,还有arguments(),rest()函数.

+ 1. js的对象
js的对象是由自定义的变量,以及其键值对构成,对象的属性值可以为任意的数据类型,不必局限于一种,也可以是数组,以至于数组的值都可以不一定为整形类.

```
var demo1=
{name='小何,
 age=20,//想要18哈哈哈
 height=180,
}
```

有点类似于局部变量.

**Html5和javascript代码未测试①!!!**

+ 2. js的map()和set()函数

**map和set都是在新的ES6的新标内进行添加**

map()函数是具有快速搜寻键值对的一种函数,无论说该js函数内容是有多复杂,都可以快速寻找到我想要的值.甚至是快速获取.map()可以有重复的键值对.

set()函数map()都是同一类函数,都是可以快速检索,但是set()函数是不会存储value的值,且不能有重复的键值对.

+ 2. js的循环类 js的循环类有 ①:while ②:for ③:for...in循环 ④: do...while循环 

 **for : 比较适合遍历数组,字符串等等.**
**for in : 比较适合遍历对象,遍历对象时使用这个再合适不过了.**
 **while : while 的话,与 for 的使用场景差不多.唯一不同的是,遍历的条件必须满足.**
**do while : 至少执行一边的循环,遍历数组和字符串也是很方便.**

①:while 循环比较友好,例如在写一些循环的时候,可以在while循环没有很复杂函数体和复杂的操作逻辑.

②:for 循环可以在函数体内写一些复杂的逻辑嵌套,当然了只是比高阶的逻辑更为简单.

③:for...in 使用for...in循环来循环数组,还有for...of循环来遍历,更加方便,for...of也是ES6引入的特性,弥补了forEach和for in 的特性.其中for...of功能更强大.for...in循环可以遍历数组内变量名的属性,
既for...in 能遍历数组还能遍历对象.

④:do...while 每一次的条件为真且每一次都是要执行一遍操作.

+ 3. arguments和rest函数

arguments函数是为了避免说我们在进行传参亦或者是调用方法的时候要使用arguments.arguments函数不是数组.

rest函数是一种数组类型,传递的是一种数组类型,返回的也是数组类型.

//本周的js学习内容不多,但是还是需要去测试代码.
