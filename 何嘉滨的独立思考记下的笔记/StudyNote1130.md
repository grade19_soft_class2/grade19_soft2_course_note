  # 今天是11.30号,天气不咋地,继续学习JavaScript的知识.
## JavaScript 对象
+ 1. js对象
js对象是一种无序的集合数据类型,它由若干个键值对组成.
描述一个对象的话,让我联想起一个c里面的关键字auto或者是register.都是带有储存类型的.

伪代码①:

```
var SmallFriend=
{   name='小何',
    age=20,
    birth=2000,
    height=180,
    weight='75kg'
}   
```

其中对象的最后一个键值对是不需要写,,的.
可以通过 SmallFriend 变量名来获取属性

```
SamllFriend.name;//'小何'
SmallFriend.age;//20
```

访问属性中如果有-\+\+++等一系列的特殊符号都要用''阔起来然后获取属性是需要[]来进行输出.

+ 2. js对象的动态类型
js的动态类型能够自由的给对象进行添加属性或者删除属性,

伪代码②:

```

var SamllFriend=
{   
    name='小何'
}
SmallFriend.age;//undefined
SmallFriend.age=18;//18新增一个属性,永远18岁,哈哈哈.
delete SamllFriend.age;
SmallFriend.age;//undefined
```
+ 3. js对象的属性查询
判断js里面属性查询,通过in来进行查询这个属性的存在与否,

伪代码③:

```
var SmallFriend=
{   name='小何',
    age='18',
    handsome='yes',
    arethin='strong'
}
'height' in SamllFriend;//false
'handsome' in SmallFriend;//true
```

判断一个对象是否是自身拥有还是继承,可以通过hasOwnProperty()函数来判断,

伪代码④

```
var SmallFriend =
 {
    name: '小何'
};
SmallFriend.hasOwnProperty('name'); // true
SmallFriend.hasOwnProperty('toString'); // false
```
## js的条件判断
+ 1. JavaScript使用if () { ... } else { ... }来进行条件判断。可以写一个简单的列式,可以做一个小算法,对不起
变量系列是不能做成算法的,误会误会.

伪代码⑤:
```
var Example=99
if(example<100)
{
    alter('破双数');
}
else(examle>100)
{
    alter('广义上的实数悖论')
}
```

+ 2. js的多条件判断 if ...else ...的组合.


带补充.未完待续.