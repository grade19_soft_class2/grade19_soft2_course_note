# 这是一位老鸽子经常拖欠一周笔记总结,但是总是会补充的.因为这个是必做系列
**虽迟鸽到**

+ 1 js的变量作用域和函数解析域

在js的变量作用域
+ 全局作用域全局都可以访问;
+ 局部作用域只有在局部才能够访问.
+ 在JS中一个函数作用域就是一个局部作用域.

且 **变量作用域又分为 静态作用域  和  动态作用域**

+ 一个方面是我们看到一个变量定义的时候,我们要知道这个变量的生命周期和作用范围是什么,是在整个全局变量一直出现,还是只是出现在一个函数式中.
+ 另一方面，当我们看到一个变量时,我们要知道引用的是哪里定义的变量，也就是说我们要找到这个变量的值.

然后全局作用域的变量**window**,不在任何函数内定义的变量就具有全局作用域。实际上，JavaScript默认有一个全局对象window，全局作用域的变量实际上被绑定到window的一个属性.

**ohhhhhhhhhhhhhhhhhhhhhhhhhhhhh!**


+ 2. js的变量提升
**变量提升**
+ 因为写函数的变量的时候,要注意变量是否被提升,有时变量在后面声明赋值,提前打印输出会显示undefined,(这好吗?这不好),所以我们每一次进行写代码变量都要提前声明.
+ 我们在函数内部定义变量时，请严格遵守“在 **函数内部首先申明所有变量** ”这一规则
+ 如:最常见的做法是用一个var申明函数内部用到的所有变量



```
function test () {
    console.log(a);  //undefined
    var a = 123; 
};
test();
```

+ 3. js的命名空间
js的命名空间要注意名字间是否有相同的命名的函数或者变量,不然会造成冲突,对的.就是这样.
+ 解决办法:减少冲突的一个方法是把自己的所有变量和函数全部绑定到一个全局变量中。
+ 把自己的代码全部放入唯一的名字空间MYAPP中，会大大减少全局变量冲突的可能。
伪代码

```
// 唯一的全局变量MYAPP:
var MYAPP = {};

// 其他变量:
MYAPP.name = 'myapp';
MYAPP.version = 1.0;

// 其他函数:
MYAPP.foo = function () {
    return 'foo';
};
```

js的局部作用域:由于JavaScript的变量作用域实际上是函数内部,我们在for循环等语句块中是无法定义具有局部作用域的变量的

```
'use strict';

function foo() {
    for (var i=0; i<100; i++) {
        //
    }
    i += 100; // 仍然可以引用变量i
}
```

引入新的函数let(),let可以声明一个块级作用域的变量

```
'use strict';

function foo() {
    var sum = 0;
    for (let i=0; i<100; i++) {
        sum += i;
    }
    // SyntaxError:
    i += 1;
}
```
在ES6标准引入了新的关键字const来定义常量，const与let都具有块级作用域

 然后const跟var一样,再写常量的时候,变量名大写,也是在函数的局部引用.

 + 4. Js的结构赋值
 结构赋值就是可以对一个数组同时进行赋值;
 对于有多层嵌套层次和位置的数组进行结构赋值的时候要对应其位置;
 还要就是例如说我们可以对一个数组的元素进行赋值的时候,可以跳过我们不需要的元素进行赋值
 ```
 'use strict'
  let [,,,,demo1]=[,,,,99]; //①
 ```
 对一个数组如果要抽取若干个属性的话,也可以使用解构赋值,快速获取属性
 ```
 'use strict'

 var friend =
 {
     name='CatNine',
     age=20,
     gender='male',
     height='180',
     weight='155'
 }
 var {name,age,height}=friend;
 console.log('friend name:'+name+'his age'+age+'his height'+height);
 ```
 

 + 5. js的方法
 方法method和函数function不是一个东西,var function demo1() {} and var demo2 ={} 
 开头就不是一个东西了懂?.
  js的方法之this方法
  this是一种指向性的特殊变量,它可以一直指向当前的对象而不做出改变,而且this在函数中如果没有变量作为接受this,那么this就会乱指,指到window或者其他的,就还是很奇怪的(奇奇怪怪),然后要让this函数定向指定的话呢,还需要使用apply()
 + 要指定函数的this指向哪个对象，可以用函数本身的apply方法，它接收两个参数，第一个参数就是需要绑定的this变量，第二个参数是Array，表示函数本身的参数。锵锵锵锵~~~
用apply修复getAge()调用：
```
function getAge() {
    var y = new Date().getFullYear();
    return y - this.birth;
}

var xiaoming = {
    name: '小明',
    birth: 1990,
    age: getAge
};

xiaoming.age(); // 25
getAge.apply(xiaoming, []); // 25, this指向xiaoming, 参数为空
```
 
 另一个与apply()类似的方法是call()，唯一区别是：

+ apply()把参数打包成Array再传入；

+ call()把参数按顺序传入。

比如调用Math.max(3, 5, 4)，分别用apply()和call()实现如下：
```
Math.max.apply(null, [3, 5, 4]); // 5
Math.max.call(null, 3, 5, 4); // 5
```
对普通函数调用，我们通常把this绑定为null.

+ 6. js的装饰器
利用apply()，我们还可以动态改变函数的行为。

JavaScript的所有对象都是动态的，即使内置的函数，我们也可以重新指向新的函数。

现在假定我们想统计一下代码一共调用了多少次parseInt()，可以把所有的调用都找出来
教程给定的
```
'use strict';

var count = 0;
var oldParseInt = parseInt; // 保存原函数

window.parseInt = function () {
    count += 1;
    return oldParseInt.apply(null, arguments); // 调用原函数
};
// 测试:
parseInt('10');
parseInt('20');
parseInt('30');
console.log('count = ' + count); // 3
```
+ 7. js的高阶函数
js的高阶函数有点类似于我所认为C里面的指针,嗯,就差不多是这样.