# 啊啊啊啊今天是12/14号,补充12/11号的笔记,完了补了太少

+ 1. 高阶函数
概念: JavaScript的函数其实都指向某个变量。既然变量可以指向函数，函数的参数能接收变量，那么一个函数就可以接收另一个函数作为参数，这种函数就称之为高阶函数。

我的理解是说高阶函数就是很像C的指针,将一个函数的变量的值设置为另一个函数的变量.

伪代码: 
```
function demo(x,y,v)
{
    return v(x) + return v(y);
}
add(-99,100,math.abs) 

x=-99;
y=100;
v=math.abs;//math.abs 这个是取;两个函数的双
return v;//199

```

论文:MapReduce: Simplified Data Processing on Large Clusters  就可以搞清楚(map/reduce)的概念

+ 2. js的高阶函数map()
 高阶函数map就是去引用一个数组,然后可以通过我写的函数体的函数式实现我想要的结果,map()接收函数体来返回新的数组返回新的值.
**算了还是写个伪代码吧**

```
'use strict';

function demo1(x) {
    return x * x * x;
}
var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
var results = arr.map(demo1); // [1, 8, 27, 64, 125, 196, 343,512,729]
console.log(results);
```

+ 3. js的高阶函数reduce()
reduce的用法。Array的reduce()把一个函数作用在这个Array的[x1, x2, x3...]上，这个函数必须接收两个参数，reduce()把结果继续和序列的下一个元素做累积计算
reduce() 方法接收一个函数作为累加器，数组中的每个值（从左到右）开始缩减，最终计算为一个值。

reduce() 可以作为一个高阶函数，用于函数的 compose。

注意: reduce() 对于空数组是不会执行回调函数的。

伪代码实例

```
'use strict' 
var arr = [1, 3, 5, 7, 9];
arr.reduce(function (x, y) {
    return x + y;
}); // 25

```

+ 4. js的高阶函数
filter()接收的回调函数，可以有多个参数。通常我们仅使用第一个参数，表示Array的某个元素。回调函数还可以接收另外两个参数，表示元素的位置和数组本身：
和map()类似，Array的filter()也接收一个函数。和map()不同的是，filter()把传入的函数依次作用于每个元素，然后根据返回值是true还是false决定保留还是丢弃该元素。
在一个Array中，删掉偶数，只保留奇数.

```
var arr = [1, 2, 4, 5, 6, 9, 10, 15];
var r = arr.filter(function (x) {
    return x % 2 !== 0;
});
r; // [1, 5, 9, 15]
```

把一个Array中的空字符串删掉，可以这么写：
```
var arr = ['A', '', 'B', null, undefined, 'C', '  '];
var r = arr.filter(function (s) {
    return s && s.trim(); // 注意：IE9以下的版本没有trim()方法
});
r; // ['A', 'B', 'C']
```
可见用filter()这个高阶函数，关键在于正确实现一个“筛选”函数。
