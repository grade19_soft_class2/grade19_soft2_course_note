# 昨天是2020/12/10日 12/11日才补的笔记
因为昨天在部署网站,(嗯!还是要把自己的网站做好,哈哈哈,顺便把自己上课做啥的记下来.)

+ 1. js的方法

js的方法和函数不是一样的,而且还是要搞清楚很多东西.

方法的伪代码实例 **(错误实例!)**

```
function GetAge() 
{
 var y=new Data().GetFullYear();
 return y-this.birth;
}
var Az=
{
    name:'阿张',
    birth:2000,
    age:GetAge
};
Az.age();
GetAge();
```

正确实例是要有this方法,而且this方法要指定明确,不然还是会报错.
this函数还是很像C里面的指针,this指定的这个函数,获取这个函数的值,然后在后面输出时,进行打印.
输出的时候,还要看是否为严格模式,因为不是严格模式的话,会指定到别的地方,window或者undefined.

正确实例

```
'use strict'
var Az=
{
    name:'阿张',
    birth:'2000',
    age:function()
    {
         var that =this;//o!在函数内获得方法.
         function() getBirthYear()
         {
             var y=new Data().GetFullYear();
             return y-that.birth;//用that而不是this 
         }
    }
}
```

+ 2.js的apply方法
要指定函数的this指向哪个对象，可以用函数本身的apply方法，它接收两个参数，第一个参数就是需要绑定的this变量，第二个参数是Array，表示函数本身的参数。
另一个与apply()类似的方法是call()，唯一区别是：

+ apply()把参数打包成Array再传入；

+ call()把参数按顺序传入。
