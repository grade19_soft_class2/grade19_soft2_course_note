# 2020 12/03 js笔记
今天呢,上课之前下了个软件,查了一下一些东西,好家伙,直接没法控制我的电脑,算了算了,有文档我怕个🔨,反正上与不上都是要看文档的,没差.
好惹,我现在开始学习,先、一个个看吧.
+ 1.map和set

map和set是新的数据类型,是基于ES6最新规范进行拓展

```
'use strict'
var m=new map();
var s=nwe set();
console.log('浏览器支持使用ES6的数据规范,可以使用map()和set()函数');
```
+ 2. map函数
map函数是一对键值对的结构,对于我们的属性字符串可以快速查找我们需要的值.
```
var name =['烤鸭','姜母鸭,'卤鸭','白切鸭','啤酒鸭','黄焖鸭'];
var time =['2.5h','3h','8h','2h','2h','2h'];
```

这个是copy下来的old date,

这个也是copy下来的但是是根据map的函数来new的
```
var name =new map([['烤鸭','2.5h'],['姜母鸭','3h'],['卤鸭','8h'],['白切鸭','2h'],['啤酒鸭',['2h']],['黄焖鸭','2h']]);
name.get('姜母鸭');// return 3h

```

初始map函数还需要一个二维数组,so 需要具有以下形式

new一个

```
var n=new map();//空的二维数组
n.set('y',99);//添加新的key-value
n.set('w',98);
n.set('q',97);
n.has('y');//判断是否存在key:'y':ture,
n.get('y');
n.delete('y');//删除y;
n.get('y');//undefined;
```
**由于一个key只能对应一个value，所以，多次对一个key放入value，后面的值会把前面的值冲掉。so,接下来的话就是说也不会有重复的值**

+ 3. set函数
set也是具有快速搜索的一种函数,是key的一种集合,但是不存储value,(说明map函数存储value),而且也一样不存在重复的值.

创建一个新的set函数

```
var n=new set([1,2,3,4,5,'6']);//空的
console.log('n');
```

通过新的add(key)函数添加到set函数里面的值,可以重复添加,没有效果,那我要干🔨.(bushi)
n.add(4);
n;//{1,2,3,4}

然后可以通过delete(key)进行删除

+ 4. 然后接下来是循环的知识点,循环的语句有:①for循环,②for in 循环 ,③while循环 ,④do...while循环.

for循环就很普通,也就是利用索引号来遍历数组,如果我们的for循环没有break语句,然后对它进行停止,会浪费电脑资源,一直浪费下去. 要注意(需要进行有结束语句);

然后是for in 循环,for in 循环,我的理解是,建立一个数组,然后对这个数组的变量进行赋值,然后我们可以多次设置我们想要的变量,在for in 循环里面的话,for (var key in 数组名){ '打印数组'},我感觉for    in 循环得到的是变量里面的键值对,就是感觉起来直接获得定义的属性,|| 遇到我们需要过滤掉继承的属性对象,通过hasOwnPreperty()函数来实现;


```
var n =
{name='鸭鸭';
 age='20';
 city='厦门';
 University='福商';
 height='175';
}
for(var key in n)
{
 if(n.hasOwnPreperty(key))
 {
     console.log(key);//然后就这些差不多了.
 }   
}
```
**请注意，for  in循环对Array的循环得到的是String类型而不是Number类型,果然还是键值对。**

while循环,我们学习while循环已经很多次了,for循环是在已知初始条件和结束条件进行循环比较实用,而while循环是对没有比较清晰逻辑的条件下进行循环是比较适合的.
so,例如我们需要进行别累加循环亦或者是累乘循环都是比较合适的,知道条件不满足在跳出循环,又或者是满足条件后退出循环.while循环比较适合某些函数,前提是需要去了解的.

do(...) while 循环是在每次循环完之后再来进行判断条件,满足就可以,不满足就不要.

```
var demo=
['啊这','不是吧,啊sir','啊,就这就这就这','差不多得了','好的好的好的'];
-------------

var n=100;
do{n=n-1;}
while(n>=1);
console.log(n);
```

+ 5. 然后是interable类型
map(),set(),array(),也是属于interable的类型,具有interable的类型集合可以通过新的循环,for...of的循环遍历.
for...in 循环遍历的数组里面的对象的属性名称,不管你是数组还是变量,选出来的都是键值对.

+ 6. 总结: 就差不多这样,感觉就还是很好接受的,不过周末的话我再来进行html5+js代码测试,进行总结.