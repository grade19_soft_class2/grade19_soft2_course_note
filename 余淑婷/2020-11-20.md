### 11-20的笔记
 
## ![a](./imgs/2020-11-22.jpg)

## 递归斐波那契
 斐波那契数列为1、1、2、3、5、8、13、21、34……此数列从第3项开始，每一项都等于前两项之和
 递推公式为F(n)=F(n-1)+F(n-2)，n≥3，F(1)=1，F(2)=1
 ------
static void Main(string[] args)

        {

            Console.WriteLine(F(10));
            
        }

 ---- 斐波那契数

        public static int F(int n)
        {
            if (n < 2)
            {
                return n;
            }
            else
            {
                return F(n - 1) + F(n - 2);   ---递归语句
            }
        }

## ceilling floor这两个方法的区别
            ceilling是如果有小数就往上加1
           var q= Math.Ceiling(2.2889512);
            Console.WriteLine(q); 输出3
-------
            floor是如果有小数就不管，就个位是什么他就还是什么
            var w = Math.Floor(6.59874);
            Console.WriteLine(w);输出6

## 关于日期，给出任意的日期值，计算当月第一天、当月最后一天是多少号，并且计算给出日期值是当月第几周

            DateTime dt = new DateTime(2020,11,22);
            Console.WriteLine("当前日期为：{0}",dt);
            Console.WriteLine("当前为本月的第几天：" + dt.Day);
            Console.WriteLine("本月的第一天为：{0}", dt.AddDays(1 - dt.Day));
            Console.WriteLine("本月的最后一天为：{0}", dt.AddMonths(1).AddDays(1 - dt.Day).AddDays(-1));

#### 并且计算给出日期值是当月第几周 我不会这个
            if(dt.Day>=1 && dt.Day <= 7)
            {
                Console.WriteLine("这个日期为本月的第一周");
            }else if (dt.Day>=8 && dt.Day <= 15)
            {
                Console.WriteLine("这个日期为本月的第二周");
            }else if (dt.Day>=16 && dt.Day <= 22)
            {
                Console.WriteLine("这个日期为本月的第三周");
            }else if (dt.Day>=23 && dt.Day <= 29)
            {
                Console.WriteLine("这个日期为本月的第四周");
            }
            else
            {
                Console.WriteLine("这个日期为本月的第五周");
            }
            Console.ReadKey();