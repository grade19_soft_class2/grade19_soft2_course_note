# 2020-12-04的笔记
## 定义函数
    functione是定义函数的的格式，在函数名称的前面；abs是自己定义的函数的名字，相当于给自己的东西取名字的样子；（）里面是函数的参数，如果有很多个参数要用,来分隔开；{ ... }之间的代码是函数体，可以包含若干语句，甚至可以没有任何语句  
        *返回绝对值
            第一种方法
                function abs(x){
                if(x>=0){
                    return x;
                }else if (x<0){
                    return -x
                }   
            }
            console.log((abs(7))); 
            console.log(abs(-20)) //调用函数

            第二种方法
            var abs2 = function(x){
                if(x>=0){
                    return x
                }else if (x<0){
                    return -x  函数体内部的语句在执行时，一旦执行到return时，函数就执行完毕，并将结果返回 如果没有return 执行完毕之后也会返回结果 不过结果是undefined
                }
            }
            console.log(77);
            console.log(-59) //调用函数

        *自己做的示例
                function abs3(x){
                if(x>0){
                    console.log(x+"是一个正数")
                }else if (x<0){
                    console.log(x+"是一个负数")
                }else if (x=0){
                    console.log(x+"是0");
                }
            }
            console.log(abs3(55))

## arguments
            function foo(x){
            console.log('x='+x);
            for (var i=0;i<arguments.length;i++){
                console.log('ary'+i+'='+arguments[i]) i是ary的下标
            }
        }
            console.log(foo(1,2,3,4))  因为这个方法没有return 所以他在最后会返回undefined


            function abs() {
            if (arguments.length === 0) {
                return 0;
            }
            var x = arguments[0];
            return x >= 0 ? x : -x;
        }
        console.log(abs())      因为没有内容，所以arguments.length的长度=0,上面的if是如果长度等于0 就返回0 所以返回0
        console.log(abs(11))    因为在abs里面定义了一个11，所以arguments.length的长度=1 不等于0，所以执行下面的代码 如果x>=0就返回x 如果x<=0就返回--x
        console.log(abs(-20));  负的-20等于20

            function abs4(a,b,c){
            if(arguments.length<=2){
                c=b;//把b传值给c
                b=null//把null值传给b
            }
            console.log(a,b,c);
    
        }
        abs4(1,2)

## rest参数
        function foo(a,b,...rest){
            console.log(a)
            console.log(b)
            console.log(rest)
            }
        foo(1,2,3,4,5)//输出1，2，[3,4,5] ...rest的部分会一起组成一个数组
        foo(1)//输出1 剩下的就是输出undefined 未定义

## 练习
    //定义一个计算圆面积的函数area_of_circle()，它有两个参数：
    //r: 表示圆的半径；
    //pi: 表示π的值，如果不传，则默认3.14

    function area_of_circle(r, pi) {
        if(pi === 3.14){
            console.log(`圆的面积为：${pi*r*r}`);
        }
        else{
            console.log(`圆的面积为：${3.14*r*r}`); 
        }       
    }
    area_of_circle(2)//圆的面积为：12.56


    //小明是一个JavaScript新手，他写了一个max()函数，返回两个数中较大的那个：

    function max(a, b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }
    console.log(max(15, 20)); 小明错是因为他return之后没有写 a 而是回车再写了a return; // 自动添加了分号，相当于return undefined;

## 变量的作用域
1. 变量如果在一个函数体里面声明，则该变量的作用域就在这这个函数体，函数体外无法使用；
2. 变量1在一个函数体1里面声明，函数体1内还有一个函数2，该函数2的函数体可以调用变量1，但是该函数2的函数体内的变量不能给函数1使用；

    function foo(){
        var x = 10;
        function too{
            var y = x + 10; //可以使用变量 a
            console.log(y);
        }
        too();
        var a = y + 1 //则无法使用变量 y
    }


## 如果内部函数和外部函数的变量名重名怎么办

    function foo(){
        var x = 10;
        function too(){
            var x = 60;
            var y = x + 10;
            console.log(y);
        }
        too();
    }
    foo();

    调用函数too()函数体内的x;


## 变量提升
    先输出y再声明变量y

    function foo(){
        console.log(y); //undefined
        var y = 20;
    }

    会将变量y提到前面，但不会将y的赋值一起提到前面
    JavaScript引擎看到的代码相当于：
    ```
    function foo() {
        var y; // 提升变量y的声明，此时y为undefined
        console.log(x);
        y = 20;
    }
    ```
