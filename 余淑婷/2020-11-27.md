### 2020-11-27的笔记 毁灭吧 我累了

        //unshift和shift  如果要往Array的头部添加若干元素，使用unshift()方法，shift()方法则把Array的第一个元素删掉
        var arr=[1,2,3]
        arr.unshift('a','b')//在arr数组里面第0项的前面增加 a ,b 两个元素
        console.log(arr)
        arr.shift()//删除a
        console.log(arr)

        //sort  sort()可以对当前Array进行排序，它会直接修改当前Array的元素位置，直接调用时，按照默认顺序排序 
        var brr=[6,5,4,3,2,1]
        brr.sort()//如果brr里面是[1,2,3,4,5,6]则里面的顺序不会变化
        console.log(brr)

        //reverse 把整个Array的元素给调个个，也就是反转
        var crr=['fish','chincken','pig','baboon']
        crr.reverse()//把整个crr的元素给调个个，也就是反转
        console.log(crr)

        //splice  splice()方法是修改Array的“万能方法”，它可以从指定的索引开始删除若干元素，然后再从该位置添加若干元素
        var drr=['red','pink','blue','yello','purle']
        drr.splice(0,2,'black','green')// 从索引0开始删除2个元素,然后再添加两个元素
        console.log(drr)//输出["black", "green", "blue", "yello", "purle"]
        drr.splice(3,2)//只删除 不添加 从第三项开始，删除2个元素
        console.log(drr)//输出["black", "green", "blue"]
        drr.splice(2,0,'orange','white')// 只添加 不删除  从第二项开始添加元素
        console.log(drr)//输出["black", "green", "orange", "white", "blue"]

        //concat  concat()方法把当前的Array和另一个Array连接起来，并返回一个新的Array
        var err=[4,5,6]
        var frr=err.concat([7,8,9])//concat()方法并没有修改当前Array
        console.log(err)//输出[4,5,6]
        console.log(frr)//输出[4,5,6,7,8,9]

        //join join()方法 它把当前Array的每个元素都用指定的字符串连接起来，然后返回连接后的字符串
        var grr=[11,22,33,44,55,66]
        console.log(grr.join('~'))//并没有改变数组 只是把输出的样子改变了 所以要这样子用
        console.log(grr)//因为上面的join没有改变数组，所以再次输出还是[11,22,33,44,55,66]

        //多维数组 如果数组的某个元素又是一个Array，则可以形成多维数组
        var hrr=[[10,20,30],[100,200,300],'~o~']
        console.log(hrr)
