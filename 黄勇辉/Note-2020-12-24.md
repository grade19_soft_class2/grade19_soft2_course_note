### 今日笔记
# 构造函数
```
function Animal(prop){
    this.name=prop.name || 'pp'
}

let dog=new Animal({name:''})
console.log(dog.name);
```

## 原型继承
```
// PrimaryStudent构造函数:
function PrimaryStudent(props) {
    Student.call(this, props);
    this.grade = props.grade || 1;
}

// 空函数F:
function F() {
}

// 把F的原型指向Student.prototype:
F.prototype = Student.prototype;

// 把PrimaryStudent的原型指向一个新的F对象，F对象的原型正好指向Student.prototype:
PrimaryStudent.prototype = new F();

// 把PrimaryStudent原型的构造函数修复为PrimaryStudent:
PrimaryStudent.prototype.constructor = PrimaryStudent;

// 继续在PrimaryStudent原型（就是new F()对象）上定义方法：
PrimaryStudent.prototype.getGrade = function () {
    return this.grade;
};

// 创建xiaoming:
var xiaoming = new PrimaryStudent({
    name: '小明',
    grade: 2
});
xiaoming.name; // '小明'
xiaoming.grade; // 2

## class继承

+ 用一个class 来编写一个构造函数如下

```
class Student{
    constructor(name){
        this.name=name;
    }
    hello(){
        console.log('Hello,' + this.name + '!');
    }
}

