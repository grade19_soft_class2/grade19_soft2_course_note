## 今日笔记
##lambda表达式
1. 查询数据的时候直接是使用以下箭头的形式来表示查询语句的：=>，即可用Studentlist.Where(t=>t.ClassCode=‘1001’)语句来直接完成，无需再写繁琐的foreach语句或者for循环。Lambda表达式的运算符即为=>。

2. Lambda表达式的表现形式

       表达式形式：(Input Param)=>Expression。在表达式左侧的表示输入参数，右侧的为相应的运算语句或者判断语句等，可包含函数调用等复杂方式。运算符=>读作为goes to，例如下面这个表达t=>t.ClassCode='1001'，多做goes to ClassCode equal 1001。

3. 
     studentList对象：此对象是一个List集合，集合中的对象为学生实体Student。此集合中存放着整个学校学生的信息。

     scoreList对象：此对象是个List集合，集合中的对象是成绩实体Score，此集合中存放着为学生的成绩信息。

     Student实体：此实体包含下列几个属性，StudentName，StudentCode，ClassCode，ClassName，BirthDay，Grade。以上几个英文单词都比较简单，就不做解释了。

     Score实体：此实体包含下列几个属性，StudentCode,SubjectName(科目名称),ScoreValue(分数，0-100的数字)。一个学生可能有多门成绩数据存放在此。       


     ## 练习

     static void Main(string[] args)        
　　　　{
　　　　　　Console.WriteLine("请输入要输出斐波那契数列哪一项的数值：");
　　　　　　int number = Convert.ToInt32(Console.ReadLine());//接收用户从键盘输入的字符，并将其转换为整形
　　　　　　Console.WriteLine("斐波那契数列的第{0}项的数值为：{1}", number, digui(number));
　　　　　　Console.ReadKey();
　　　　}
　　　　public static int digui(int sum) // 定义一个静态的递归函数
　　　　{
　　　　　　if (sum == 1 || sum == 2) //若sum的值等于1或2，则返回1
　　　　　　{
　　　　　　　　return 1;
　　　　　　}
　　　　　　else
　　　　　　{
　　　　　　　　return sum = digui(sum - 1) + digui(sum - 2); //若sum的值大于2，则返回（sum-1）和（sum-2）的和
　　　　　　}
　　　　}
```



2. Math下，ceilling floor这两个方法的有何区别。

+ Ceiling	返回大于或等于指定的双精度浮点数的最小整数值
+ Floor	返回小于或等于指定的双精度浮点数的最大整数值

```
            Console.WriteLine("请输入第一个数："); //输入5.22
            double num1 = double.Parse(Console.ReadLine());
            Console.WriteLine("请输入第二个数："); //输入5.22
            double num2 = double.Parse(Console.ReadLine());
            Console.WriteLine("第一数为{0}",Math.Ceiling(num1));//输出 6
            Console.WriteLine("第二个数为{0}",Math.Floor(num2));//输出 5
            Console.ReadKey();
```
+ Ceiling 返回大于本身的数四舍五入取整。
+ Floor 返回小于本身的数，化繁为简。



3. 关于日期，给出任意的日期，计算当月第一天，当月最后一天是多少号，并且计算给出日期值是当月第几周。
```
             DateTime now = DateTime.Now;
            //当月的第一天
            DateTime firstDay = new DateTime(now.Year,now.Month,1);

            //当月的最后一天
            DateTime lastDay = firstDay.AddMonths(1).AddDays(-1);

            Console.WriteLine(firstDay);
            Console.WriteLine(lastDay);
            Console.ReadKey();