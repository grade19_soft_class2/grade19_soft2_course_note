### 今日笔记

## 字符串
### JavaScript的字符串就是用''或""括起来的字符表示。
+ 如果'本身也是一个字符，那就可以用""括起来
+ 字符串内部既包含'又包含",用转义字符\
```
"i\'m \"ok\""
```
表示的内容是：i'm"ok"
### 多行字符串
+ 表示方法，用反引号 `... ` 表示：
```
`这是一个
多行
字符串`;
### 操作字符串
```
var s = 'Hello, world!';
alert(s[0]); 
alert(s[1]); 
```

#### toUpperCase

toUpperCase()把一个字符串全部变为大写：
```
var s = 'Hello';
s.toUpperCase(); // 返回'HELLO'
 alert( s.toUpperCase())
```
#### toLowerCase
toLowerCase()把一个字符串全部变为小写：
```
var s = 'Hello';
var lower = s.toLowerCase(); 
alert(lower);
```
#### indexOf
indexOf()会搜索指定字符串出现的位置：
```
var s = 'hello, world';
alert(s.indexOf('world'))// 返回7
alert(s.indexOf('World')) // 没有找到指定的子串，返回-1
```

#### substring
substring()返回指定索引区间的子串：
````