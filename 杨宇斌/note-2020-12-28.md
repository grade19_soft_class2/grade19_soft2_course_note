## 插入DOM
+ appendChild() 方法向节点添加最后一个子节点。
```
html 文件
    <p id="js">JavaScript</p>
<div id="list">
    <p id="java">Java</p>
    <p id="python">Python</p>
    <p id="scheme">Scheme</p>
</div>

js 文件
'use strict';

var js=document.getElementById("js");
var list=document.getElementById("list");
list.appendChild(js);
```
+ createElement() 方法通过指定名称创建一个元素
```
html 文件
    <p id="js">JavaScript</p>
<div id="list">
    <p id="java">Java</p>
    <p id="python">Python</p>
    <p id="scheme">Scheme</p>
</div>

js 文件
'use strict';

var js=document.getElementById("js");
var go=document.createElement("go");
//定义'go'节点的id
go.id='go';
//添加html中的'go'
go.innerHTML='go';
//向节点添加最后一个子节点
list.appendChild(go);
```
insertBefore() 方法在您指定的已有子节点(之前)插入新的子节点。
```
html 文件
<div id="list">
    <p id="java">Java</p>
    <p id="python">Python</p>
    <p id="scheme">Scheme</p>
</div>
js 文件
'use strict';

var list=document.getElementById("list");
var scheme = document.getElementById('scheme');
var go=document.createElement("go");
//定义'go'节点的id
go.id='go';
//添加html中的'go'
go.innerHTML='go';
//将go节点插入到scheme节点的前面
list.insertBefore(go,scheme);

显示效果：
Java

Python

go

Scheme
```
+ children 属性返回元素的子元素的集合，是一个 HTMLCollection 对象。
```
html 文件
 <div id="list">
        <p id="java">Java</p>
        <p id="haskell">Haskell</p>
        <p id="python">Python</p>
        <p id="scheme">Scheme</p>
</div>

js 文件   

var list = document.getElementById("list");
var arr=[];

for(let i=0;i<list.children.length;i++){
   
    arr.push(list.children[i]);
}
console.log(arr);
```
练习
对于一个已有的HTML结构：
```
Scheme
JavaScript
Python
Ruby
Haskell
<!-- HTML结构 -->
<ol id="test-list">
    <li class="lang">Scheme</li>
    <li class="lang">JavaScript</li>
    <li class="lang">Python</li>
    <li class="lang">Ruby</li>
    <li class="lang">Haskell</li>
</ol>
```
按字符串顺序重新排序DOM节点：
```
'use strict';
// sort list:

// 测试:
;(function () {
    var
        arr, i,
        t = document.getElementById('test-list');
    if (t && t.children && t.children.length === 5) {
        arr = [];
        for (i=0; i<t.children.length; i++) {
            arr.push(t.children[i].innerText);
        }
        if (arr.toString() === ['Haskell', 'JavaScript', 'Python', 'Ruby', 'Scheme'].toString()) {
            console.log('测试通过!');
        }
        else {
            console.log('测试失败: ' + arr.toString());
        }
    }
    else {
        console.log('测试失败!');
    }
})();
```
```
var list=document.getElementById("test-list");
var li=list.children;
var arr=[];
    for(let i=0;i<li.length;i++){
        arr.push(li[i].innerText)
}
arr.sort();
for(let j=0;j<li.length;j++){
    li[j].innerText=arr[j];
}
```
## 删除DOM
+ removeChild() 方法指定元素的某个指定的子节点。
```
// 拿到待删除节点:
var list=document.getElementById('test-list');
// 拿到父节点:
var par=list.parentElement;
// 删除:
var rem=par.removeChild(list);

console.log(rem===list);//true
```
+ children 属性返回元素的子元素的集合，是一个 HTMLCollection 对象。
```
html 文件
    <ol id="test-list">
        <li class="lang">Scheme</li>
        <li class="lang">JavaScript</li>
    </ol>
js 文件
var list=document.getElementById('test-list');

list.removeChild(list.children[0]);
list.removeChild(list.children[1]);
浏览器报错
节点被删除后，节点数量已经从2变为了1，索引[1]已经不存在了
```
练习
JavaScript
Swift
HTML
ANSI C
CSS
DirectX
```
<!-- HTML结构 -->
<ul id="test-list">
    <li>JavaScript</li>
    <li>Swift</li>
    <li>HTML</li>
    <li>ANSI C</li>
    <li>CSS</li>
    <li>DirectX</li>
</ul>
```
把与Web开发技术不相关的节点删掉：
```
'use strict';
// TODO

// 测试:
;(function () {
    var
        arr, i,
        t = document.getElementById('test-list');
    if (t && t.children && t.children.length === 3) {
        arr = [];
        for (i = 0; i < t.children.length; i ++) {
            arr.push(t.children[i].innerText);
        }
        if (arr.toString() === ['JavaScript', 'HTML', 'CSS'].toString()) {
            console.log('测试通过!');
        }
        else {
            console.log('测试失败: ' + arr.toString());
        }
    }
    else {
        console.log('测试失败!');
    }
})();
```
## 操作DOM
```
var list=document.getElementById('test-list');
list.removeChild(list.children[1]);
list.removeChild(list.children[2]);
list.removeChild(list.children[3]);

var list=document.getElementById('test-list');
for(let i=1;i<4;i++){
    list.removeChild(list.children[i])
}
```
+ document.getElementById()可以直接定位唯一的一个DOM节点。document.getElementsByTagName()和document.getElementsByClassName()总是返回一组DOM节点。要精确地选择DOM，可以先定位父节点，再从父节点开始选择，以缩小范围。

+ querySelector()和querySelectorAll()
详情请见 note-2020-12-25.md

练习
如下的HTML结构：

JavaScript

Java

Python

Ruby

Swift

Scheme

Haskell
```
<!-- HTML结构 -->
<div id="test-div">
<div class="c-red">
    <p id="test-p">JavaScript</p>
    <p>Java</p>
  </div>
  <div class="c-red c-green">
    <p>Python</p>
    <p>Ruby</p>
    <p>Swift</p>
  </div>
  <div class="c-green">
    <p>Scheme</p>
    <p>Haskell</p>
  </div>
</div>
```
请选择出指定条件的节点：
```
'use strict';
// 选择<p>JavaScript</p>:
var js = ???;

// 选择<p>Python</p>,<p>Ruby</p>,<p>Swift</p>:
var arr = ???;

// 选择<p>Haskell</p>:
var haskell = ???;

// 测试:
if (!js || js.innerText !== 'JavaScript') {
    alert('选择JavaScript失败!');
} else if (!arr || arr.length !== 3 || !arr[0] || !arr[1] || !arr[2] || arr[0].innerText !== 'Python' || arr[1].innerText !== 'Ruby' || arr[2].innerText !== 'Swift') {
    console.log('选择Python,Ruby,Swift失败!');
} else if (!haskell || haskell.innerText !== 'Haskell') {
    console.log('选择Haskell失败!');
} else {
    console.log('测试通过!');
}
```
```
// 选择<p>JavaScript</p>:
var js = document.getElementById("test-p");

// 选择<p>Python</p>,<p>Ruby</p>,<p>Swift</p>:
var arr = document.getElementsByClassName("c-red c-green")[0].children;

// 选择<p>Haskell</p>:
var haskell = document.getElementsByClassName("c-green")[1].lastElementChild;

// 测试:
if (!js || js.innerText !== 'JavaScript') {
    alert('选择JavaScript失败!');
} else if (!arr || arr.length !== 3 || !arr[0] || !arr[1] || !arr[2] || arr[0].innerText !== 'Python' || arr[1].innerText !== 'Ruby' || arr[2].innerText !== 'Swift') {
    console.log('选择Python,Ruby,Swift失败!');
} else if (!haskell || haskell.innerText !== 'Haskell') {
    console.log('选择Haskell失败!');
} else {
    console.log('测试通过!');
}

```
1. 对于getElementByClassName()返回是一个数组，获得第一个正确的为[0]，后续依次类推，
//getElementsByClassName() 方法返回文档中所有指定类名的元素集合，作为 NodeList 对象。
2. children 属性返回元素的子元素的集合，是一个 HTMLCollection 对象。
3. lastChild 属性返回指定节点的最后一个子节点，以 Node 对象。

## 操作表单
+ 文本框，对应的`<input type="text">`，用于输入文本；

+ 口令框，对应的`<input type="password">`，用于输入口令；

+ 单选框，对应的`<input type="radio">`，用于选择一项；

+ 复选框，对应的`<input type="checkbox">`，用于选择多项；

+ 下拉框，对应的`<select>`，用于选择一项；

+ 隐藏文本，对应的`<input type="hidden">`，用户不可见，但表单提交时会把隐藏文本发送到服务器。

## AJAX
```
不刷新的情况下读取数据或提交数据

（最早出现ajax：谷歌地图，拖动一下出现一片新的视野）

应用：用户注册、在线聊天、微博

特性：只能从服务器上去读取数据（所以我们需要配置自己的服务器程序AMP）

Ajax里面文件的编码要和页面的编码一致

缓存、阻止缓存（好处大于缺点，所以不能什么时候都清缓存）
缓存能帮助我们加速网络访问，所谓缓存，就是服务器上这个文件，它只读一次，第二次就从你的硬盘里、缓存里直接去拿，而不是真的通过网络来请求.
```