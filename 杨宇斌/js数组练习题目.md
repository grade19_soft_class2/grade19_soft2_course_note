## 1.题目描述 找出元素 item 在给定数组 arr 中的位置 输出描述: 如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
```
示例
输入  [ 1, 2, 3, 4 ], 3
输出   2
```
```
'use strict'

var arr=[1,2,3,4,5,6,7,8,9];
function indexOf(arr, item) {
    for (var i = 0; i < arr.length; i++){
        if (arr[i] === item){
            return i;
        }
    }
    return -1;
}
console.log(indexOf(arr,9));//8
```
```
'use strict'

var arr=[1,2,3,4,5,6,7,8,9];
function indexOf(arr, item) {
    if(arr.indexOf(item)>=0){
         return arr.indexOf(item)
    }else{
        return -1;
    }
    
}
console.log(indexOf(arr,3));//2
```
## 2.题目描述 计算给定数组 arr 中所有元素的总和 输入描述: 数组中的元素均为 Number 类型
```
示例
输入  [ 1, 2, 3, 4 ]
输出  10
```
```
'use strict'

var arr=[1,2,3,4,5,6,7,8];

function sum(arr){
    var sum=0;
    for(var i=0;i<arr.length;i++){
        sum+=arr[i]
        
    }
    return sum;
}
console.log(sum(arr));//36
```
+ forEach遍历：
```
'use strict'

var arr=[1,2,3,4,5,6,7,8];

function sum(arr) {
    var sum = 0;
    arr.forEach(function(item,index) {
        sum  += item;
    });
  
    return sum;
}
console.log(sum(arr));//36
```
## 3.题目描述 移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
```
示例
输入  [1, 2, 3, 4, 2], 2
输出  [1, 3, 4]
```
```
'use strict'

var arr=[1,2,3,4,5,2,7,8];
arr.length
function test(arr,item) {
   var a=[];
   for(var i=0;i<arr.length;i++){
   //如果arr[i]不等于item，就加入数组a;
   if(item!=arr[i]){
       a.push(arr[i])
     }
   }
   return a;
}
console.log(test(arr,2));// [1, 3, 4, 5, 7, 8]
```
## 4.题目描述 移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
```
示例
输入  [1, 2, 2, 3, 4, 2, 2], 2
输出  [1, 3, 4]
```
```
'use strict'

var arr=[1,2,3,4,5,2,7,8];
function test(arr,item) {
   for(var i=0;i<arr.length;i++){
    if(item==arr[i]){
        arr.splice(i,1);
        i--;	//执行i--，才能保证不会漏掉某个元素
    }

   }
   return arr;
}
console.log(test(arr,2));// [1, 3, 4, 5, 7, 8]
```
## 5.题目描述 在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
```
示例
输入  [1, 2, 3, 4],  10
输出  [1, 2, 3, 4, 10]
```
```
'use strict'

var arr=[1,2,3,4,5,2,7,8];
function test(arr,item) {
    var a=[]
   for(var i=0;i<arr.length;i++){
        a.push(arr[i]);
   }
   a.push(item)
   return a;
}
console.log(test(arr,2));//  [1, 2, 3, 4, 5, 2, 7, 8, 2]
```
## 6.题目描述 删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
```
示例
输入  [1, 2, 3, 4]
输出  [1, 2, 3]
```
```
'use strict'

var arr=[1,2,3,4,5,2,7,8];
function test(arr) {
    var a = new Array();//新的数组
    for (var i = 0;i < arr.length-1;i++){
        a.push(arr[i]);
    }
    return a;
}
console.log(test(arr));//   [1, 2, 3, 4, 5, 2, 7]
```
## 7.题目描述 在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
```
示例
输入  [1, 2, 3, 4], 10
输出  [10, 1, 2, 3, 4]
```
```
'use strict'

var arr=[1,2,3,4,5,2,7,8];
function test(arr,item) {
    var a=new Array();
    a.push(item);
   for(var i=0;i<arr.length;i++){
        a.push(arr[i]);
   }
   
   return a;
}
console.log(test(arr,4));//  [4, 1, 2, 3, 4, 5, 2, 7, 8]
```
## 8.题目描述 删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
```
示例
输入  [1, 2, 3, 4]
输出  [2, 3, 4]
```
```
'use strict'

var arr=[1,2,3,4,5,2,7,8];
function test(arr) {
   var a=arr.slice(0)//这是个生成新数组的方法
   a.shift(arr[0]);
   return a;
}
console.log(test(arr));//  [2, 3, 4, 5, 2, 7, 8]
```
## 9.题目描述 合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
```
示例
输入  [1, 2, 3, 4], ['a', 'b', 'c', 1]
输出  [1, 2, 3, 4, 'a', 'b', 'c', 1]
```
```
'use strict'

var arr1=[1,2,3,4,5,2,7,8];
var arr2=['a','b','c'];
function concat(arr1, arr2) {
    var a = arr1.slice(0);
    var b = arr2.slice(0);
    return a.concat(b);
}
console.log(concat(arr1,arr2));//  [1, 2, 3, 4, 5, 2, 7, 8, "a", "b", "c"]
```
## 10.题目描述 在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
```
示例
输入  [1, 2, 3, 4], 'z', 2
输出  [1, 2, 'z', 3, 4]
```
```
'use strict'

var arr=[1,2,3,4,5,2,7,8];
function insert(arr, item, index) {
    var result = arr.slice(0);
    result.splice(index,0,item);
    return result;
}
console.log(insert(arr,'aer',4));//[1, 2, 3, 4, "aer", 5, 2, 7, 8]
```
## 11.题目描述 统计数组 arr 中值等于 item 的元素出现的次数
```
示例
输入  [1, 2, 4, 4, 3, 4, 3], 4
输出  3
```
```
'use strict'

var arr=[1,2,3,4,5,2,7,8];

function count(arr, item) {
    var a = 0;
    for(var i = 0; i<arr.length; i++){
        if(arr[i] == item){
            a++;  
        }    
    }
    return a;  
}  
console.log(count(arr,2));//2
```
## 12.题目描述 找出数组 arr 中重复出现过的元素
```
示例
输入  [1, 2, 4, 4, 3, 3, 1, 5, 3]
输出  [1, 3, 4]
```
```
不懂看了看+-+
'use strict'

var arr=[1,2,3,4,3,2,7,8];
function duplicates(arr) {
    var result = [];
    arr.forEach(function(item){
        if((arr.indexOf(item) != -1)&&(arr.indexOf(item) != arr.lastIndexOf(item))){
            if(result.indexOf(item) == -1){
                result.push(item)
            }
        }
    });
    return result;
}
console.log(duplicates(arr));//[2, 3]

```
## 13.题目描述 为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
```
示例
输入  [1, 2, 3, 4]
输出  [1, 4, 9, 16]
```
```
'use strict'

var arr=[1,2,3,4,5,2,7,8];

function test(arr) {
    var arr1 = arr.slice(0);
    for (var i = 0;i<arr1.length; i++){
        arr1[i]=arr[i]*arr[i];
    }
    return arr1;
 }      
console.log(test(arr));// [1, 4, 9, 16, 25, 4, 49, 64]
```
## 14.题目描述 在数组 arr 中，查找值与 item 相等的元素出现的所有位置
```
示例
输入  'abcdefabc'
输出  [0, 6]
```
```

```
