# 递归
```
递归是经常在企业笔试中考到的问题，也是一种特殊的执行程序，它是用方法调用自身的形式实现的，让程序代码循环执行。
```
## 递归，使用递归方式，打印斐波那契数列，要求定义一个函数，具有一个参数，指示打印的数列的个数
```
namespace ConsoleApp7
{
   public class LeonardodaFibonacci
    {
        public static int  Fibonacci(int n)
        {
            if (n < 1)
            {
                return 0;
            }
            if (n == 1 || n == 2)
            {
                return 1;
            }
            return Fibonacci(n - 1) + Fibonacci(n - 2);

        }
        public static void Fiallda(int a)
        {
            for (int i = 1; i <= a; i++)
            {
                Console.WriteLine("a({0})={1}\t", i,LeonardodaFibonacci.Fibonacci(i));
            }
        }
    }
}
namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            LeonardodaFibonacci.Fiallda(10);

            Console.ReadLine();
        }
    }
}
```
# 效果如下：
![难啊](./imgs/2020-11-20-1.png)

# Math
```
C# Math 类主要用于一些与数学相关的计算，并提供了很多静态方法方便访问，常用的方法如下表所示。

方法	描述
Abs	取绝对值
Ceiling	返回大于或等于指定的双精度浮点数的最小整数值
Floor	返回小于或等于指定的双精度浮点数的最大整数值
Equals	返回指定的对象实例是否相等
Max	返回两个数中较大数的值
Min	返回两个数中较小数的值
Sqrt	返回指定数字的平方根
Round	返回四舍五入后的值

```
## Math下，ceilling floor这两个方法的区别
```
namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 4.23 / 0.6;

            //返回大于或等于指定的双精度浮点数的最小整数值
            var test = Math.Ceiling(a);
            Console.WriteLine(test);

            //返回小于或等于指定的双精度浮点数的最大整数值
            var test01 = Math.Floor(a);
            Console.WriteLine(test01);

            Console.ReadLine();
        }
    }
}
```
# 效果如下：
![难啊](./imgs/2020-11-20-2.png)

# DateTime类
```
C# DateTime 类用于表示时间

在 DateTime 类中提供了常用的属性和方 法用于获取或设置日期和时间，如下表所示。

方法	描述
Date	获取实例的日期部分
Day	获取该实例所表示的日期是一个月的第几天
DayOfWeek	获取该实例所表示的日期是一周的星期几
DayOfYear	获取该实例所表示的日期是一年的第几天
Add(Timespan value)	在指定的日期实例上添加时间间隔值 value
AddDays(double value)	在指定的日期实例上添加指定天数 value
AddHours(double value)	在指定的日期实例上添加指定的小时数 value
AddMinutes(double value)	在指定的日期实例上添加指定的分钟数 value
AddSeconds(double value)	在指定的日期实例上添加指定的秒数 value
AddMonths(int value)	在指定的日期实例上添加指定的月份 value
AddYears (int value)	在指定的日期实例上添加指定的年份 value
```

```
namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            //关于日期，给出任意的日期值，计算当月第一天、
            // 当月最后一天是多少号，并且计算给出日期值是当月第几周
            Console.WriteLine("请输入你的日期");
            DateTime dt = DateTime.Parse(Console.ReadLine());
            DateTime dt1 = new DateTime(dt.Year, dt.Month, 1);
            Console.WriteLine("这个月的第一天是:" + dt1.ToLongDateString());
            //Console.WriteLine("这个月的第一天是"+dt.AddDays(1-dt.Day).ToLongDateString());

            var dt2 = (dt1.AddMonths(1).AddDays(-1)).ToLongDateString();
            Console.WriteLine("这个月的最后一天是" + dt2);
 
           //Console.WriteLine("这个月的最后一天是:" + dt.AddDays(1-dt.Day).AddMonths(1).AddDays(-1).ToLongDateString());

            double dt3 = dt.Day /(double)7;
            var a = Math.Ceiling(dt3);
            Console.WriteLine("当月第几周:" + a);

       

            Console.ReadLine();
        }
    }
}
```
# 效果如下：
![难啊](./imgs/2020-11-20-3.png)

