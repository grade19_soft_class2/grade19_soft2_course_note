# JavaScript
```
JavaScript是世界上最流行的脚本语言

简单地说，JavaScript是一种运行在浏览器中的解释型的编程语言
```

## 基本
```
1. JavaScript是一种弱类语法
2. 花括号{}内的语句具有缩进，通常有4个空格
3. JavaScript严格区分大小写，如果弄错了大小写，程序将报错或者运行不正常。
4. JavaScript本身对嵌套的层级没有限制，但是过多的嵌套会增加看懂代码的难度。遇到这种情况，需要把部分代码抽出来，作为函数来调用，这样可以减少代码的复杂度。

```
## 简单的语法
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script>
        var i=5;
        let a=40;

        console.log(i);
        console.log(a);
    </script>
</head>
<body>
    
</body>
</html>
```

## 注释
```
// 行注释
/*    */ 块注释
```

# 数据类型

## 1.Number

+ Number类型表示数字,和其他编程语言（如 C 和 Java）不同，JavaScript 不区分整数值和浮点数值，所有数字在 JavaScript 中均用浮点数值表示，所以在进行数字运算的时候要特别注意进度缺失问题。


+ Number.NaN 是一个特殊值，说明某些算术运算（如求负数的平方根）的结果不是数字。方法 parseInt() 和 parseFloat() 在不能解析指定的字符串时就返回这个值。对于一些常规情况下返回有效数字的函数，也可以采用这种方法，用 Number.NaN 说明它的错误情况。

+ JavaScript 以 NaN 的形式输出 Number.NaN。请注意，NaN 与其他数值进行比较的结果总是不相等的，包括它自身在内。因此，不能与 Number.NaN 比较来检测一个值是不是数字，而只能调用 isNaN() 来比较。
# 例：
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script>
        var i=NaN;
        console.log(isNaN(i));
    </script>
</head>
<body>
    
</body>
</html>

```
# 输出结果是 true
+ Infinity; // Infinity表示无限大，当数值超过了JavaScript的Number所能表示的最大值时，就表示为Infinity

+ 当数字运算结果超过了JavaScript所能表示的数字上限（溢出），结果为一个特殊的无穷大（infinity）值，在JavaScript中以Infinity表示。同样地，当负数的值超过了JavaScript所能表示的负数范围，结果为负无穷大，在JavaScript中以-Infinity表示。无穷大值的行为特性和我们所期望的是一致的：基于它们的加、减、乘和除运算结果还是无穷大（当然还保留它们的正负号）。
```
实例
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script>
        var  myNumber=2;
         while (myNumber!=Infinity)
          {
          console.log(myNumber=myNumber*myNumber)    // 重复计算直到 myNumber 等于 Infinity
          }
    </script>
</head>
<body>
</body>
</html>
```
## 2. 布尔值
```
布尔值和布尔代数的表示完全一致，一个布尔值只有true、false两种值，要么是true，要么是false，可以直接用true、false表示布尔值，也可以通过布尔运算计算出来
```
+ &&运算是与运算，只有所有都为true，&&运算结果才是true
+ ||运算是或运算，只要其中有一个为true，||运算结果就是true
+ !运算是非运算，它是一个单目运算符，把true变成false，false变成true：
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script>
        console.log(true&&true); //true
        console.log(false&&false) // false
        console.log(true&&false) //false
        console.log(true||false) //true
        console.log(!true) //false
    </script>
</head>
<body>
    
</body>
</html>
```
## 3. 字符串
+ 字符串是以单引号'或双引号"括起来的任意文本，比如'abc'...

## 4.比较运算符
# 例：
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script>
        var a=10;
        var b=11;
        let c=11;
        console.log(a>b); // false
        console.log(a<b) // true
        console.log(b==c) //true
        console.log(false == 0) // true
        console.log(false === 0)// false
    </script>
</head>
<body>
    
</body>
</html>
```
+ 要特别注意相等运算符==。JavaScript在设计时，有两种比较运算符：

+ 第一种是==比较，它会自动转换数据类型再比较，很多时候，会得到非常诡异的结果；

+ 第二种是===比较，它不会自动转换数据类型，如果数据类型不一致，返回false，如果一致，再比较。

+ 由于JavaScript这个设计缺陷，不要使用==比较，始终坚持使用===比较。
