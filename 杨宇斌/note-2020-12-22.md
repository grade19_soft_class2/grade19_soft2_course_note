### 构造函数
除了直接用{ ... }创建一个对象外，JavaScript还可以用一种构造函数的方法来创建对象。它的用法是，先定义一个构造函数

+ 为了区分普通函数和构造函数，按照约定，构造函数首字母应当大写，而普通函数首字母应当小写
```
'use strict';

function Zoon(name){
    this.name= name || 'rabbit';
    this.hello=function(){
        console.log(this.name + ";你好");
    }
}
```
```
var dog=new Zoon('小狗');
console.log(dog.name);//小狗
dog.hello();//小狗;你好

var rabbit=new Zoon();
console.log(rabbit.name);//rabbit
rabbit.hello();//rabbit;你好

```
+ 如果不写new，这就是一个普通函数，它返回undefined。但是，如果写了new，它就变成了一个构造函数，它绑定的this指向新创建的对象，并默认返回this，也就是说，不需要在最后写return this

新创建的dog的原型链是：
```
dog ----> Zoon.prototype ---->Object.prototype ----> null
```

```
1.__proto__和constructor属性是对象所独有的；
2. prototype属性是函数所独有的，因为函数也是一种对象，所以函数也拥有__proto__和constructor属性。
```
```
'use strict';

function Zoon(name){
    this.name= name || 'rabbit';
    this.hello=function(){
        console.log(this.name + ";你好");
    }
}
var dog=new Zoon('小狗');
console.log(dog);



console.log(dog.__proto__);
console.log(dog.constructor);
console.log(Zoon);
console.log();

console.log(Zoon.prototype);
console.log(Zoon.constructor);
console.log(Zoon.__proto__);
console.log(Zoon.prototype.constructor);

console.log(dog.__proto__===Zoon.prototype);//true
console.log(dog.constructor===Zoon.prototype.constructor);//true

```
