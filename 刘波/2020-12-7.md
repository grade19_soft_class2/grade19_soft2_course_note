# 函数
* 函数内定义的变量互不影响
```
function foo(){
    var x = 1;
    return x+1;
}

function boo(){
    var x = 2;
    return x+1;
}

console.log(foo());//2
console.log(boo());//3
```
* 内部函数可以调用外部函数定义的变量，外部调用内部的会报错
```
function foo(){
    var x = 1;
    function boo(){
        var j = 2;
        return x+j;
    }
    return x+boo();
}
```
* 内外函数变量重名并不冲突各用各的
```
function foo(){
    var x = 1;
    function boo(){
        var x = 2;
        return x;
    }
    return x+boo();// 3
}
```
* 函数的变量要首先声明避免报错
* 不在函数内定义的变量有全局作用域，并且会自动绑定到window
```
var pt = '是你爹';

function print(){
    var cc = 11;
    console.log(window.pt);
}
alert(window.cc);//undefined
print();//是你爹
```
* 函数都具有全局作用域会自动绑定window
```
var foo = function(){
    console.log('傻逼');
}

function boo(){
    console.log('脑瘫');
}
window.foo();//傻逼
window.boo();//脑瘫
```
* alert也是全局变量
```
window.alert = '大帅逼';
window.alert('zzz');//重新定义后执行这段语句会报错
console.log(alert);// 大帅逼
console.log(window.alert);//大帅逼
```
* 为了防止全局变量的命名冲突会定义唯一一个全局变量
```
var only = {};
only.sa = 11;
only.bb = function(){
    console.log('ccccc');
};
only.bb();
console.log(only.sa);
```
* 用let声明局部作用域的变量
```
for(var i = 0;i<=10;i++){

}
console.log(i);//11

for(let j = 0;i<=10;i++){

}
console.log(j);//报错
```
* 用const定义常量，常量无法更改要重新定义