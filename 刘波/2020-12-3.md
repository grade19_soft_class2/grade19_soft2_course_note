# 循环
* for....in
```
var arr = [384,'吉吉国王',779,'猴子','猩猩','SH🗡龙'];
var student ={
    name:'好果子',
    age:'18',
    sex:'男',
    type:'坏孩子',
}
for(var key in arr){
    console.log(key);//数组下标
    console.log(arr[key]);//数组元素
}
console.log('-----------------------------------------');
for(var key in student){
    console.log(key);//输出属性
}
```
![001](./img/2020-12-03-01.png)  
* do...while
```
var arr = [384,'吉吉国王',779,'猴子','猩猩','SH🗡龙'];
var n = arr.length-1;
do{
    console.log(arr[n]);
    n = n-1;
}while(n>=0);//SH🗡龙 猩猩 猴子 779 吉吉国王 384
```
```
//当不满足条件的时候也会执行一次
var arr = [384,'吉吉国王',779,'猴子','猩猩','SH🗡龙'];
var n = arr.length-1;
do{
    console.log(arr[n]);
    n = n-1;
}while(n<0);//SH🗡龙
```
## Map和Set
* Map是一组键值对的结构，具有极快的查找速度
```
var arr = [384,'吉吉国王',779,'猴子','猩猩','SH🗡龙'];
//前面一个是key后面一个是value
var S = new Map([[20,'少黄'],['少黄',10],['少煌',30],['少HH',40]]);
console.log(S.get('少黄')) //10
console.log(S.get(20)); //少黄
//添加新的key-value
S.set('SH',50);
console.log(S.get('SH')); // 50
console.log(S.has('SH')); //是否存在key:SH true
S.delete("SH"); //删除key
console.log(S.has('SH'));//false
```
* Set不储存value并且key不重复
```
var s1 = new Set([1,2,3,3]);
console.log(s1);
s1.add(998);//添加key
console.log(s1);
s1.delete(1);//删除key
console.log(s1);
```
![002](./img/2020-12-03-02.png)  
### iterable
* Map,Set,Array都属于iterable都可以用for...of
```
//for...of与for...in功能差不多但是for...in会有些问题
var arr = [384,'吉吉国王',779,['猴子',1],'猩猩','SH🗡龙'];
arr.name = 'ccc';
for(var i in arr){
    console.log(i); //0,1,2,3,4,5,name
}
//返回原本数组元素
for(var i of arr){
    console.log(i); //384,'吉吉国王',779,['猴子',1],'猩猩','SH🗡龙'
}
```
* forEach
```
var a = ['A', 'B', 'C'];
a.forEach(function (element, index, array) {
    // element: 指向当前元素的值
    // index: 指向当前索引
    // array: 指向Array对象本身
    console.log(element + ', index = ' + index);
});
```
```
var m = new Map([[1, 'x'], [2, 'y'], [3, 'z']]);
m.forEach(function (value, key, map) {
    console.log(value);
});
```
```
//如果后面参数不需要使用可以忽略
var a = ['A', 'B', 'C'];
a.forEach(function (element) {
    console.log(element);
});
```