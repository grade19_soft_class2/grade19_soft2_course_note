# 解构赋值
* 对多个变量赋值
```
var arr = ['猴子','猩猩','野猴子','家养的猴子'];

var [a,b,c,d]=arr;

console.log(a);// 猴子
console.log(b);// 猩猩
console.log(c);// 野猴子
console.log(d);// 家养的猴子
```
* 数组内包含数组
```
var arr = ['猴子','猩猩','野猴子','家养的猴子'];

var str = ['草',['小草','野草']];

var [a,[b,c]] = str;

console.log(a); //草
console.log(b); //小草
console.log(c); //野草
```
* 结构赋值可以忽略元素
```
var arr = ['猴子','猩猩','野猴子','家养的猴子'];

var str = ['草',['小草','野草']];

var [a,[,c]] = str;

console.log(a); //草
console.log(c); //野草
```
* 对象也可以使用结构赋值但是属性名要一样
```
var grass = {
    name:'草',
    age:'?',
    eat:{
        one:'猪',
        two:'猴子',
        three:'大象',
    }
}

var {name,age,eat:{one,two,three}} = grass;

console.log(name); //草
console.log(one); //猪
```
 * 解构赋值可以用默认值
 ```
var grass = {
    name:'草',
    age:'?',
    eat:{
        one:'猪',
        two:'猴子',
        three:'大象',
    }
}

var {name,height=200} = grass;

console.log(height);//200
 ```