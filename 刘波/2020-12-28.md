# 操作表单
+ 文本框，对应的`<input type="text">`，用于输入文本；

+ 口令框，对应的`<input type="password">`，用于输入口令；

+ 单选框，对应的`<input type="radio">`，用于选择一项；

+ 复选框，对应的`<input type="checkbox">`，用于选择多项；

+ 下拉框，对应的`<select>`，用于选择一项；

+ 隐藏文本，对应的`<input type="hidden">`，用户不可见，但表单提交时会把隐藏文本发送到服务器。
1. 用value获取内容,单选框和复选框判断是否勾上用checked
```
<input type="text" id="001" value="123456">
<input type="checkbox" id="002" value="2" checked="true">
<input type="radio" id="003" value="1">
```
```
var che = document.getElementById("002");
var rad = document.getElementById("003");
var inp = document.getElementById("001");
console.log(che.checked);//true
console.log(rad.checked);//false
console.log(inp.value);//123456
```
2. 设置值
```
var che = document.getElementById("002");
var inp = document.getElementById("001");
var sel = document.getElementById("004");
inp.value = "123888888";
che.checked= false;
```
3. HTML5控件
```
<input type="color">
<input type="datetime">//日期
<input type="datetime-local">//日期时间
```
4. 提交表单
* 利用`<input type="hidden">`后将明文转为MD5
* 没有name属性的`<input>`不会被提交
```
<form id="login-form" method="post" onsubmit="return checkForm()">
    <input type="text" id="username" name="username">
    <input type="password" id="input-password">
    <input type="hidden" id="md5-password" name="password">
    <button type="submit">Submit</button>
</form>

<script>
function checkForm() {
    var input_pwd = document.getElementById('input-password');
    var md5_pwd = document.getElementById('md5-password');
    // 把用户输入的明文变为MD5:
    md5_pwd.value = toMD5(input_pwd.value);
    // 继续下一步:
    return true;
}
</script>
```
## 操作文件
* 控件`<input type="file">`用于上传文件
* File API可以获得文件信息并读取文件
* 回调函数就是一个函数作为参数传给另一个函数