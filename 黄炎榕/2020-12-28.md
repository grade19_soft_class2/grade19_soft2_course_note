# 操作表单：
## 一、获取值
```
// 单击 确定 按钮，将文本框里面的值打印到输出台
<body>
    <input type="text" name="txtUserName" id="text-username">
    <input type="button" name="submit" id="btn-submit" value="确定">
    <script>
        var user = document.getElementById('text-username');
        var btn = document.getElementById('btn-submit');
        btn.onclick = function () {
            console.log(user.value);
        }
    </script>
</body>
```
## 二、设置值
```
<body>
    <input type="text" name="txtUserName" id="text-username">
    <script>
        var user = document.getElementById('text-username');
        user.value = '设置值测试';
    </script>
</body>
```
## 三、H5控件
```
<input type="date" value="2000-06-19">

<input type="datetime-local" value="2000-05-18T12:00:00">

<input type="color" value="#ffd8d9">
```
![H5控件](./imgs/H5控件.png)
## 四、练习
```
</head>
<!-- 练习
利用JavaScript检查用户注册信息是否正确，在以下情况不满足时报错并阻止提交表单：

1. 用户名必须是3-10位英文字母或数字；

2. 口令必须是6-20位；

3. 两次输入口令必须一致。 -->

<body>
    <!-- HTML结构 -->
    <form id="test-register" action="#" target="_blank" onsubmit="return checkRegisterForm()">
        <p id="test-error" style="color:red"></p>
        <p>
            用户名: <input type="text" id="username" name="username">
        </p>
        <p>
            口令: <input type="password" id="password" name="password">
        </p>
        <p>
            重复口令: <input type="password" id="password-2">
        </p>
        <p>
            <button type="submit">提交</button> <button type="reset">重置</button>
        </p>
    </form>

    <script>
        'use strict';
        var checkRegisterForm = function() {
            // TODO:
            // 获取用户名
            var username = document.getElementById('username');
            // 设置用户名的正则表达式
            var a = /^[_0-9a-zA-Z]{3,10}$/;
            // 获取密码
            var password = document.getElementById('password');
            // 获取重复密码
            var password_2 = document.getElementById('password-2');
            if (a.test(username.value) == true && (password.value.lenth > 6 && password.value.lenth < 20) && (password.value == password_2.value)) {
                return true;
            } else {
                return false;
            }
        }

        // 测试:

        ;
        (function() {
            window.testFormHandler = checkRegisterForm;
            var form = document.getElementById('test-register');
            if (form.dispatchEvent) {
                var event = new Event('submit', {
                    bubbles: true,
                    cancelable: true
                });
                form.dispatchEvent(event);
            } else {
                form.fireEvent('onsubmit');
            }
        })();
    </script>
</body>
```