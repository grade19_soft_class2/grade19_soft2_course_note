## 分部类的应用

### 分部类关键字 Partial

### 分部类的使用
 + 先创建一个名为BigClass的类
      
    再创建一个名为BigClass的类会报错（名称重复）

    可以先创建一个名为BigClassOther的类，添加分部类关键字Partial后，将类名改为与另一个类相同的名称则不会报错

### 代码如下：
~~~
 先建的一个BigClass

namespace Practice
{
    public partial class BigClass
    {
        private int Id;
        
    }
}

~~~
~~~
 后建一个原名为BigClassOther，添加分部类关键字Partial后，将类名改为与另一个类相同的名称

namespace Practice
{
    public partial class BigClass 
    {
        private string Name;
    }
}
~~~

  ### 它们的字段可以互通使用

  ~~~
namespace Practice
{
    public partial class BigClass
    {
        private int Id;
        
        public void Add(int a ,int b)
        {
            this.Id = 1;
            this.Name = "小胖";
        }
    }
}
  ~~~


## 递归

### 递归定义
+ 递归，就是在运行的过程中调用自己。
~~~
public static int Fibonacc(int n)
        {

            if(n == 0)  //传入的参数为0则输出0
            {
                return 0;
            }
            else if (n <= 2)  //传入的参数小于2则输出1
            {
                return 1;
            }
            else
            {

                var values = Fibonacc(n - 1) + Fibonacc(n - 2);  //递归：运行的过程中调用自己

                    //注意与 (5 - 1) = 4 + (5 - 2) = 3 区别开

                return values;
            }
        }
~~~

### 构成递归需具备的条件：
1. 子问题须与原始问题为同样的事，且更为简单；
2. 不能无限制地调用本身，须有个出口，化简为非递归状况处理。


## Math类

  + 主要用于一些与数学相关的计算，并提供了很多静态方法方便访问

1. ### Abs	取绝对值 (如：-1 返回 1)
~~~
        static void Main(string[] args)
        {
            Console.WriteLine("请输入第一个数：");
            double num1 = Double.Parse(Console.ReadLine());
          
            var abs = Math.Abs(num1);
            Console.WriteLine(abs);
        }
~~~
2. ### Ceiling	返回大于或等于指定的双精度浮点数的最小整数值 (如：5.1 返回 6 ; 5.9 返回 6)
~~~
            Console.WriteLine("请输入第一个数：");
            double num1 = Double.Parse(Console.ReadLine());
          
            var ceiling = Math.Ceiling(num1);
            Console.WriteLine(ceiling);
~~~
3. ### Floor	返回小于或等于指定的双精度浮点数的最大整数值 (如：5.1 返回 5 ; 5.9 返回 5)
~~~
            Console.WriteLine("请输入第一个数：");
            double num1 = Double.Parse(Console.ReadLine());
          
            var floor = Math.Floor(num1);
            Console.WriteLine(floor);
~~~
4. ### Equals	返回指定的对象实例是否相等 (判断是否相等 如果相等则返回 True 不相等则返回 False )
  + 较多使用
~~~
            Console.WriteLine("请输入第一个数：");
            double num1 = Double.Parse(Console.ReadLine());
            Console.WriteLine("请输入第二个数：");
            double num2 = Double.Parse(Console.ReadLine());

            var equals = Math.Equals(num1,num2);
            Console.WriteLine(equals);
~~~
5. ### Max	返回两个数中较大数的值
  + 较多使用
~~~
            Console.WriteLine("请输入第一个数：");
            double num1 = Double.Parse(Console.ReadLine());
            Console.WriteLine("请输入第二个数：");
            double num2 = Double.Parse(Console.ReadLine());

            var max = Math.Max(num1,num2);
            Console.WriteLine(max);
~~~
6. ### Min	返回两个数中较小数的值
  + 较多使用
~~~
            Console.WriteLine("请输入第一个数：");
            double num1 = Double.Parse(Console.ReadLine());
            Console.WriteLine("请输入第二个数：");
            double num2 = Double.Parse(Console.ReadLine());

            var mix = Math.Mix(num1,num2);
            Console.WriteLine(mix);
~~~
7. ### Sqrt	返回指定数字的平方根 (如果数为负数则返回NaN)
~~~
            Console.WriteLine("请输入第一个数：");
            double num1 = Double.Parse(Console.ReadLine());
          
            var sqrt = Math.Sqrt(num1);
            Console.WriteLine(sqrt);
~~~
8. ### Round    返回四舍五入后的值 (小学学的四舍五入懂？)
~~~
            Console.WriteLine("请输入第一个数：");
            double num1 = Double.Parse(Console.ReadLine());
          
            var round = Math.Round(num1);
            Console.WriteLine(round);
~~~


## DateTime类
~~~
 DateTime dt = new DateTime();
  Console.WriteLine("当前日期为：{0}", dt);
         当前日期为：0001 / 1 / 1 0:00:00
~~~

+ ### 在当前计算机的时间上进行修改
~~~
       DateTime dt = DateTime.Now;//实例
                            //Now 当前计算机的时间   

            Console.WriteLine("当前日期为：{0}", dt);

            //Day	获取该实例所表示的日期是一个月的第几天
            Console.WriteLine("当前日期是本月的第{0}天",dt.Day);

            //DayOfWeek	获取该实例所表示的日期是一周的星期几
            Console.WriteLine("当前日期是星期{0}",dt.DayOfWeek);

            //DayOfYear 获取该实例所表示的日期是一年的第几天
            Console.WriteLine("当前日期是今年的第{0}天",dt.DayOfYear);

            //Add(Timespan value)	在指定的日期实例上添加时间间隔值 value

            //AddDays(double value)	在指定的日期实例上添加指定天数 value
            Console.WriteLine("三天半后的日期：{0}",dt.AddDays(3.5));

            //AddDays(double value)	在指定的日期实例上添加指定天数 value
            Console.WriteLine("三小时后的时间：{0}",dt.AddHours(3));

            //AddMinutes(double value)    在指定的日期实例上添加指定的分钟数 value
            Console.WriteLine("三分钟后的时间：{0}", dt.AddMinutes(3));

            //AddSeconds(double value)	在指定的日期实例上添加指定的秒数 value
            Console.WriteLine("三秒钟后的时间：{0}",dt.AddSeconds(3));

            //AddYears(int value)    在指定的日期实例上添加指定的年份 value
            Console.WriteLine("三年后的日期：{0}", dt.AddYears(3));

            //AddMonths(int value)	在指定的日期实例上添加指定的月份 value
            Console.WriteLine("三个月后的日期：{0}", dt.AddMonths(3));

~~~

## 课后练习

### 使用递归方式，打印斐波那契数列，要求定义一个函数，具有一个参数，指示打印的数列的个数

+ 斐波纳契数列（Fibonacci Sequence），又称黄金分割数列，指的是这样一个数列：1、1、2、3、5、8、13、21..... 
+ 规律：前两个数相加得出第三个数

+ ### 给一个参数输出一个斐波纳契数
~~~
public static int Fibonacc(int n)
        {

            if(n == 0)  //传入的参数为0则输出0
            {
                return 0;
            }
            else if (n <= 2)  //传入的参数小于2则输出1
            {
                return 1;
            }
            else
            {

                var values = Fibonacc(n - 1) + Fibonacc(n - 2);  //递归：运行的过程中调用自己

                    //注意与 (5 - 1) = 4 + (5 - 2) = 3 区别开

                return values;
            }
        }
~~~

+ ### 给一个参数输出斐波纳契数列
~~~
static void Main(string[] args)
        {
            List<int> ls = new List<int>(); //给一个泛型集合

            var UserInValue = 10;//给一个参数输出斐波纳契数列

            for (int x = 1; x <= UserInValue; x++) //循环输入范围参数
            {
                var i = Fibonacc(x); //输出一个斐波纳契数
                ls.Add(i); //将每一个斐波纳契数存入泛型集合
            }

            foreach(int item in ls)//遍历输出泛型集合
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }
 
public static int Fibonacc(int n)  //输出一个斐波纳契数的方法
        {

            if(n == 0)  //传入的参数为0则输出0
            {
                return 0;
            }
            else if (n <= 2)  //传入的参数小于2则输出1
            {
                return 1;
            }
            else
            {

                var values = Fibonacc(n - 1) + Fibonacc(n - 2);  //递归：运行的过程中调用自己

                    //注意与 (5 - 1) = 4 + (5 - 2) = 3 区别开

                return values;
            }
        }
~~~

###  Math下，ceilling floor这两个方法的区别
   1. Ceilling：

      返回大于或等于指定的双精度浮点数的最小整数值 (如：5.1 返回 6 ; 5.9 返回 6)

   2. Floor：

      返回小于或等于指定的双精度浮点数的最大整数值 (如：5.1 返回 5 ; 5.9 返回 5)
       

### 关于日期，给出任意的日期值，计算当月第一天、当月最后一天是多少号，并且计算给出日期值是当月第几周

~~~ 
            //New一个日期
            DateTime dt = new DateTime(2020,1,21);

            //当前日期
            Console.WriteLine("当前日期：{0}",dt.ToString("D"));

            //这个月的第一天   =   当前天数 + (1 - 当前天数)
            Console.WriteLine("本月的第一天为：{0}",dt.AddDays(1 - dt.Day).ToString("M"));

            //这个月最后一天   =  下个月的第一天 再 减去一天
            Console.WriteLine("本月的最后一天为：{0}", dt.AddMonths(1).AddDays(1 - dt.Day).AddDays(-1).ToString("M"));

             //今天是这个月的第几周
~~~