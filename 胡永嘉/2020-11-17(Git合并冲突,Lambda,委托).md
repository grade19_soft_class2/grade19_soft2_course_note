# 大二第一学期课堂笔记

## Description

```
 预览快捷键 Ctrl+Shift+V
```
### 1.如何向笔记中插入图片
  + 笔记中插入本地图片 
  ![笔记中插入本地图片](./imgs/2020-11-17-01.png)
  
### + Git 提交到远程仓库顺序
1. 将文件提交到暂存区 add
```
git add .
```
2. 查看状态 status
```
git status
```
3. 提交到工作区
```
git commit -m'备注'
```
4. 提交到远程仓库
```
git push
```

### + Git 远程仓库更新到本地仓库
```
git pull
```

### + Git 远程仓库克隆到本地仓库
```
git clone 远程仓库地址
```

### 2.如何解决Git合并冲突问题
    
(1)Git避免合并冲突

 + Git远程仓库克隆下来的所有源文件不能去更改。
 + 可以重新建一个文件夹，将自己的所有要添加或修改的文件储存进去；

 (2)解决合并冲突

 + 用 远程仓库源文件 替代 克隆下来的冲突文件

   将克隆文件里冲突的文件删除 --》 重新克隆远程源原仓库，将没有冲突的源文件复制进克隆文件；
### 3.Lambda表达式  其实就是匿名函数
### 4.委托
(1)自定义委托
 ```
deleget void Say();

function void SayHello()
{

}

Say s = SayHello
 ```

 (2)内置委托 func action

   + func 代表有一个返回值的方法 func<bool> abc = () => true;

   + action 代表没有返回值的方法 action<string> bbc = (str) => {str.length>0}

