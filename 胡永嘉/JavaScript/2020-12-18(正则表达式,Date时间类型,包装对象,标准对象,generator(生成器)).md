# generator（生成器）
+ 一个generator看上去像一个函数，但可以返回多次
+ generator和函数不同的是，generator由function`*`定义（注意多出的`*`号），并且，除了return语句，还可以用yield返回多次。

1. 如果要编写一个产生斐波那契数列的函数，可以这么写：
```
'use strict'

function* fun(max){
    var t , a = 0 , b = 1 , n = 0;
    while(n<max){
        yield a;
        [a,b] = [b,a+b]
        n++
    }
    return;
}
```
直接调用一个generator和调用函数不一样，fun(10)仅仅是创建了一个generator对象，还没有去执行它。
1. 可以用for用for ... of循环迭代generator对象，这种方式不需要我们自己判断done
```
for(var x of fun(10)){
    console.log(x);
}
```
2. 不断地调用generator对象的next()方法：
```
var f = fun(5);
f.next(); // {value: 0, done: false}
f.next(); // {value: 1, done: false}
f.next(); // {value: 1, done: false}
f.next(); // {value: 2, done: false}
f.next(); // {value: 3, done: false}
f.next(); // {value: undefined, done: true}
```

# 标准对象
用typeof操作符获取对象的类型，它总是返回一个字符串
1. number类型
```
console.log(typeof 123); //'number'
console.log(typeof NaN); //'number'
```
2. string类型
```
console.log(typeof "xiaopang"); //'string'
```
3. boolean类型
```
console.log(typeof true); // 'boolean'
```
4. undefined类型
```
console.log(typeof undefined); // 'undefined'
console.log(typeof xiaopang); //'undefined'
```
5. function类型
```
console.log(typeof Math.max); //'function'
```
6. object类型
```
console.log(typeof []); //'object'
console.log(typeof {}); //'object'
console.log(typeof null); //'object'
```
## 包装对象
number、boolean和string都有包装对象。没错，在JavaScript中，字符串也区分string类型和它的包装类型。包装对象用new创建：
```
var n = new Number(123); // 123,生成了新的包装类型
var b = new Boolean(true); // true,生成了新的包装类型
var s = new String('str'); // 'str',生成了新的包装类型
```
虽然包装对象看上去和原来的值一模一样，显示出来也是一模一样，但他们的类型已经变为object了！所以，包装对象和原始值用===比较会返回false
```
console.log(new Number(123) === 123);
console.log(new String("xiaopang") === 'xiaopang');
console.log(new Boolean("true") === true);
```
+ 上面3个输出都是false；

也就是说，包装一下，就和没包装前的感觉一样，但是类型已经完全不一样了,所以闲的蛋疼也不要使用包装对象！尤其是针对string类型！！！

此时，Number()、Boolean()和String()被当做普通函数，把任何类型的数据转换为number、boolean和string类型（注意不是其包装类型）：
```
var n = Number('123'); // 123，相当于parseInt()或parseFloat()
typeof n; // 'number'

var b = Boolean('true'); // true
typeof b; // 'boolean'

var b2 = Boolean('false'); // true! 'false'字符串转换结果为true！因为它是非空字符串！
var b3 = Boolean(''); // false

var s = String(123.45); // '123.45'
typeof s; // 'string'
```

### 总结一下，有这么几条规则需要遵守：

+ 不要使用new Number()、new Boolean()、new String()创建包装对象；

+ 用parseInt()或parseFloat()来转换任意类型到number；

+ 用String()来转换任意类型到string，或者直接调用某个对象的toString()方法；

+ 通常不必把任意类型转换为boolean再判断，因为可以直接写if (myVar) {...}；

+ typeof操作符可以判断出number、boolean、string、function和undefined；

+ 判断Array要使用Array.isArray(arr)；

+ 判断null请使用myVar === null；

+ 判断某个全局变量是否存在用typeof window.myVar === 'undefined'；

+ 函数内部判断某个变量是否存在用typeof myVar === 'undefined'。

# Date Date对象用来表示日期和时间。
要获取系统当前时间，用：
```
var now = new Date();
now; // Wed Jun 24 2015 19:49:22 GMT+0800 (CST)
now.getFullYear(); // 2015, 年份
now.getMonth(); // 5, 月份，注意月份范围是0~11，5表示六月
now.getDate(); // 24, 表示24号
now.getDay(); // 3, 表示星期三
now.getHours(); // 19, 24小时制
now.getMinutes(); // 49, 分钟
now.getSeconds(); // 22, 秒
now.getMilliseconds(); // 875, 毫秒数
now.getTime(); // 1435146562875, 以number形式表示的时间戳
```
要注意的是：当前时间是浏览器从本机操作系统获取的时间，所以不一定准确，因为用户可以把当前时间设定为任何值

1. 我们可以创建一个指定的日期和时间
```
var date = new Date(2020,11,18,16,40,23)

console.log(date);
```
上面输出的月份是12月

要注意的是：一个非常非常坑爹的地方，就是JavaScript的月份范围用整数表示是0~11，0表示一月，1表示二月……

2. 创建一个指定日期和时间的方法是解析一个符合ISO 8601格式的字符串：
```
var date = Date.parse('2020,11,18,2:30:30')

console.log(date); //1605628830000
```
但它返回的不是Date对象，而是一个时间戳。不过有时间戳就可以很容易地把它转换为一个Date：
```
var date2 = new Date(1605637830000)

console.log(date2); //2020,11,18,2:30:30
```
使用Date.parse()时传入的字符串使用实际月份01~12，转换为Date对象后getMonth()获取的月份值为0~11。

## 时区
Date对象表示的时间总是按浏览器所在时区显示的，不过我们既可以显示本地时间，也可以显示调整后的UTC时间：
```
var d = new Date(1435146562875);
d.toLocaleString(); // '2015/6/24 下午7:49:22'，本地时间（北京时区+8:00），显示的字符串与操作系统设定的格式有关
d.toUTCString(); // 'Wed, 24 Jun 2015 11:49:22 GMT'，UTC时间，与本地时间相差8小时
```
那么在JavaScript中如何进行时区转换呢？实际上，只要我们传递的是一个number类型的时间戳，我们就不用关心时区转换。任何浏览器都可以把一个时间戳正确转换为本地时间。

时间戳是个什么东西？时间戳是一个自增的整数，它表示从1970年1月1日零时整的GMT时区开始的那一刻，到现在的毫秒数。假设浏览器所在电脑的时间是准确的，那么世界上无论哪个时区的电脑，它们此刻产生的时间戳数字都是一样的，所以，时间戳可以精确地表示一个时刻，并且与时区无关。

所以，我们只需要传递时间戳，或者把时间戳从数据库里读出来，再让JavaScript自动转换为当地时间就可以了。

### 要获取当前时间戳，可以用：
```
'use strict';
if (Date.now) {
    console.log(Date.now()); // 老版本IE没有now()方法
} else {
    console.log(new Date().getTime());
}
```

# 正则表达式
正则表达式是一种用来匹配字符串的强有力的武器。它的设计思想是用一种描述性的语言来给字符串定义一个规则，凡是符合规则的字符串，我们就认为它“匹配”了，否则，该字符串就是不合法的。
所以我们判断一个字符串是否是合法的Email的方法是：

创建一个匹配Email的正则表达式；

用该正则表达式去匹配用户的输入来判断是否合法。

因为正则表达式也是用字符串表示的，所以，我们要首先了解如何用字符来描述字符。

在正则表达式中，如果直接给出字符，就是精确匹配。用\d可以匹配一个数字，\w可以匹配一个字母或数字，所以：
```
'00\d'可以匹配'007'，但无法匹配'00A'；

'\d\d\d'可以匹配'010'；

'\w\w'可以匹配'js'；
```

.可以匹配任意字符，所以：
```
'js.'可以匹配'jsp'、'jss'、'js!'等等。
```

要匹配变长的字符，在正则表达式中，用*表示任意个字符（包括0个），用+表示至少一个字符，用?表示0个或1个字符，用{n}表示n个字符，用{n,m}表示n-m个字符：
```
\d{3}\s+\d{3,8}
```
1. \d{3}表示匹配3个数字，例如'010'；

2. \s可以匹配一个空格（也包括Tab等空白符），所以\s+表示至少有一个空格，例如匹配' '，'\t\t'等；

3. \d{3,8}表示3-8个数字，例如'1234567'。

综合起来，上面的正则表达式可以匹配以任意个空格隔开的带区号的电话号码。

如果要匹配'010-12345'这样的号码呢？由于'-'是特殊字符，在正则表达式中，要用`\`转义，所以，上面的正则是`\d{3}\-\d{3,8}`

但是，仍然无法匹配'010 - 12345'，因为带有空格。所以我们需要更复杂的匹配方式。

## 进阶
要做更精确地匹配，可以用[]表示范围，比如：
```
[0-9a-zA-Z\_]可以匹配一个数字、字母或者下划线；

[0-9a-zA-Z\_]+可以匹配至少由一个数字、字母或者下划线组成的字符串，比如'a100'，'0_Z'，'js2015'等等；

[a-zA-Z\_\$][0-9a-zA-Z\_\$]*可以匹配由字母或下划线、$开头，后接任意个由一个数字、字母或者下划线、$组成的字符串，也就是JavaScript允许的变量名；

[a-zA-Z\_\$][0-9a-zA-Z\_\$]{0, 19}更精确地限制了变量的长度是1-20个字符（前面1个字符+后面最多19个字符）。
```
A|B可以匹配A或B，所以(J|j)ava(S|s)cript可以匹配'JavaScript'、'Javascript'、'javaScript'或者'javascript'。

^表示行的开头，^\d表示必须以数字开头。

$表示行的结束，\d$表示必须以数字结束。

你可能注意到了，js也可以匹配'jsp'，但是加上^js$就变成了整行匹配，就只能匹配'js'了。

## RegExp
有了准备知识，我们就可以在JavaScript中使用正则表达式了。

JavaScript有两种方式创建一个正则表达式：

第一种方式是直接通过/正则表达式/写出来，第二种方式是通过new RegExp('正则表达式')创建一个RegExp对象。

两种写法是一样的：
```
var re1 = /ABC\-001/;
var re2 = new RegExp('ABC\\-001');

re1; // /ABC\-001/
re2; // /ABC\-001/
```
注意，如果使用第二种写法，因为字符串的转义问题，字符串的两个\\实际上是一个\。

先看看如何判断正则表达式是否匹配：
```
var re = /^\d{3}\-\d{3,8}$/;
re.test('010-12345'); // true
re.test('010-1234x'); // false
re.test('010 12345'); // false
```
RegExp对象的test()方法用于测试给定的字符串是否符合条件。
