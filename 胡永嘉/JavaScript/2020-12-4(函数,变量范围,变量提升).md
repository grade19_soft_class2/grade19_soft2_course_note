# 函数function 定义和调用
## 函数定义
+ function指示这是个函数定义
+ abs函数的名称
+ x函数的输入值
    多个函数用,分隔开
+ {...}函数体
    一但执行到return时，函数就执行完毕并将结果返回

1. 函数定义一
```
function abs(x){
 
}
```

2. 函数定义二

给函数赋值abs
```
var abs = function(){
    
};
```
上述两种定义完全等价，注意第二种方式按照完整语法需要在函数体末尾加一个;，表示赋值语句结束

## 调用函数
abs为函数名称(values)
```
abs()
abs(30)
```
### 调用函数时传入参数少于指定参数也没关系
```
function abs(x,y,j){
    console.log(x);
    console.log(y);
    console.log(j);
}

abs(1,2); //将输出 1，2，undefined
```
### 调用函数时传入参数多于指定参数也没关系
```
function abs(x,y,j){
    console.log(x);
    console.log(y);
    console.log(j);
}

abs(1,2,3,4,5); //将输出 1，2，3 多余的将不再输出
```

## arguments 参数
JavaScript还有一个免费赠送的关键字arguments，它只在函数内部起作用，并且永远指向当前函数的调用者传入的所有参数。arguments类似Array但它不是一个Array
```
function foo(x){
    for(var i=0;i<arguments.length;i++){
        console.log(arguments[i]);
    }
}
foo(10,20,30) // 10 , 20 , 30
    可以将传入的参数都打印出来
```

 利用arguments，你可以获得调用者传入的所有参数。也就是说，即使函数不定义任何参数，还是可以拿到参数的值

### 实际上arguments最常用于判断传入参数的个数
1. 接收2~3个参数，b是可选参数，如果只传2个参数，b默认为null：
```
function foo(x,y,j){
    if(arguments.length  === 2){
        j=y;
        y=null;
    }
    console.log(x);
    console.log(y);
    console.log(j);
}
foo(10,20); // 10 , null ,20
```

## rest 参数
rest参数只能写在最后，前面用...标识，传入的参数先绑定a、b，c多余的参数以数组形式交给变量rest，所以，不再需要arguments我们就获取了全部参数
+ 可以将多传入的参数以数组的形式输出
```
function foo(x,y,j,...rest){
    console.log(x);
    console.log(y);
    console.log(j);
    console.log(rest);
}

foo(10,20,3,4,5); //10，20，3，[4,5]
```

### 小心你的return语句
前面我们讲到了JavaScript引擎有一个在行末自动添加分号的机制，这可能让你栽到return语句的一个大坑：
```
function foo() {
    return { name: 'foo' };
}

foo(); // { name: 'foo' }
如果把return语句拆成两行：

function foo() {
    return
        { name: 'foo' };
}

foo(); // undefined
```
要小心了，由于JavaScript引擎在行末自动添加分号的机制，上面的代码实际上变成了：
```
function foo() {
    return; // 自动添加了分号，相当于return undefined;
        { name: 'foo' }; // 这行语句已经没法执行到了
}
```
所以正确的多行写法是：
```
function foo() {
    return { // 这里不会自动加分号，因为{表示语句尚未结束
        name: 'foo'
    };
}
```

# 变量的作用域
1. 变量如果在一个函数体里面声明，则该变量的作用域就在这这个函数体，函数体外无法使用；
2. 变量1在一个函数体1里面声明，函数体1内还有一个函数2，该函数2的函数体可以调用变量1，但是该函数2的函数体内的变量不能给函数1使用；
```
function foo(){
    var x = 10;
    function too{
        var y = x + 10; //可以使用变量 a
        console.log(y);
    }
    too();
    var a = y + 1 //则无法使用变量 y
}
```

### 如果内部函数和外部函数的变量名重名怎么办
```
function foo(){
    var x = 10;
    function too(){
        var x = 60;
        var y = x + 10;
        console.log(y);
    }
    too();
}
foo();
```
调用函数too()函数体内的x;


## 变量提升
先输出y再声明变量y
```
function foo(){
    console.log(y); //undefined
    var y = 20;
}
```
会将变量y提到前面，但不会将y的赋值一起提到前面
JavaScript引擎看到的代码相当于：
```
function foo() {
    var y; // 提升变量y的声明，此时y为undefined
    console.log(x);
    y = 20;
}
```
