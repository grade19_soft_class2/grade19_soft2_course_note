# 循环

### for循环
1. 通过初始条件、结束条件和递增条件来循环

打印出0-10（并不包括10）
···
for(i=0;i<10;i++){
    console.log(i);
}
···

### for循环最常用的地方是利用索引来遍历数组
```
var arr = ['小胖','小花',66]

for(var i=0;i<=arr.length-1;i++){
    console.log(arr[i]);
}
```

for循环的3个条件都是可以省略的，如果没有退出循环的判断条件，就必须使用break语句退出循环，否则就是死循环：
```
var x = 0;
for (;;) { // 将无限循环下去
    if (x > 100) {
        break; // 通过if判断来退出循环
    }
    x ++;
}
```
这将导致大量的内存被占用

### for in 可以把一个对象的所以属性依次循环出来
1. for in 对数组效果:将所有的元素下标打印出来
```
var arr = ['小胖','小花',66]

for(var key in arr){
    console.log(key); // 0,1,2
}
```

要过滤掉对象继承的属性，用hasOwnProperty()来实现

2. for in 对对象效果：将元素名打印出来
```
var diuxiang = {
    name : 'xiaopang',
    age : 20,
    sex : '男'
}

for(var key in diuxiang){
    console.log(key); //name,age,sex 
}
```

### 要得到属性值则
 将对象或数组里的属性值打印出来
```
var diuxiang = {
    name : 'xiaopang',
    age : 20,
    sex : '男'
}

for(var key in diuxiang){
    console.log(diuxiang[key]);
}
```

# Map 与 Set
## Map Map是一组键值对的结构，具有极快的查找速度
```
var m = new Map([['Michael', 95], ['Bob', 75], ['Tracy', 85]]);
m.get('Michael'); // 95
```
### 或者直接初始化一个空Map
set(key) // 添加
has(key) // 是否存在
delete(key) // 删除
get(key) // 查询
```
var m = new Map(); // 空Map
m.set('Adam', 67); // 添加新的key-value
m.set('Bob', 59);
m.has('Adam'); // 是否存在key 'Adam': true
m.get('Adam'); // 67
m.delete('Adam'); // 删除key 'Adam'
m.get('Adam'); // undefined
```

## Set Set和Map类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在Set中，没有重复的key
1. 要创建一个Set，需要提供一个Array作为输入，或者直接创建一个空Set：
```
var s1 = new Set(); // 空Set
var s2 = new Set([1, 2, 3]); // 含1, 2, 3
```
+ 通过 add(key) 添加 ，如果添加重复的值将自动给省略
```
var s2 = new Set([1, 2, 3]); // 含1, 2, 3
s2.add(0,3,4,5,6)
s2 //1,2,3,0,4,5,6
```

+ 通过delete(key)方法可以删除元素：
```
var s2 = new Set([1, 2, 3]);
s2; // Set {1, 2, 3}
s2.delete(3);
s2; // Set {1, 2}
```
# iterable

遍历Array可以采用下标循环，遍历Map和Set就无法使用下标。为了统一集合类型，ES6标准引入了新的iterable类型，Array、Map和Set都属于iterable类型。

具有iterable类型的集合可以通过新的for ... of循环来遍历

1. 遍历Array
```
var a = [1,2,3];
for(var x of a){
    console.log(x);
}
```
2. 遍历Set
```
var b = new Set([1,2,3,3]);
for(var x of b){
    console.log(x);
}
```
3. 遍历Map
```
var c = new Map([['xiaopang',99],['xiaohua',98],['abc',100]]);
for(var x of c){
    console.log(x[0] + '=' + x[1]);
}
```

## for ... of循环和for ... in循环的区别

当我们手动给Array对象添加了额外的属性后，for ... in循环将带来意想不到的意外效果：
```
var a = ['A', 'B', 'C'];
a.name = 'Hello';
for (var x in a) {
    console.log(x); // '0', '1', '2', 'name'
}
```
for ... in循环将把name包括在内，但Array的length属性却不包括在内。

for ... of循环则完全修复了这些问题，它只循环集合本身的元素：
```
var a = ['A', 'B', 'C'];
a.name = 'Hello';
for (var x of a) {
    console.log(x); // 'A', 'B', 'C'
}
```

## forEach()
然而，更好的方式是直接使用iterable内置的forEach方法，它接收一个函数，每次迭代就自动回调该函数。
### 以Array为例
```
'use strict';
var a = ['A', 'B', 'C'];
a.forEach(function (element, index, array) {
    // element: 指向当前元素的值
    // index: 指向当前索引
    // array: 指向Array对象本身
    console.log(element + ', index = ' + index);
});
```

如果对某些参数不感兴趣，由于JavaScript的函数调用不要求参数必须一致，因此可以忽略它们。
### 只需要获得Array的element：
```
var a = ['A', 'B', 'C'];
a.forEach(function (element) {
    console.log(element);
});
```