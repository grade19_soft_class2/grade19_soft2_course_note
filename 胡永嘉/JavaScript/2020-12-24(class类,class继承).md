# class继承

## class 类
新的关键字class从ES6开始正式被引入到JavaScript中。class的目的就是让定义类更简单。
```
'use strict'

class Student{
    constructor(name){
        this.name = name || '匿名';
    }

    hello(){
        return `hello,${this.name}!`
    }
}

var xiaopang =  new Student('xiaopang');
console.log(xiaopang.hello());

var xiaohua = new Student('小花');
console.log(xiaohua.hello());
```
### class的定义包含了构造函数constructor和定义在原型对象上的函数hell0()（注意没有function关键字）

### constructor
+ 每个类都会有一个constructor方法，该方法属于构造方法。
+ 在new这个对象的时候会执行该方法
+ 该构造方法默认返回实例对象（this）

## class继承
### 关键字 extends
```
'use strict'
//创建Student类
class Student{
    constructor(name){          
        this.name = name || '匿名'; //类属性不需要var声明 ， var prrent = '';是声明普通的变量
    } 
    //成员方法
    hello(name){    x 
        return `hello,${this.name}!` //访问类属性需要用 this    
    }
}
//PrimaryStrdent类 继承 Student类
//PrimaryStrdent 为子类 ，继承自 Student父类
class PrimaryStrdent extends Student{
    constructor(name,age){
        super(name);    //调用 super() 代替父类构造函数,初始化与父类共同的属性name
        this.age = age || 1; //初始化子类新的属性age
    }


}

//父类调用
var xiaopang =  new Student('xiaopang');
console.log(xiaopang.hello());

//继承后，子类调用
var xiaohua = new PrimaryStrdent('xiaohua');    
console.log(xiaohua.hello('xiaohua'));
console.log(xiaohua.age = 20);

var xiaopang2 = new PrimaryStrdent('小胖');
console.log(xiaopang2.hello('小胖'));
console.log(xiaopang2.name);
console.log(xiaopang2.age);
```

