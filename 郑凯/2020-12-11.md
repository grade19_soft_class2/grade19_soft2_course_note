# 2020-12-11课堂笔记
## 装饰器
+ 装饰器是对类、函数、属性之类的一种装饰，可以针对其添加一些额外的行为。
+ 在原有代码外层包装了一层处理逻辑。
```
'use strict'
var count = 0;
var oldParseInt = parseInt; 

window.parseInt = function () {
    count += 1;
    return oldParseInt.apply(null, arguments); 
};

console.log(parseInt('10'));
console.log(parseInt('20'));
console.log(parseInt('30'));
console.log('count = ' + count);//3
```
## 高阶函数
+ JavaScript的函数其实都指向某个变量。既然变量可以指向函数，函数的参数能接收变量，那么一个函数就可以接收另一个函数作为参数，这种函数就称之为高阶函数。
+ 编写高阶函数，就是让函数的参数能够接收别的函数。

```
'use strict'
function add(x,y,z){
    return Math.abs(x)+Math.abs(y)+Math.abs(z);
}
var q =add(99,96,98,Math.abs);
console.log(q);//293
```
## Map
```
'use strict'
var arr=[111,222,333,444,555,666,777,888,999];

function pow(x){
    return x*x*x;
}
console.log(arr.map(pow));
```

## reduce
+ reduce() 方法接收一个函数作为累加器,reduce 为数组中的每一个元素依次执行回调函数，不包括数组中被删除或从未被赋值的元素，接受四个参数：初始值（上一次回调的返回值），当前元素值，当前索引，原数组 
```
'use strict'
function product(arr){
    return arr.reduce((x,y)=>{
        return x*y;
    });

}
if(product([1,2,3,4]) === 24 && product([0,1,2]) === 0 && product([44,55,66,77]) === 44274384){
    console.log('测试通过');
}else{
    console.log('测试失败');
}
```