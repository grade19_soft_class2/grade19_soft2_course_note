# 递归的用法
>> 说白了就是函数内部调用本身这个函数（自己调用自己），然后必须要有循环结束的条件
## 以计算斐波那契数为例

```
        //计算斐波那契数
        static int GetNum(int n)
        {
            if (n <= 2)
                return 1;
            return GetNum(n - 2) + GetNum(n - 1);//自己调用自己

        }
        //打印指定个数的斐波那契数列
        static void OutPut(int n)
        {
            string str = string.Empty;
            for (int i = 1; i <= n; i++)
            {
                str += GetNum(i) + " ";
            }
            Console.WriteLine(str);
        }

        static void Main(string[] args)
        {

            OutPut(10);
        }
```




# Math类下的Ceiling方法和Floor方法的用法和区别
+ Ceiling和Floor都是对浮点型的一种格式化操作
+ Ceiling：去掉小数，取整并且整数位加1
+ Floor:去掉小数，取整

# DateTime
### DateTime常用用，废话不多说，上代码
```
        static void Main(string[] args)
        {
            
            //当月第一天
            Console.WriteLine(GetDayInFirstMonth("2020-10-8"));
            //当月最后一天是多少号
            Console.WriteLine(GetDayInLastMonth("2020-10-8"));
            //当月第几周
            Console.WriteLine(GetWeekInMonth("2020-10-8"));

        }
```
```
        static DateTime GetDayInFirstMonth(string str)
        {
            var time = Convert.ToDateTime(str);
            return time.AddDays(-time.Day + 1);
        }

        static int GetDayInLastMonth(string str)
        {
            var time = Convert.ToDateTime(str);
            return time.AddMonths(1).AddDays(-time.Day).Day;
        }


        static int GetWeekInMonth(string str)
        {
            var time = Convert.ToDateTime(str);
            var val = time.AddDays(-time.Day + 1).DayOfWeek == 0 ? 1 :7- (int)time.AddDays(-time.Day + 1).DayOfWeek+1;
            if (time.Day <= val)
                return 1;
            else if (time.Day < val + 7)
                return 2;
            else if (time.Day < val + 14)
                return 3;
            else if (time.Day < val + 21)
                return 4;
            else if (time.Day < val + 28)
                return 5;
            else
                return 6;
        }
```
