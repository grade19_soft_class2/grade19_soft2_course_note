# git分支管理
## 创建与合并分支的常用命令
>查看分支：git branch
>
>创建分支：git branch 分支名
>
>切换分支：git checkout 分支名   或者   git switch 分支名
>
>创建+切换分支：git checkout -b 分支名  或者git switch -c 分支名
>
>合并某分支到当前分支：git merge 分支名
>
>删除分支：git branch -d 分支名

### 小结
>1.master或者其他分支的指向为当前提交，而HEAD指针指向的是分支，当前正在工作中的分支
>
>2.使用merge合并分支，不存在冲突的情况下，直接把当前分支指向要合并的分支上

## 合并冲突的原因和解决
### 原因
>拿master和dev分支来说，当我们分别在master和dev分支下同时修改同一个文件同一位置并提交，master和dev各自多出了一个提交记录，所以现在我们想把dev合并到master上，正常情况是可以自动合并的，各自的提交合并在一起形成时间线，但是，因为它俩同时修改同一位置的相同文件，所以产生冲突，不能形成一条时间线，使用status可以查看当前是否存在冲突
```
$ git status
On branch master
Your branch is ahead of 'origin/master' by 2 commits.
  (use "git push" to publish your local commits)

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)

	both modified:   readme.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

### 解决
>使用vim编辑产生冲突的文件，git会使用<<<<<<<，=======，>>>>>>>标记不同分支的内容，根据自己的需要进行取舍
>
>解决冲突的根本是，我们要取哪个分支下内容或者都不取，不管怎么选择，但记得commit提交，这样git的提交记录就可以形成一条时间线了，

### 小结
>1.当我们使用vi或者vim编辑文件不正常退出时，可以会产生一个.swp的文件，导致不能删除和提交，这个时候打开任务管理器，把vim.exe拉出去枪毙五分钟。
>
>2.用git log --graph命令可以看到分支合并图。
>
>3.用git log查看提交版本记录，如果存在“未来版本记录”，用git reflog可以查看“未来版本记录”