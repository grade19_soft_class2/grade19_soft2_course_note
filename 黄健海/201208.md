# this 永远指向函数运行时所在的对象，而不是函数被创建时所在的对象。
+ 1.单独的this，指向的是window这个对象
```
	
alert(this); // this -> window
```
+ 2.全局函数中的this，指向的是window这个对象
```
function demo() {

 alert(this); // this -> window

}

demo();
```
+ 在严格模式下，this是undefined.
```
function demo() {

 'use strict';

 alert(this); // undefined

}

demo();
```
+ 3.函数调用的时候，前面加上new关键字
所谓构造函数，就是通过这个函数生成一个新对象，这时，this就指向这个对象。
```
function demo() {

 //alert(this); // this -> object

 this.testStr = 'this is a test';

}

let a = new demo();

alert(a.testStr); // 'this is a test'
```