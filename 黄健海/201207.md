# 块级作用域：
### 特点： 不存在变量声明提升；作用域仅在所在的{}之内（称为暂时性死区）；不允许重复声明；
+ 1.let:
```
let i= 10;
for(let i = 0;i<10;i++){
	let i= 20;
}
//  本段代码并不会报错（不允许重复声明错误），因为是三个层级的i,注意for循环的()与{}属于两个层级
//  即：
let i = 10
{
	let i =0;
	{
		let i = 20;
	}
}
```
+ 2.const
```
const A= 0；
A = 1；//会报错
const obj = {};
obj.a = 10;
obj = {b:10}; //此时会报错，因为地址被改变；
```
# 解构赋值（实质是模式匹配）
### 解构失败：let [a,b] = [1]; // 此时b为undefined
不完全解构：let [a] = [1,2];
实质上，当右边的值绝对（===）等于undefined的时，才会用左边的默认值；并且是一种惰性赋值（不用不加载）；
例：
```
let [x = 1,y = x] = [];       //   x=1  y=1
let [x = 1,y = x] = [2];   	  //   x=2  y=2
let [x = 1,y = x] = [2,4];    //   x=2  y=4
let [x = y,y = 1] = [];       //   报错，使用了let,先使用后声明
let [x = y,y = 1] = [2];      //   x=2  y=1
```