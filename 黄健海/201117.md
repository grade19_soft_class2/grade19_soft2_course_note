创建与合并分支的常用命令
查看分支：git branch

创建分支：git branch 分支名

切换分支：git checkout 分支名 或者 git switch 分支名

创建+切换分支：git checkout -b 分支名 或者git switch -c 分支名

合并某分支到当前分支：git merge 分支名

删除分支：git branch -d 分支名
合并冲突的原因和解决
原因
拿master和dev分支来说，当我们分别在master和dev分支下同时修改同一个文件同一位置并提交，master和dev各自多出了一个提交记录，所以现在我们想把dev合并到master上，正常情况是可以自动合并的，各自的提交合并在一起形成时间线，但是，因为它俩同时修改同一位置的相同文件，所以产生冲突，不能形成一条时间线，使用status可以查看当前是否存在冲突