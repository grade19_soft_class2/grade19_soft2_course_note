# 2020.12.19学习笔记(星期六)

## JSON
+  JSON全名是 (JavaScript Object Notation) js对象表现法
+  JSON是一种特殊的字符串,这个字符串可以被任意语言所识别,并且可以转换成任意语言中的对象,JSON主要用于数据交换
+  JSON和JS对象一样,只不过JSON的属性名必须要加上""号，只能是""号，并且整体需要用''号包裹起来才叫JSON对象
+  注意一点：创建JSON对象的时候给予属性赋值字符串是最好也使用""不使用''来定义值,因为前面的属性名需使用""来包裹所以你在使用''时ta可能把你当作字符了会报错
    ```
    比如说这样：
    var obj1 ='{"name":'小明',"age":18,"gender":'男'}'
    从小明',之后就显示报错了,所以在创建JSON对象时使用""来赋值
    ```
+ JSON分两种
 1. 对象 {}
 2. 数组 []
    ```
    // 这是一个js对象
    var obj = {name:'小明',age:'18',gender:'男'};
    console.log(typeof obj); // object
    console.log(obj); // {name:'小明',age:'18',gender:'男'}

    // 这是一个JSON对象
    var str ='{"name":"小明","age":18,"gender":"男"}'
    console.log(typeof str); // string
    console.log(str) // {"name":"小明","age":18,"gender":"男"}

    //这是一个JSON数组
    var str ='[{"name":"小明","age":18,"gender":"男"},{"name":"小红","age":18,"gender":"女"}]'
    console.log(typeof str); // string
    console.log(str) // [{"name":"小明","age":18,"gender":"男"},{"name":"小红","age":18,"gender":"女"}]
    ```

+ 如果我们要自己去写这些JSON还要一个个属性去加上""号太麻烦了,但js中为我们提供了方法让我们自由的转换     
1. JSON.parse(); parse:解析。可以把JSON解析成js对象

    JSON --> js对象
    ```
    // 这是一个JSON数组
     var str ='[{"name":"小明","age":18,"gender":"男"},{"name":"小红","age":18,"gender":"女"}]'

    var res=JSON.parse(str); // 进行转化
    console.log(typeof res); // object
    console.log(res[1]); //{"name":"小红","age":18,"gender":"女"}
    ```


2. JSON.stringify(); 可以把js对象解析成JSON

    js对象 --> JSON
    ```
    // 这是一个js对象
    var obj = {name:'小明',age:'18',gender:'男'};

    var res1=JSON.stringify(obj); // 进行转化
    console.log(typeof res1); // string
    console.log(res1);  // '{name:'小明',age:'18',gender:'男'}'
    ```


3. eval();

    + JSON.parse()与JSON.stringify()并不能兼容IE7及一下,如果使用会报JSON未定义

    + eval()方法可以兼容IE7及一下的浏览器进行转化 js对象-->JSON

    eval();这个函数可以执行一段以字符串形式的js代码,并返回结果
    ```
    var str = 'console.log("我使用eval()函数执行了这一串代码")';
    eval(str); // 我使用eval()函数执行了这一串代码
    ```


    注意：如果使用eval()执行的字符串中含有{},它会将{}当成是代码块，如果不喜欢将其当成代码块解析,则需要在字符串前后各加一个()
    ```
    var obj = '{name:"小明",age:18,gender:"男"}'
    eval(obj); //报错


    需加括号
    var obj1 = '({name:"小明",age:18,gender:"男"})'
    var res = eval(obj1); 
    console.log(res); // {name:"小明",age:18,gender:"男"}

    var res1 = eval('(' + obj + ')') 
    consol.log(res1) // {name:"小明",age:18,gender:"男"}
    ```
    注意：eval()这个函数功能很强大,可以直接执行一个字符串中的js代码,但是在开发中尽量不要使用,首先它的执行性能比较差,然后它还具有安全隐患,如果需要兼容IE7及以下的话可以使用js外部代码引入

