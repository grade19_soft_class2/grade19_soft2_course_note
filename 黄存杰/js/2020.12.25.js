'use strict'

// let menu = document.getElementById('drink-menu');  // 只能根据样式的Id查找节点

// console.log(menu);

// let drinks = document.getElementsByTagName('dt'); // 根据标签的名称查找节点

// console.log(drinks);

// let d = document.querySelector('#divid');  // 匹配第一个节点
// console.log(d);

// let d2 = document.querySelectorAll('#divid');  // 匹配多个节点
// console.log(d2);  

let d = document.querySelector('#divid');

console.log(d);

// d.innerHTML = '<font color=red>修改了第一个样式</font>'

d.textContent='修改了第一个样式'  // textContent属性单独修改文本信息
d.style.color= 'yellow';

// setInterval(() => {
//    d.innerHTML = '<font color=yellow>第一个样式变颜色了</font>';
// }, 5000);



document.title='冰雪奇缘'


{/* <div>
        <dl id="drink-menu" style="border:solid 1px #ccc;padding:6px;">
            <dt>摩卡</dt>
            <dd>热摩卡咖啡</dd>
            <dt>酸奶</dt>
            <dd>北京老酸奶</dd>
            <dt>果汁</dt>
            <dd>鲜榨苹果汁</dd>
        </dl>
    </div>
    <div id="divid">
        <p>第一个样式</p>
    </div>
    <div id="divid">
        <p>第二个样式</p>
    </div>
    <script src="./js/2020.12.25.js"></script> */}