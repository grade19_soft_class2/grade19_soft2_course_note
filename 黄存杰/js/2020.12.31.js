'use strict'


var
    form = $('#test-form'),
    selectAll = $('label[class=selectAll]>input'), // 获取到全选不全选的复选框
    selectAllLabel = $('span[class=selectAll]'), // 全选
    deselectAllLabel = $('span[class=deselectAll]'), // 全不选
    langs = $('[name=lang]'), // 所有name为lang 的 input
    a = $('a[class=invertSelect]'), // 反选
    s = $('[name=lang]:checked')
    console.log(s);

window.onload = function () {

    /*
     * 当用户勾上“全选”时，自动选中所有语言，并把“全选”变成“全不选”；
     * 当用户去掉“全不选”时，自动不选中所有语言；
     */

    //给input复选框设置一个改变事件
    selectAll.change(function () {
        //如果input发生改变就判断 input复选框的checked方法 
        // 如果为true 那么 显示全不选,隐藏全选，否则反之
        if (this.checked) {
            deselectAllLabel.show();// 显示全不选
            selectAllLabel.hide();// 隐藏全选
            // selectAll.css('visibility','visible');
            // deselectAll.css('visibility','hidden');

        } else {
            selectAllLabel.show(); // 显示全选
            deselectAllLabel.hide(); // 隐藏全不选
            // deselectAll.css('visibility','visible');
            // selectAll.css('visibility','hidden');
        }

        // 遍历lang
        // checked属性为 true / false
        for (let i = 0; i < langs.length; i++) {
            langs[i].checked = this.checked;  // 如果input.checked状态为true那么langs复选框的状态也为true,反之        
        }
    })


    /*
     *当用户点击“反选”时，自动把所有语言状态反转（选中的变为未选，未选的变为选中）；
     */

    // 给a标签设置一个单击事件
    a.on('click', function () {

        // 遍历lang 
        for (let i = 0; i < langs.length; i++) {
            // 点击后状态取反
            langs[i].checked = !langs[i].checked  


            // 判断 如果有一个lang复选框没有没勾选,那么显示全选，隐藏全不选，并且把 selectAll 复选框勾取消
            if(!langs[i].checked){
                selectAll.prop('checked',false); // 设置 selectAll 复选框 为 不勾选状态
                selectAllLabel.show(); // 显示全选 
                deselectAllLabel.hide(); // 隐藏全不选
            }else{  // 如果lang复选框全部勾选,那么显示全不选，隐藏全选，并且把 selectAll 复选框勾上
                selectAll.prop('checked',true);  // 设置 selectAll 复选框 为 勾选状态
                selectAllLabel.hide(); // 隐藏全选
                deselectAllLabel.show();// 显示全不选 
            }
        }
        

    })



    /*
     * 当用户把所有语言都手动勾上时，“全选”被自动勾上，并变为“全不选”；
     * 当用户手动去掉选中至少一种语言时，“全不选”自动被去掉选中，并变为“全选” 
     */

    // 使用map方法循环所有的lang复选框
    langs.map(function(){
        //给所有langs复选框添加 change 事件
        langs.change(function(){//当用户把所有语言都手动勾上时，“全选”被自动勾上，并变为“全不选”；
             if($('[name=lang]:checked').length === langs.length){// 如果 langs 复选框的checked 属性长度为 langs.length 说明全部勾选
                 selectAll.prop('checked',true); // 设置 selectAll 复选框 为 勾选状态
                 selectAllLabel.hide(); // 隐藏全选
                 deselectAllLabel.show(); // 显示全不选
             }else{//当用户手动去掉选中至少一种语言时，“全不选”自动被去掉选中，并变为“全选”。
                 selectAll.prop('checked',false); // 设置 selectAll 复选框 为 不勾选状态
                 selectAllLabel.show(); // 显示全选
                 deselectAllLabel.hide(); // 隐藏全不选 
             }
         });
     });


}



// 重置初始化状态:
form.find('*').show().off();
form.find(':checkbox').prop('checked', false).off();
deselectAllLabel.hide();
// 拦截form提交事件:
form.off().submit(function (e) {
    e.preventDefault();
    alert(form.serialize());
});
// TODO:绑定事件

// 测试:
console.log('请测试功能是否正常。');



