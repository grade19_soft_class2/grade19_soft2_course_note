// typeof 数据

// console.log(typeof 1234);
// console.log(typeof "我是林伟强");
// console.log(typeof true);
// console.log(typeof undefined);
// console.log(typeof null);

// console.log(typeof 123);
// console.log(typeof '林伟强');
// console.log(typeof true);
// console.log(typeof undefined);
// console.log(null);

    // var a = ['123','2','3'];

    // console.log(a);

/**
 * 强转（强制转换） 
 * 
 *   转换为 String 类型
 * 将其它数据类型转换为 字符串 有三种方式： toString()、String()、拼串（+）。
 */

 // 方法一

    // var a = 123;

    // a = a.toString();

    // console.log(a);
    // console.log(typeof a);

 // 方法二

        // var a = 123;

        // a = String(a);

        // console.log(a);
        // console.log(typeof a);
        // var s = typeof a;
        // console.log('a的数据类型'+s);
        
        // var b = undefined;
        // b = String(b);
        // console.log(b);
        // console.log(typeof b);

        // var c = null;
        // c = String(c);
        // console.log(c);
        // console.log(typeof c);

// 方法三 ： 任意的数据类型   +"" 的形式

    // var a = 123;

    // a = a + "";

    // console.log(a);
    // console.log(typeof a);


/**
 * 转换为Number类型
 */

 /**
  * 方式一：使用Number()函数
  * 
  * + 字符串 => 数字
  *     1. 如果是纯数字，直接转换为数值型数据
  *     2. 如果字符串有非数字内容，转换为  NaN
  *     3. 如果字符串是一个空串 或者 是一个全是空串的字符串，则转为 0
  * 
  * + 布尔 => 数字
  *     true 转为 1
  *     false 转为 0
  * 
  * + null => 数字
  *     null 转为 0
  * 
  * + undefined => 数字
  *     undefined 转为 0
  */

  // 方法二： 这种方式专门用来对付字符串，parseInt() 把 “一个” 字符串转换为 “一个” 整数

  var a = '123';
  a = parseInt(a);
  console.log(a);
  console.log(typeof a);

  // 方法三： 这种方式专门处理字符串， parseFloat() 把一个字符串转换为浮点数

  // 若出现重复声明，不报错，会出现在同一个环境作用域下的覆盖及误操作
  var b = '123.321';
  b = parseFloat();
  console.log(a);
  console.log(typeof a);

  /**
   *    转换为 Boolean 类型
   * 将其它的数据类型转换为 Boolean, 只能使用 Boolean() 函数
   */

   /**
    * 使用 Boolean() 函数
    * 
    * + 数字 => 布尔
    *   除了 0 和 NaN， 其余都为 true
    * 
    * + 字符串 => 布尔
    *   除了空串，起于都为true
    * 
    * + null and undefined 都会转换为 false
    * 
    * + 对象也会转化为 true
    * 
    */

    