// 创建三个对象，包括： name，age, sayName 键值对
    // var person1 = {
    //     name: '林伟强',
    //     age: 18,
    //     sayName: function(){
    //         console.log(this.name);
    //     }
    // };

    // var person2 = {
    //     name: '孙悟空',
    //     age: 999,
    //     sayName: function(){
    //         console.log(this.name);
    //     }
    // };

    // var person3 = {
    //     name: '猪八戒',
    //     age: 998,
    //     sayName: function(){
    //         console.log(this.name);
    //     }
    // }

    // console.log(person1);
    // console.log(person2);
    // console.log(person3);

// 上述代码可以创建很多对象，可这种方式过于僵，对于少量对象可以使用。
// 建设要创 100 or 200 or 500 个对象，这种方式似乎就不合适了：

// 使用工厂模式创建对象

// function createPerson(){
//     // 创建新的对象
//     var obj = new Object();
//     // 设置对象属性
//     obj.name = '孙悟空';
//     obj.age = 888;
//     obj.sayName = function(){
//         console.log(this.name);
//     }
//     // 返回新的对象
//     return obj;
// }

// var person1 = createPerson();
// var person2 = createPerson();
// var person3 = createPerson();

// console.log(person1);
// console.log(person2);
// console.log(person3);

// 这一串代码，比之前简洁许多，可属性相同，若对象需要不同的属性需用用上this指向

// 使用工厂模式创建对象
    // function createPerson(name, age){
    //     // 创建新的对象
    //     var obj = new Object();
    //     // 设置对象属性
    //     obj.name = name;
    //     obj.age = age;
    //     // 设置对象方法
    //     obj.sayName = function () {
    //         console.log(this.name);
    //     };
    //     // 返回对象
    //     return obj
    // }

    // var person1 = createPerson('林伟强', 21);
    // var person2 = createPerson('林小士', 22);
    // var person3 = createPerson('林小生', 23);

    // console.log(person1);
    // console.log(person2);
    // console.log(person3);


// 使用工行模式创建对象
function createPerson(name, age){
    // 创建新的对象
    var obj = new Object();
    //　设置对象属性
    obj.name = name;
    obj.age = age;
    //　设置对象方法
    obj.sayName = function(){
        console.log(this.name);
    };
    //　返回新的对象
    return obj;
}

// 创建一个姓名数组
var arrName = [];
// 创建一个年龄数组
var arrAge = [];

for(var i = 1; i < 1000; i++){
    arrName[i] = '名称' + i;
    arrAge[i] = i;
}

for(i = 1; i < 1000; i++){
            var person = createPerson(arrName[i],1000-i);
            console.log(person);
}