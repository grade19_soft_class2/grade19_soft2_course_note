// 使用构造函数创建对象，之前用工厂模式创建的对象都是Object类型的。

// JavaScript 中，构造函数使用 new 关键字来调用的函数，称为构造函数
/**
 * 一般创建对象那的方式，可以发现会出现很多无意义的代码， 如果一个班上有40个学生，就这一块而言，就需要写40遍代码
 * 如果是一个学校的学生，每年都需要写入大量无意义重复性的代码，降低系统的效率
 * 
 *   var p1 = {name: 'zs', age: 6, gender: '男', address: '福建省福州市马尾区'};
 *   var p2 = {name: 'lwq', age: 5, gender: '男', address: '福建省福州市马尾区'};
 *   var p3 = {name: 'ljj', age: 7, gender: '男', address: '福建省福州市马尾区'};
 *   var p4 = {name: 'zbb', age: 8, gender: '女', address: '福建省福州市仓山区福建交通船政学院'};
 * 
 * 上栗，每个对象都有共有的属性，但是它们的内容不同，可以把这些属性当做构造函数的参数传递
 * 设年龄：20岁，写死，遇到特殊情况单独处理，okok
 */

    function Person(name, gender, address){
        this.name = name;
        this.gender = gender;
        this.address = address;
    }

    var person1 = new Person('zs','男','福建省福州市马尾区');
    var person2 = new Person('lwq','男','福建省龙岩市新罗区');
    var person3 = new Person('lxs','男','福建省福州市马尾区');
    var person4 = new Person('zbb','女','福建省福州市仓山区');

    console.log(person1);
    console.log(person2);
    console.log(person3);
    console.log(person4);

    // 可以感受到，创建对象这一件事变得非常的方便。
    // 虽然，封装构造函数的过程会比较麻烦，可是，一旦封装成功，继续创建对象将是非常轻松的事情，这也是我们为什么要使用构造函数的原因

    /**
     * 什么是字面量
     * 
     * 
     * 什么是名值对
     * 
     * 
     * 什么是 索引
     * 
     * 
     */