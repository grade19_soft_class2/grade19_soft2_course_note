# 使用扩展方法有几个值得注意的地方

（1）扩展方法不能和调用的方法放到同一个类中

（2）第一个参数必须要，并且必须是this，这是扩展方法的标识。如果方法里面还要传入其他参数，可以在后面追加参数

（3）扩展方法所在的类必须是静态类

（4）最好保证扩展方法和调用方法在同一个命名空间下
(5) 扩展方法理论上可以无限套娃



# 拓展方法
```
// 在同一个命名空间下面定义的所有的string类型的变量都可以.GetNotNullStr()这样直接调用`
namespace ConsoleApp1
{
    public class Program
    {
        static void Main(string[] args)
        {
            // 定义一个null
            string strText = null;
            // 调用拓展方法
            var str = strText.GetNotNullStr(); // strText就是作为参数传入到方法里面的
            Console.WriteLine(str);
            Console.ReadKey();
        }
    }
    // 拓展方法不能和调用的方法放到同一个类里
   
    public static class Str
    {
        // this string就表示给string对象添加扩展方法
        public static string GetNotNullStr(this string str)
        {
            if (str == null)
            {
                return string.Empty;
            }
            else
            {
                return str;
            }
        }
    }
}
```

##  自定义对象拓展方法
```
amespace ConsoleApp1
{
    public class Program
    {
        static void Main(string[] args)
        {
            // 实例化Person类
            Person per = new Person();
            // 给Age属性赋值
            per.Age = 18;
            // 调用拓展方法
            var isOk = per.GetIsNotChild();
            // 输出
            Console.WriteLine(isOk);
            Console.ReadKey();
        }
    }

    // 创建Person类
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }

    // 创建拓展方法
    public static class Str
    {
        public static bool GetIsNotChild(this Person per)
        {
            if (per.Age >= 18)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
`````````````````````````````````