# 变量作用域
* 大致按照就近原则，先用局部变量再看全局变量，如果都没有就返回undefined。
但是要注意的是：
JavaScript默认有一个全局对象window，全局作用域的变量实际上被绑定到window的一个属性，顶层函数的定义也被视为一个全局变量，并绑定到window对象：
window变量也是可以改的查看的方法是输出window
```
// 唯一的全局变量gitadd
var gitadd={}//这是一个名字空间。用来减少局部的冲突和用于区分其他同名的变量
//名字空间属性
gitadd.name="456";
gitadd.add="1.0.0.1"
//名字空间函数
var gitadd.git=function ()//也是一个全局变量
{
    for（var i=0；i<2;i++）
    {
        let c=1；
        const v=1;

    }
    console.log(i);//在函数内声明的局部变量，都能用无论在函数哪里。
    console.log(c);//但是如果用let定义变量那么，let是块作用域的语句就只能在定义的语句块里用。
    console.log(v);//const和let也是一样的,但是名字要全大写，且只能定义常量。
}
alert（course）；//alert和course是默认全局变量window的属性和方法，用window.alert(window.course)和前面写的其实是一样的

```
比较需要注意的是：
```
funtcion  ftun(a,b)
{
    var a='123'+b;
    console.log(a);//返回123undefined
    var b='456';


}

```
因为在读取过程中实际是
```
funtcion  ftun(a,b)
{
    var a;
    var b;
    a='123'+b;
    console.log(a);//返回123undefined
    b='456';
后面即使在y赋值以后输出也没有用了，因为前面已经赋值过了
conles.log(a);//返回123undefined
}

```
由于JavaScript的这一怪异的“特性”，我们在函数内部定义变量时，请严格遵守“在函数内部首先申明所有变量”这一规则。