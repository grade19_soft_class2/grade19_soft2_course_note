<!-- TOC -->autoauto- [1. 委托](#1-委托)auto    - [1.1.](#11)auto    - [1.2.](#12)autoauto<!-- /TOC -->
# 1. 委托
1.为什么要用委托，节省时间，实际上委托是一个人工提前写好的隐方法用于帮助人们更好的在数值等需要循环和遍历的数据上精确提前数据
## 1.1.  
使用for循环很麻烦，而且不可维护和可读。C#2.0引入了delegate，可以使用委托来处理这种场景，代码如
## 1.2.  

# 内置委托
## Func类的委托

1.Func(TResult)委托封装封装一个不具有参数但却返回 TResult 参数指定的类型值的方法

2.Func(T,TResult)委托 封装一个具有一个参数并返回 TResult 参数指定的类型值的方法

3.Func(T1,T2,TResult)委托 封装一个具有两个参数并返回 TResult 参数指定的类型值的方法
17.Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,TResult>委托 封装一个方法，该方法具有16个参数，并返回TResult参数所指定的类型的值
```
static void Main(string[] args)
        {
            #region Func<T,TResult>委托示例
            //需求：查找整型集合list中大于3的所有元素组成的新集合，并打印出集合元素
            
            List<int> list = new List<int>() { 1, 2, 3, 4, 5 };
            
            //将匿名方法分配给 Func<T,TResult> 委托实例
            Func<int, bool> concat1 = delegate(int i) { return i > 3; };
            var newlist1 = list.Where(concat1).ToList();
            
            //将 Lambda 表达式分配给 Func<T,TResult> 委托实例
            Func<int, bool> concat2 = i => i > 3;
            var newlist2 = list.Where(concat2).ToList();

            newlist1.ForEach(i => Console.WriteLine(i.ToString()));
            newlist2.ForEach(i => Console.WriteLine(i.ToString()));
            Console.ReadKey();
            #endregion
        }
```