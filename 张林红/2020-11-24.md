
## null和undefined

```

null表示一个空的值，而undefined表示值未定义。

undefined仅仅在判断函数参数是否传递的情况下有用。

undefined表示"缺少值"，就是此处应该有一个值，但是还没有定义。典型用法是：

（1）变量被声明了，但没有赋值时，就等于undefined。

（2) 调用函数时，应该提供的参数没有提供，该参数等于undefined。

（3）对象没有赋值的属性，该属性的值为undefined。

（4）函数没有返回值时，默认返回undefined。

判断一个值是否存在就可以使用Undefined


```
## 数组

```

var arr=['111',111,'你好','呀',true]

console.log(arr[1])     //111
console.log(arr[5])     //索引超出了范围，返回undefined

```

## 对象

```

var StudentInfo={
    Id:1,
    name:'Alice',
    Age:18,
    sex:'女'
}
console.log(StudentInfo.name)   //Alice
console.log(StudentInfo)

JavaScript对象的键都是字符串类型，值可以是任意数据类型

```
## 变量

```

JavaScript 变量是存储数据值的容器。

var x = 7; //x 存储值 7
var y = 8; //y 存储值 8
var z = x + y; //z 存储值 15

var a=123;
a='abc'

console.log(a) //abc


var x = 10;
x = x + 2;

console.log(x) //12
```
```
构造变量名称（唯一标识符）的通用规则是：

名称可包含字母、数字、下划线和美元符号
名称必须以字母开头
名称也可以 $ 和 _ 开头（但是在本教程中我们不会这么做）
名称对大小写敏感（y 和 Y 是不同的变量）
保留字（比如 JavaScript 的关键词）无法用作变量名称
提示：JavaScript 标识符对大小写敏感。
```

## strict模式

```

为了修补JavaScript这一严重设计缺陷，ECMA在后续规范中推出了strict模式，在strict模式下运行的JavaScript代码，强制通过var申明变量，未使用var申明变量就使用的，将导致运行错误。

启用strict模式的方法是在JavaScript代码的第一行写上：

'use strict';
这是一个字符串，不支持strict模式的浏览器会把它当做一个字符串语句执行，支持strict模式的浏览器将开启strict模式运行JavaScript。

'use strict';
// 如果浏览器支持strict模式，
// 下面的代码将报ReferenceError错误:
abc = 'Hello, world';
console.log(abc);

支持严格模式的浏览器:
Internet Explorer 10 +、 Firefox 4+ Chrome 13+、 Safari 5.1+、 Opera 12+。


<script>
"use strict";
x = 3.14;       // 报错 (x 未定义)
</script>

```
### 为什么使用严格模式:
```
消除Javascript语法的一些不合理、不严谨之处，减少一些怪异行为;
消除代码运行的一些不安全之处，保证代码运行的安全；
提高编译器效率，增加运行速度；
为未来新版本的Javascript做好铺垫。
"严格模式"体现了Javascript更合理、更安全、更严谨的发展方向，包括IE 10在内的主流浏览器，都已经支持它，许多大项目已经开始全面拥抱它。

另一方面，同样的代码，在"严格模式"中，可能会有不一样的运行结果；一些在"正常模式"下可以运行的语句，在"严格模式"下将不能运行。


```
### 严格模式的限制：
```
不允许使用未声明的变量。
对象也是一个变量。
不允许删除变量或对象。
不允许删除函数。
不允许变量重名。
不允许使用八进制。
不允许对一个使用getter方法读取的属性进行赋值。
不允许删除一个不允许删除的属性。等。。
