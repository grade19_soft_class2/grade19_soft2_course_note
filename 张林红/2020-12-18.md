## generator

generator由function*定义（注意多出的*号），并且，除了return语句，还可以用yield返回多次。

函数只能返回一次，所以必须返回一个Array。但是，如果换成generator，就可以一次返回一个数，不断返回多次

调用generator对象两个方法：

一是不断的使用generator对象的next()方法
```
function* foo() {
    yield 'hello';
    yield 'world';
    return 'ending';
}

var hw = foo();
console.log(hw.next());
console.log(hw.next());
console.log(hw.next()); ////第三次调用，Generator函数从上次yield表达式停下的地方，一直执行到return语句（如果没有return语句，则value属性为undefined），done属性为true，表示遍历已经执行结束。
console.log(hw.next()); 
console.log(hw.next()); 
```
结果：

![次元](./imgs/2020-12-18_01.png)

二是使用for ... of循环迭代generator对象

```
function* foo() {
    yield 'hello';
    yield 'world';
    return  'ending';
}
for(var x of foo()){
    console.log(x);
}
```
结果：

![次元](./imgs/2020-12-18_02.png)

因为generator可以在执行过程中多次返回，所以它看上去就像一个可以记住执行状态的函数，利用这一点，generator就可以实现需要用面向对象才能实现的功能。

```
var fib = {
    a: 0,
    b: 1,
    n: 0,
    max: 5,
    next: function () {
        var
            r = this.a,
            t = this.a + this.b;
        this.a = this.b;
        this.b = t;
        if (this.n < this.max) {
            this.n ++;
            return r;
        } else {
            return undefined;
        }
    }
};
console.log(fib.next());//0
```

generator还有另一个巨大的好处，就是把异步回调代码变成“同步”代码。

### 练习
要生成一个自增的ID，可以编写一个next_id()函数：
```
var current_id = 0;

function next_id() {
    current_id ++;
    return current_id;
}
```
由于函数无法保存状态，故需要一个全局变量current_id来保存数字。

不用闭包，试用generator改写：
```
'use strict';
function* next_id() {
    var current_id = 0;
    while(true){
       current_id++; 
       yield current_id;
    }
}

// 测试:
var
    x,
    pass = true,
    g = next_id();
for (x = 1; x < 100; x ++) {
    if (g.next().value !== x) {
        pass = false;
        console.log('测试失败!');
        break;
    }
}
if (pass) {
    console.log('测试通过!');
}
```


## 标准对象

在JavaScript的世界里，一切都是对象。

但是某些对象还是和其他对象不太一样。为了区分对象的类型，我们用typeof操作符获取对象的类型，它总是返回一个字符串：
```
typeof 123; // 'number'
typeof NaN; // 'number'
typeof 'str'; // 'string'
typeof true; // 'boolean'
typeof undefined; // 'undefined'
typeof Math.abs; // 'function'
typeof null; // 'object'
typeof []; // 'object'
typeof {}; // 'object'
```
可见，number、string、boolean、function和undefined有别于其他类型。特别注意null的类型是object，Array的类型也是object，如果我们用typeof将无法区分出null、Array和通常意义上的object——{}。

## 包装对象

定义

所谓“包装对象”，指的是与数值、字符串、布尔值分别相对应的Number、String、Boolean三个原生对象。这三个原生对象可以把原始类型的值变成（包装成）对象。
```
var v1 = new Number(123);
var v2 = new String('abc');
var v3 = new Boolean(true);

console.log(typeof v1 );
console.log(typeof v2 );
console.log(typeof v3 );
console.log(v1 === 123);
console.log(v2 === 'abc' );
console.log(v3 === true);
```
结果：

![次元](./imgs/2020-12-18_03.png)

总结一下，有这么几条规则需要遵守：

+ 不要使用new Number()、new Boolean()、new String()创建包装对象；

+ 用parseInt()或parseFloat()来转换任意类型到number；

+ 用String()来转换任意类型到string，或者直接调用某个对象的toString()方法；

+ 通常不必把任意类型转换为boolean再判断，因为可以直接写if (myVar) {...}；

+ typeof操作符可以判断出number、boolean、string、function和undefined；

+ 判断Array要使用Array.isArray(arr)；

+ 判断null请使用myVar === null；

+ 判断某个全局变量是否存在用typeof window.myVar === 'undefined'；

+ 函数内部判断某个变量是否存在用typeof myVar === 'undefined'。

+ null和undefined 没有toString()方法 这两个特殊值要除外，虽然null还伪装成了object类型。

number对象调用toString()报SyntaxError：
```
123.toString(); // SyntaxError
```
遇到这种情况，要特殊处理一下：
```
123..toString(); // '123', 注意是两个点！
(123).toString(); // '123'
```
## Date
Date对象用来表示日期和时间。

要获取系统当前时间，用：
```
var now = new Date();
now; // Wed Jun 24 2015 19:49:22 GMT+0800 (CST)
now.getFullYear(); // 2015, 年份
now.getMonth(); // 5, 月份，注意月份范围是0~11，5表示六月
now.getDate(); // 24, 表示24号
now.getDay(); // 3, 表示星期三
now.getHours(); // 19, 24小时制
now.getMinutes(); // 49, 分钟
now.getSeconds(); // 22, 秒
now.getMilliseconds(); // 875, 毫秒数
now.getTime(); // 1435146562875, 以number形式表示的时间戳
```
JavaScript的月份范围用整数表示是0~11，0表示一月，1表示二月……，所以要表示12月，我们传入的是11
```
var d = new Date(2019, 12, 18, 20, 15, 30, 123);
console.log(d);
var d1 = Date.parse('2020-12-18T20:15:30.875+08:00')
console.log(d1);
var d2=new Date(1608293730875);
console.log(d2);
console.log(d2.getMonth());
```
![次元](./imgs/2020-12-18_04.png)
使用Date.parse()时传入的字符串使用实际月份01~12，转换为Date对象后getMonth()获取的月份值为0~11。
### 时区
Date对象表示的时间总是按浏览器所在时区显示的，不过我们既可以显示本地时间，也可以显示调整后的UTC时间：
```
var d = new Date(1608282647495);
console.log(d.toLocaleString());// 显示的字符串与操作系统设定的格式有关
console.log(d.toUTCString()); // UTC时间，与本地时间相差8小时
```
那么在JavaScript中如何进行时区转换呢？实际上，只要我们传递的是一个number类型的时间戳，我们就不用关心时区转换。任何浏览器都可以把一个时间戳正确转换为本地时间。

时间戳是个什么东西？时间戳是一个自增的整数，它表示从1970年1月1日零时整的GMT时区开始的那一刻，到现在的毫秒数。假设浏览器所在电脑的时间是准确的，那么世界上无论哪个时区的电脑，它们此刻产生的时间戳数字都是一样的，所以，时间戳可以精确地表示一个时刻，并且与时区无关。

所以，我们只需要传递时间戳，或者把时间戳从数据库里读出来，再让JavaScript自动转换为当地时间就可以了。

要获取当前时间戳，可以用：
```
'use strict';
if (Date.now) {
    console.log(Date.now()); // 老版本IE没有now()方法
} else {
    console.log(new Date().getTime());
}
```
 
## RegExp
正则表达式是一种用来匹配字符串的强有力的武器。它的设计思想是用一种描述性的语言来给字符串定义一个规则，凡是符合规则的字符串，我们就认为它“匹配”了，否则，该字符串就是不合法的。

因为正则表达式也是用字符串表示的，所以，我们要首先了解如何用字符来描述字符。

所以我们判断一个字符串是否是合法的Email的方法是：

创建一个匹配Email的正则表达式；

用该正则表达式去匹配用户的输入来判断是否合法。

在正则表达式中，如果直接给出字符，就是精确匹配。用\d可以匹配一个数字，\w可以匹配一个字母或数字，所以：
```
'00\d'可以匹配'007'，但无法匹配'00A'；

'\d\d\d'可以匹配'010'；

'\w\w'可以匹配'js'；
```
.可以匹配任意字符，所以：
```
'js.'可以匹配'jsp'、'jss'、'js!'等等。
```
要匹配变长的字符，在正则表达式中，用*表示任意个字符（包括0个），用+表示至少一个字符，用?表示0个或1个字符，用{n}表示n个字符，用{n,m}表示n-m个字符：

来看一个复杂的例子：\d{3}\s+\d{3,8}。

我们来从左到右解读一下：

1. \d{3}表示匹配3个数字，例如'010'；

2. \s可以匹配一个空格（也包括Tab等空白符），所以\s+表示至少有一个空格，例如匹配' '，'\t\t'等；

3. \d{3,8}表示3-8个数字，例如'1234567'。

综合起来，上面的正则表达式可以匹配以任意个空格隔开的带区号的电话号码。

如果要匹配'010-12345'这样的号码呢？由于'-'是特殊字符，在正则表达式中，要用'\'转义，所以，上面的正则是\d{3}\-\d{3,8}。

但是，仍然无法匹配'010 - 12345'，因为带有空格。所以我们需要更复杂的匹配方式。
### 进阶
要做更精确地匹配，可以用[]表示范围，比如：
```
[0-9a-zA-Z\_]可以匹配一个数字、字母或者下划线；

[0-9a-zA-Z\_]+可以匹配至少由一个数字、字母或者下划线组成的字符串，比如'a100'，'0_Z'，'js2015'等等；

[a-zA-Z\_\$][0-9a-zA-Z\_\$]*可以匹配由字母或下划线、$开头，后接任意个由一个数字、字母或者下划线、$组成的字符串，也就是JavaScript允许的变量名；

[a-zA-Z\_\$][0-9a-zA-Z\_\$]{0, 19}更精确地限制了变量的长度是1-20个字符（前面1个字符+后面最多19个字符）。
```
A|B可以匹配A或B，所以(J|j)ava(S|s)cript可以匹配'JavaScript'、'Javascript'、'javaScript'或者'javascript'。

^表示行的开头，^\d表示必须以数字开头。

$表示行的结束，\d$表示必须以数字结束。

你可能注意到了，js也可以匹配'jsp'，但是加上^js$就变成了整行匹配，就只能匹配'js'了。
### RegExp
JavaScript有两种方式创建一个正则表达式：

第一种方式是直接通过/正则表达式/写出来，第二种方式是通过new RegExp('正则表达式')创建一个RegExp对象。

两种写法是一样的：
```
var re1 = /ABC\-001/;
var re2 = new RegExp('ABC\\-001');

re1; // /ABC\-001/
re2; // /ABC\-001/
```
注意，如果使用第二种写法，因为字符串的转义问题，字符串的两个\\实际上是一个\。

先看看如何判断正则表达式是否匹配：
```
var re = /^\d{3}\-\d{3,8}$/;
re.test('010-12345'); // true
re.test('010-1234x'); // false
re.test('010 12345'); // false
```
RegExp对象的test()方法用于测试给定的字符串是否符合条件。