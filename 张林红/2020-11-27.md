# 数组

### unshift和shift

如果要往Array的头部添加若干元素，使用unshift()方法，shift()方法则把Array的第一个元素删掉
```
var arr=[1,2,3];
arr.unshift('55','66')
console.log(arr)        //(5) ["55", "66", 1, 2, 3]

arr.shift()
console.log(arr)        //(4) ["66", 1, 2, 3]
```

### sort

sort()可以对当前Array进行排序，它会直接修改当前Array的元素位置，直接调用时，按照默认顺序排序

```
var res=[7,5,3,9,1];
console.log(res);//     (5) [7, 5, 3, 9, 1]
res.sort();
console.log(res);//     (5) [1, 3, 5, 7, 9]
```
### reverse

reverse()把整个Array的元素给调个个，也就是反转
```
var arr=[1,2,3];
arr.reverse();
console.log(arr);//3, 2, 1
```

### splice

splice()方法是修改Array的“万能方法”，它可以从指定的索引开始删除若干元素，然后再从该位置添加若干元素

```
//只删除
var arr1=['dadada','有有有',12,35,598,111];
arr1.splice(0,2)
console.log(arr1);              //[12, 35, 598, 111]
//只添加
arr1.splice(3,0,'啦啦啦',100);
console.log(arr1)               // ["dadada", "有有有", 12, "啦啦啦", 100, 35, 598, 111]
//又删又加
arr1.splice(0,2,'啦啦啦',100)   //["啦啦啦", 100, 12, 35, 598, 111]
console.log(arr1)
```

### concat

concat()方法把当前的Array和另一个Array连接起来，并返回一个新的Array

```
var arr=[1,2,3,4];
var newArr=arr.concat([5,6])
console.new(newArr);        //[1, 2, 3, 4, 5, 6]
```

### join

join()方法是一个非常实用的方法，它把当前Array的每个元素都用指定的字符串连接起来，然后返回连接后的字符串

```
var arr3=[1,2,3,4];

console.log(arr3.join('-'));//1-2-3-4

```

### 多维数组

如果数组的某个元素又是一个Array，则可以形成多维数组

```
var arr=[['111',222,'嘤嘤嘤'],[3,4,100],'-'];
var x=arr[1][2];
console.log(x); //100
```

### 练习

练习：在新生欢迎会上，你已经拿到了新同学的名单，请排序后显示：欢迎XXX，XXX，XXX和XXX同学！：
```
'use strict'
var arr = ['小明', '小红', '大军', '阿黄'];
arr.sort();
console.log(`欢迎${arr[0]}、${arr[1]}、${arr[2]}和${arr[3]}同学!`);

//console.log('欢迎'+arr.slice(0,3)+'和'+arr[3]+'同学')
```