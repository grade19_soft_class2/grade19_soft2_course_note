# 对象

JavaScript的对象是一种无序的集合数据类型，它由若干键值对组成。

JavaScript的对象用于描述现实世界中的某个对象。

JavaScript 中的所有事物都是对象：字符串、数字、数组、日期，等等。

在 JavaScript 中，对象是拥有属性和方法的数据。

### 属性和方法

属性是与对象相关的值。

方法是能够在对象上执行的动作。

```
var honghong={
    name:'honghong',
    age:17,
    height:168,
    'senior-high-school':'xx一中' 
};
console.log(honghong.age);
console.log(honghong["senior-high-school"]);
```

访问属性是通过.操作符完成的，但这要求属性名必须是一个有效的变量名。如果属性名包含特殊字符，就必须用''括起来.
honghong的属性名middle-school不是一个有效的变量，就需要用''括起来。访问这个属性也无法使用.操作符，必须用['xxx']来访问;

```

var honghong={
    name:'honghong',
    age:17,
    height:168,
    'senior-high-school':'xx一中' 
};
console.log(honghong.age);
console.log(honghong["senior-high-school"]);

```

实际上JavaScript对象的所有属性都是字符串，不过属性对应的值可以是任意数据类型。

JavaScript规定，访问不存在的属性不报错，而是返回undefined,

由于JavaScript的对象是动态类型，可以自由地给一个对象添加或删除属性：

```
var mingming={
    name:mingming
};
mingming.age=17;
console.log(mingming.age);//17
delete mingming.name;
console.log(mingming.name);//undefined;
```

如果我们要检测xiaoming是否拥有某一属性，可以用in操作符

如果in判断一个属性存在，这个属性不一定是xiaoming的，它可能是xiaoming继承得到的

因为toString定义在object对象中，而所有对象最终都会在原型链上指向object，所以xiaoming也拥有toString属性。

要判断一个属性是否是xiaoming自身拥有的，而不是继承得到的，可以用hasOwnProperty()方法：
```
var honghong={
    name:'honghong',
    age:17,
    height:168,
    'senior-high-school':'xx一中' 
};
console.log('name' in honghong); //true
console.log('grade' in honghong);//false
console.log('toString' in honghong);//true
xiaoming.hasOwnProperty('toString'); // false
```


# 条件判断

JavaScript使用if () { ... } else { ... }来进行条件判断；

```
var age=111;

if(age<90){
    alert('死亡')
}else{
    alert('长生')
}
```
其中else语句是可选的。如果语句块只包含一条语句，那么可以省略{};
如果多个语句则不能省略;


### 多行条件判断
```
var age=111;

if(age<99){
    alert(0)
}else if(age>200){
    alert(1)
}else{
    alert(age)
}//111
```
```
var age=111;
if(age<99){
    alert(0)
}else{
   if(age>200){
       alert(1)
   }else{
       alert(age)
   }
}
```


练习
小明身高1.75，体重80.5kg。请根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，并根据BMI指数：

低于18.5：过轻
18.5-25：正常
25-28：过重
28-32：肥胖
高于32：严重肥胖
用if...else...判断并显示结果：
```
'use strict';

var height = parseFloat(prompt('请输入身高(m):'));
var weight = parseFloat(prompt('请输入体重(kg):'));
var bmi = weight/(height*height);
if (bmi>=32){
    alert('严重肥胖')
}else if(28<bmi&&bmi<32){
    alert('肥胖')
}else if(25<=bmi&&bmi<28){
    alert('过重')
}else if(18.5<=bmi&&bmi<25){
    alert('正常')
}else{
    alert('过轻')
}
```