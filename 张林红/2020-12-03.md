## /跟./的区别：

/:根目录

./：当前目录

由于环境不同，当在js使用/时会进入电脑磁盘，因此获取不到文件夹中的文件。

# 循环

## for...in

for循环的一个变体是for ... in循环，它可以把一个对象的所有属性依次循环出来

for...in 语句用于对数组或者对象的属性进行循环操作。

for ... in 循环中的代码每执行一次，就会对数组的元素或者对象的属性进行一次操作。

```
var Info={
    name:'yy',
    age:17,
    sex:'女',
    friend:{
        one:'zz',
        two:'jj'
    }
}

for(var key in Info){

    console.log(key);//name age sex friend
 
    console.log(Info[key]);//yy    17    女   {one: "zz", two: "jj"}
};

var isFriend='friend' in Info;

console.log(isFriend);//true
```

要过滤掉对象继承的属性，用hasOwnProperty()来实现：

```
for(var key in Info){
    if (Info.hasOwnProperty(key)) {
        console.log(key); // name age sex friend;
    }
}
```

## while 

while循环只有一个判断条件，条件满足，就不断循环，条件不满足时则退出循环。

```
var x="",i=0;
	while (i<5){
		x=x + "该数字为 " + i ;
		i++;}
console.log(x);//该数字为 0该数字为 1该数字为 2该数字为 3该数字为 4

```
不再满足while条件，循环退出

## do while 

do...while 循环是 while 循环的变体。该循环会在检查条件是否为真之前执行一次代码块，然后如果条件为真的话，就会重复这个循环。

do { ... } while()循环，它和while循环的唯一区别在于，不是在每次循环开始的时候判断条件，而是在每次循环完成的时候判断条件

```
var x="",i=0;
	do{
		x=x + "该数字为 " + i;
        i++;
    }while(i<5);
console.log(x);//该数字为 0该数字为 1该数字为 2该数字为 3该数字为 4
```

## 练习

练习
请利用循环遍历数组中的每个名字，并显示Hello, xxx!：
```
'use strict';
var arr = ['Bart', 'Lisa', 'Adam'];
var i,x;
for (i=0;i<arr.length;i++){
    x=arr[i];
    console.log('Hello,'+x+'!')
};
```
![太阳](./imgs/2020-12-03.png)

# Map 和 Set

JavaScript的默认对象表示方式{}可以视为其他语言中的Map或Dictionary的数据结构，即一组键值对。

```
'use strict';
var m = new Map();
var s = new Set();
console.log('你的浏览器支持Map和Set！');
```
## Map
Map是一组键值对的结构，具有极快的查找速度。

```
'use strict';
var m = new Map();

m.set('yy',88);//添加新的Key-Value
m.set('xx',108);
console.log(m.get('yy'));//88
```
key只能对应一个value，所以，多次对一个key放入value，后面的值会把前面的值冲掉

## set
Set和Map类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在Set中，没有重复的key。

重复元素在Set中自动被过滤

```
'use strict';
var s = new Set([1,2,3,2,4]);
s.add(5);
console.log(s);//{1, 2, 3, 4, 5}
s.delete(3);
console.log(s);//{1, 2, 4, 5}
```

## iterable

Array、Map和Set都属于iterable类型。

具有iterable类型的集合可以通过新的for ... of循环来遍历。

```
'use strict';
var a = [1, 2, 3];
for (var x of a) {
    console.log(x)
}
console.log('你的浏览器支持for ... of');//1 2 3 你的浏览器支持for... of
```
```
'use strict';
var a = [1, 2, 3];
var s=new Set([11,22,33]);
var m=new Map([[1, 'x'], [2, 'y'], [3, 'z']]);


for (var x of a) {
    console.log(x)
};

for(var x of s){
    console.log(x)
};

for (var x of m){
    console.log(x)
};
```

for ... of循环和for ... in循环区别
```
'use strict';
var a = [1, 2, 3];

a.name='dada'

for (var x of a) {
    console.log(x)  //1 2 3
};

for(var x in a){
    console.log(x);// 1 2 3 name
}

```
for..in 的遍历实际上是对象的属性名称，一个Array数组也是一个对象，数组的每个元素的索引被视为属性名称，所以可以看到使用for...in 循环Array数组，拿到的其实是每个元素的索引

for...of 它只是循环集合本身的元素