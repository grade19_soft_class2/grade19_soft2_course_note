## JavaScript数组常用方法练习

1. 题目描述
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
    ```
    示例
    输入  [ 1, 2, 3, 4 ], 3
    输出   2
    ```
    ```
    解决方法：
    var arr=[1,2,3,4]
   function indexOf(arr, item) {
    if(arr.indexOf(item)>0){
        return arr.indexOf(item);
    }else{
        return -1;
    };
    }
    console.log(indexOf(arr,3));
    ```

2. 题目描述
计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
    ```
    示例
    输入  [ 1, 2, 3, 4 ]
    输出  10
    ```
    ```
    解决方法:
    var arr=[1,2,3,4]
    function sum(arr) {
    var sum = 0;
    arr.forEach(function(item,index){
    sum = sum + item;
    })
    return sum;
    }
    console.log(sum(arr));
    ```


3. 题目描述
移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4, 2], 2
    输出  [1, 3, 4]
    ```
    ```
    方法一；
    var arr=[1,2,3,4,2]

    function remove(arr,item){
        var reault=[];
        arr.foreach(function(arrItem){
            if(item!=arrItem){
                result.push(arrItem);
            }
        })
        return result;
    }
    console.log(remove(arr,2));
    方法二：
    function remove(arr,item) {
        var a=arr.slice(0);
        for(var i=0;i<arr.length;i++){
            if(a[i]==item){
                a.splice(i,1);
                i--;
                }
    }
    return a;
    };
    console.log(remove(arr,2));
    
    ```


4. 题目描述
    移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
    ```
    示例
    输入  [1, 2, 2, 3, 4, 2, 2], 2
    输出  [1, 3, 4]
    ```
    ```
    var arr =[1,2,2,3,4,2];
    function removeWithoutCopy(arr, item) {
            while(arr.indexOf(item) != -1){
                arr.splice(arr.indexOf(item),1)
                }
                return arr;
    }
    console.log(removeWithoutCopy(arr,2));
    ```


5. 题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4],  10
    输出  [1, 2, 3, 4, 10]
    ```
    ```
    方法一：
    var arr=[1,2,3,4]

    function append(arr, item) {
        return arr.concat(item);
    }
    console.log(append(arr,10))

    方法二：
    function append(arr, item) {
        var arr1 = arr.slice(0);
        arr1[arr1.length]=item;
        // arr1.push(item);
        return arr1;
    }
    console.log(append(arr,10))
    方法三：
    function append(arr, item) {
        var result = [];
        arr.forEach(function(arrItem){
            result.push(arrItem);
        })
        result.push(item);
        return result;
    }
    console.log(append(arr,10))
    ```

6. 题目描述
删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4]
    输出  [1, 2, 3]
    ```
    ```
    方法一：
    var arr=[1,2,3,4]
    function truncate(arr){
        var result=[];
        var arrItem=arr.lenght-1;
        while(arrItem--){
            result.unshfit(arr[arrItem])
        }
        return result
    }
    console.log(truncate(arr));


    方法二：
    var arr=[1,2,3,4]
    function truncate(arr) {
            var arr1 = arr.slice(0);
            arr1.pop(arr1.length-1);
            return arr1;
        }
    console.log(truncate(arr));


    方法三：
    var arr=[1,2,3,4];
    function truncate(arr) {
        var a = new Array();
        for (var i = 0;i < arr.length-1;i++){
            a.push(arr[i]);
        }
        return a;
    }
    console.log(truncate(arr));


    方法四：
    var arr=[1,2,3,4];
    function truncate(arr) {
            return arr.slice(0,3);
        }
    console.log(truncate(arr));
    
    ```


7. 题目描述
在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4], 10
    输出  [10, 1, 2, 3, 4]
    ```
    ```
   var arr=[1,2,3,4];

    function addFrist(arr,item){
        var result = arr.slice(0);
        result.unshift(item);
        return result;
    };
    console.log(addFrist(arr,10));
    ```


8. 题目描述
删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4]
    输出  [2, 3, 4]
    ```
    ```
    var arr=[1,2,3,4]
    function curtail(arr) {
                var result = arr.slice(0);
                result.shift();
                return result;
            }
    console.log(curtail(arr));
    ```


9. 题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4], ['a', 'b', 'c', 1]
    输出  [1, 2, 3, 4, 'a', 'b', 'c', 1]
    ```
    ```
    方法一：
    var arr1=[1, 2, 3, 4];
    var arr2=['a', 'b', 'c', 1];
    function concat(arr1, arr2) {
                var currentArr1 = arr1.slice(0);
                var currentArr2 = arr2.slice(0);
                return currentArr1.concat(currentArr2)
            }
    console.log(concat(arr1,arr2));


    方法二：
    var arr1=[1, 2, 3, 4];
    var arr2=['a', 'b', 'c', 1];
    function concat(arr1, arr2) {
            var arr = arr1.slice(0);
            arr = arr.concat(arr2);
            return arr;
        }
    console.log(concat(arr1,arr2))
    ```


10. 题目描述
在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4], 'z', 2
    输出  [1, 2, 'z', 3, 4]
    ```
    ```
    'use strict'
    var arr=[1,2,3,4];

    方法一：
    function index(arr,item,index){
        var newArr=arr.slice(0);
        newArr.splice(index,0,item);
        return newArr;
    }
    console.log(index(arr,'z',2))
    ```


11. 题目描述
统计数组 arr 中值等于 item 的元素出现的次数
    ```
    示例
    输入  [1, 2, 4, 4, 3, 4, 3], 4
    输出  3
    ```
    ```
    'use strict'
    var arr=[1,2,4,4,3,4,3];
    var item=4;
    var count=0;
    for(var i=0;i<arr.length;i++){
        if(arr[i]==item){
            count++
        }
    }
    console.log(count)

    ```


12. 题目描述
找出数组 arr 中重复出现过的元素
    ```
    示例
    输入  [1, 2, 4, 4, 3, 3, 1, 5, 3]
    输出  [1, 3, 4]
    ```
    ```
    ```


13. 题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
    ```
    示例
    输入  [1, 2, 3, 4]
    输出  [1, 4, 9, 16]
    ```
    ```
    ```


14. 题目描述
在数组 arr 中，查找值与 item 相等的元素出现的所有位置
    ```
    示例
    输入  'abcdefabc'
    输出  [0, 6]
    ```
    ```
    ```