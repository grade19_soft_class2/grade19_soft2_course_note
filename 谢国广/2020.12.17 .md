`友情提示：以下内容为个人主观理解，未经同意阅读后走火入魔本人概不负责！`
# 箭头函数
### 基本语法

```
(param1, param2, …, paramN) => { statements }
(param1, param2, …, paramN) => expression
//相当于：(param1, param2, …, paramN) =>{ return expression; }

// 当只有一个参数时，圆括号是可选的：
(singleParam) => { statements }
singleParam => { statements }

// 没有参数的函数应该写成一对圆括号。
() => { statements }
```
### 作用
1. 更简洁的函数。  

```
//例1
var elements = [
  'Hydrogen',
  'Helium',
  'Lithium',
  'Beryllium'
];

elements.map(function(element) {
  return element.length;
}); // 返回数组：[8, 6, 7, 9]

// 上面的普通函数可以改写成如下的箭头函数
elements.map((element) => {
  return element.length;
}); // [8, 6, 7, 9]

// 当箭头函数只有一个参数时，可以省略参数的圆括号
elements.map(element => {
 return element.length;
}); // [8, 6, 7, 9]

// 当箭头函数的函数体只有一个 `return` 语句时，可以省略 `return` 关键字和方法体的花括号
elements.map(element => element.length); // [8, 6, 7, 9]

// 在这个例子中，因为我们只需要 `length` 属性，所以可以使用参数解构
// 需要注意的是字符串 `"length"` 是我们想要获得的属性的名称，而 `lengthFooBArX` 则只是个变量名，
// 可以替换成任意合法的变量名
elements.map(({ "length": lengthFooBArX }) => lengthFooBArX); // [8, 6, 7, 9]
```

2. 没有独立的this。  

```
//例2
function Person() {
  // Person() 构造函数定义 `this`作为它自己的实例.
  this.age = 0;

  setInterval(function growUp() {
    // 在非严格模式, growUp()函数定义 `this`作为全局对象,
    // 与在 Person()构造函数中定义的 `this`并不相同.
    this.age++;
  }, 1000);
}

var p = new Person();
```
### 练习
 **请使用箭头函数简化排序时传入的函数** 
- 方法1：
```
'use strict'
var arr = [10, 20, 1, 2];
arr.sort((x, y) => {
    return x-y
});
console.log(arr); // [1, 2, 10, 20]
```
> 结果如下：  
![效果1](https://images.gitee.com/uploads/images/2020/1217/173813_8245ed05_7446325.png "2020-12-17_172944.png")

- 方法2：
```
'use strict'
var arr = [10,20,1,2];
arr.sort((x,y) =>x-y);
console.log(arr);
```
> 结果如下：  
![效果2](https://images.gitee.com/uploads/images/2020/1217/173943_7d868e62_7446325.png "2020-12-17_173905.png")