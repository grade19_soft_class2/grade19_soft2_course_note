# 变量提升
- 函数及变量的声明都将被提升到函数的最顶部。  
- 变量可以在使用后声明，也就是变量可以先使用再声明。  
- 只有声明的变量会提升，初始化的不会。
```
// 例一

x = 5; // 变量 x 设置为 5
elem = document.getElementById("demo"); // 查找元素
elem.innerHTML = x;                     // 在元素中显示 x
var x; // 声明 x
```
```
// 例二

var x; // 声明 x
x = 5; // 变量 x 设置为 5
elem = document.getElementById("demo"); // 查找元素
elem.innerHTML = x;                     // 在元素中显示 x
```
