## 递归：是一种特殊的执行程序，它是用方法调用自身的形式实现的，让程序代码循环执行。
+ 使用递归实现计算所输入数的阶乘


例如计算 5 的阶乘，则是 5*4*3*2*1 的结果。根据题目要求，实现的代码如下。

```
//创建一个类
class FactorialClass
{
    public static int Factorial(int n)
    {
        if(n == 0)
        {
            return 1;
        }
        return n * Factorial(n - 1);
    }
}

在 Main 方法中调用该静态方法，代码如下。
class Program
{
    static void Main(string[] args)
    {
        int rs = FactorialClass.Factorial(5);
        Console.WriteLine("结果是：" + rs);
    }
}
```
+ 实现递归的部分是由 n * Factorial(n-l) 语句实现的

---
+ 递归计算斐波那契数列
```
    class FactorialClass
    {

        public static int Factorial(int n)
        {


            if (n <= 0)
            {
                return 0;
            }
            if (n == 1 || n == 2)
            {
                return 1;
            }
            return Factorial(n - 2) + Factorial(n - 1);

        }

    }
    ```
    调用
    ```


            static void Main(string[] args)
        {
            int rs = FactorialClass.Factorial(7);
            Console.WriteLine("结果是：" + rs);
        }
     ```