## 解构赋值
用解构赋值，直接对多个变量同时赋值如下：
```
var [x, y, z] = ['hello', 'JavaScript', 'ES6'];
```
或
```
var arr=['不负韶华','与君共勉','阁下已乘风起','何不扶摇直上九万里'];

var [a,b,c,d]=arr
console.log(a)
console.log(b)
console.log(c)
console.log(d)
```
+ 解构赋值，多个变量要用[]括起来
+ 如果数组本身还有嵌套，进行解构赋值时嵌套层次和位置哟啊保持一致
```
var [a,[b,c]]=['南风起',['鲸一落','万物生'];
```
+ 解构赋值还可以忽略某些元素，如下：
```
var [,,c]=['123','46','45'];
console.log(c)//忽略前两个输出45
```
+ 也可以使用解构赋值从一个对象中取出若干属性，便于快速获取对象的指定属性
```
var person={
    name:'黄少天',
    age:'20',
    job:'荣耀剑客'

};
var {name,age,job}=person;//已被赋予对应的值；
```
+ 如果对象有嵌套，一样保持一致即可

```
var person={
    name:'黄少天',
    age:'20',
    job:'荣耀剑客'
    other:{
        team:'蓝雨'
        constellation:'狮子座'
        teamJob:'副队'


    }
};
var {name,other:{team,teamJob}}=person;
console.log(name)
console.log(team)
console.log(teamJob)
//other不是变量
```
+ 使用解构赋值对对象属性进行赋值时，如果对应的属性不存在，变量将被赋值为undefined，
+ 用的变量名和属性名不一致，可以用下面的语法获取：
```
var person={
    name:'黄少天',
    age:'20',
    job:'荣耀剑客'
    other:{
        team:'蓝雨'
        constellation:'狮子座'
        teamJob:'副队'


    }
};
// 把job属性赋值给变量id:
let {name, job:id} = person;
name; // '黄少天'
id; // '荣耀剑客'
```



+ 解构赋值可以使用默认值，可避免不存在的属性返回undefined的问题：
```
var person={
    name:'黄少天',
    age:'20',
    job:'荣耀剑客'
    other:{
        team:'蓝雨'
        constellation:'狮子座'
        teamJob:'副队'


    }
};
// 如果person对象没有single属性，默认赋值为true:
var {name, single=true} = person;
name; // '黄少天'
single; // true
```

