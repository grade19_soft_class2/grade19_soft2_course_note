# 课堂笔记_25 🤨
## jQuery
### 选择器 按ID查找 按tag查找 按class查找 按属性查找 组合查找
```
var f=$('#first');  // 按ID查找id为first的
var a=$('[class="one two"]')         //  按属性查找class  class="one two"
var b= $('.one')                     //  按class查找出one 
var email=$('input[name=email]')     //  按属性查找

console.log(b);
console.log(email);
console.log(a);
console.log(f);

var i=$('[name^=icon]')               //找出前缀为 'icon' 的
console.log(i);
var item=$('[name$=item]')            //找出后缀为 'item' 的
console.log(item);

var ott=$('[class="one two three"]')
console.log(ott);
var fo = $('[class^=f]')
console.log(fo);

var email=$('input[name=email]')        //组合查找 input 里的 'name=email'
console.log(email);

console.log($('div,input'));           //找出div  和 input
```
![01](./Picture/2021-01-01-01.png)
## 层级选择器
### Descendant Selector
```
//找出在class为testing的div 里 class为lang-python的li
var divli=$('div.testing li.lang-python')     
console.log(divli);

//找出在class为lang的ul 里 class为li.lang-lua的li
var ulli=$('ul.lang li.lang-lua')
console.log(ulli);

//父子关系  限定了层级关系必须是父子关系，就是`<child>`节点必须是`<parent>`节点的直属子节点
var chulli=$('ul.lang>li.lang-javascript')
console.log(chulli);
```
![03](./Picture/2021-01-01-03.png)
![02](./Picture/2021-01-01-02.png)


### 过滤器（Filter）
过滤器一般不单独使用，它通常附加在选择器上，帮助我们更精确地定位元素。观察过滤器的效果：
```
$('ul.lang li'); // 选出JavaScript、Python和Lua 3个节点

$('ul.lang li:first-child'); // 仅选出JavaScript
$('ul.lang li:last-child'); // 仅选出Lua
$('ul.lang li:nth-child(2)'); // 选出第N个元素，N从1开始
$('ul.lang li:nth-child(even)'); // 选出序号为偶数的元素
$('ul.lang li:nth-child(odd)'); // 选出序号为奇数的元素
```
## 查找 过滤
### find()查找 parent()方法  
### 同一层级的节点，next()和prev()方法
```
var ullag=$('ul.lang')
console.log(ullag);

//通过find()查找
console.log(ullag.find('.dy'));
console.log(ullag.find('.js'));
console.log(ullag.find('#swift'));
console.log(ullag.find('[name=haskell]'));

var sw=$('ul.lang #swift')
console.log(sw);

// 找到上层节点 父节点
console.log(sw.parent());

//同一层级的
console.log(sw.next());
console.log(sw.next('[name=haskell]'));
console.log(sw.prev());
```
![05](./Picture/2021-01-01-05.png)
![04](./Picture/2021-01-01-04.png)

### 过滤filter()  map()
```
var lang=$('ul.lang li')

console.log(lang.filter('.js'));

var funlang=lang.filter(function (){
    return this.innerHTML.indexOf('H') === 0
})
console.log(funlang);
```
![06](./Picture/2021-01-01-06.png)
```
var malang=lang.map(function(){
    return this.innerHTML;
}).get()
console.log(malang);
```
![07](./Picture/2021-01-01-07.png)

## 操作DOM
### 修改Text和HTML css()
```
var ja=$('ul.lang li[class="js dy"]')
var ha=$('ul.lang li[name=haskell]')


console.log(ja.html());
console.log(ja.text());

ja.html('<span style="color: red">JavaScript</span>')
ha.text('JavaScript & ECMAScript')


lang.css('background-color', '#ffd351').css('color', 'red')

```
![08](./Picture/2021-01-01-08.png)
![09](./Picture/2021-01-01-09.png)
```
var div = $('#test-div');
div.css('color'); // '#000033', 获取CSS属性
div.css('color', '#336699'); // 设置CSS属性
div.css('color', ''); // 清除CSS属性
```
### css()方法将作用于DOM节点的style属性，具有最高优先级。如果要修改class属性，可以用jQuery提供的下列方法：
```
var div = $('#test-div');
div.hasClass('highlight'); // false， class是否包含highlight
div.addClass('highlight'); // 添加highlight这个class
div.removeClass('highlight'); // 删除highlight这个class
```
### 添加DOM
```
var lang =$('ul.lang li')
var  ul=$('ul.lang')
ul.append('<li>添加</li>')

// 创建一个节点添加至ul
var add = document.createElement('li')
add.innerHTML='<span>ADD</span>'
ul.append(add)


// 添加函数对象:
ul.append(function (index, html) {
    return '<li><span>Language - ' + index + '</span></li>';
});
```
![10](./Picture/2021-01-01-10.png)
### 新节点插入到指定位置
```
var js = $('#test-div>ul>li:first-child');
js.after('<li><span>Lua</span></li>');
```
### 删除节点
要删除DOM节点，拿到jQuery对象后直接调用remove()方法就可以了。如果jQuery对象包含若干DOM节点，实际上可以一次删除多个DOM节点：
```
var li = $('#test-div>ul>li');
li.remove(); // 所有<li>全被删除
```
## 事件
### 鼠标单击时触发
```
var a= $('#test-link')

//用on() 绑定 事件'click'
a.on('click',function(){
    console.log('ShowTime');
})

//一般直接用click()
a.click(function(){
    console.log('单机后:ShowTime');
})


// setTimeout(()=>{
//     console.log('结束啦');

// 结束事件：
//     a.off()
// },2000)

var x=$('[lang=en]')

x.mousemove(()=>{
    console.log('Show');
})

```
![11](./Picture/2021-01-01-11.png)
### 鼠标事件
+ click: 鼠标单击时触发；
+ dblclick：鼠标双击时触发；
+ mouseenter：鼠标进入时触发；
+ mouseleave：鼠标移出时触发；
+ mousemove：鼠标在DOM内部移动时触发；
+ hover：鼠标进入和退出时触发两个函数，相当于mouseenter加上mouseleave。
### 键盘事件
键盘事件仅作用在当前焦点的DOM上，通常是`<input>`和`<textarea>`。

+ keydown：键盘按下时触发；
+ keyup：键盘松开时触发；
+ keypress：按一次键后触发。
其他事件
+ focus：当DOM获得焦点时触发；
+ blur：当DOM失去焦点时触发；
+ change：当`<input>`、`<select>`或`<textarea>`的内容改变时触发；
+ submit：当`<form>`提交时触发；
+ ready：当页面被载入并且DOM树完成初始化后触发。
其中，ready仅作用于document对象

由于ready事件使用非常普遍，所以可以这样简化：
```
$(document).ready(function () {
    // on('submit', function)也可以简化:
    $('#testForm).submit(function () {
        alert('submit!');
    });
});
```
### 事件触发条件：
一个需要注意的问题是，事件的触发总是由用户操作引发的。例如，我们监控文本框的内容改动：
```
var input = $('#test-input');
input.change(function () {
    console.log('changed...');
});
```
当用户在文本框中输入时，就会触发change事件。但是，如果用JavaScript代码去改动文本框的值，将不会触发change事件：
### 事件参数
有些事件，如mousemove和keypress，我们需要获取鼠标位置和按键的值，否则监听这些事件就没什么意义了。所有事件都会传入Event对象作为参数，可以从Event对象上获取到更多的信息：
```
$(function () {
    $('#testMouseMoveDiv').mousemove(function (e) {
        $('#testMouseMoveSpan').text('pageX = ' + e.pageX + ', pageY = ' + e.pageY);
    });
});
```