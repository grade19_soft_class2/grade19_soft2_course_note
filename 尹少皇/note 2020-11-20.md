# 课堂笔记_03
## 递归，打印斐波那契数列

### Fibonacci
```
        public static int Fibonacci(int i)
        {
            if (i == 1 || i == 2)
            {
                return i = 1;
            }
            else
            {
                return Fibonacci(i - 1) + Fibonacci(i - 2);
            }

        }
```
### 打印
```
   Console.WriteLine("请输入所需查找的斐波那契数：");
            var a = Console.ReadLine();
            var n = int.Parse(a);
            Console.WriteLine("结果如下：");
            for (int i=1;i<=n;i++)
            {
                Console.WriteLine(FibonacciClass.Fibonacci(i));
            }
            Console.WriteLine();
```
## Math下，ceilling floor这两个方法的区别
+ Ceiling	返回大于或等于指定的双精度浮点数的最小整数值
+ Floor	返回小于或等于指定的双精度浮点数的最大整数值
```
            double x = 6.0;
            double y = 4.0;
            double z = x / y;
            Console.WriteLine(z);   //值：z = 1.5
            Console.WriteLine(Math.Ceiling(z)); //值：z = 2
            Console.WriteLine(Math.Floor(z));   //值：z = 1
```
## 关于日期，给出任意的日期值，计算当月第一天、当月最后一天是多少号，并且计算给出日期值是当月第几周
```
            DateTime dt = DateTime.Now;
            Console.WriteLine("请输入年：");
            var y = Console.ReadLine();
            var year = int.Parse(y);
            Console.WriteLine("请输入月：");
            var m = Console.ReadLine();
            var month = int.Parse(m);
            Console.WriteLine("请输入日：");
            var d = Console.ReadLine();
            var day = int.Parse(d);

            DateTime date = new DateTime(year, month, day);
            Console.WriteLine("当月第一天日期为：");
            Console.WriteLine(date.AddDays(-(day - 1)));
            Console.WriteLine("当月最后一天日期为：");
            Console.WriteLine(date.AddDays(-(day - 1)).AddMonths(1).AddDays(-1));

            double w = day / (double)7;
            var week = Math.Ceiling(w);
            Console.WriteLine("该日期为当月第" + week + "周");
```