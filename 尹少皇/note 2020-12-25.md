# 课堂笔记_22 🤨
## 今日笔记
### document document对象就是整个DOM树的根节点
```
document的title属性是从HTML文档中的<title>xxx</title>读取的，但是可以动态改变 如：
document.title = '努力学习JavaScript!'

console.log(document.title);    //原有的title      
document.title='我只是个路过的假面骑士'
console.log(document.title);    //修改后的
```
![01](./Picture/2020-12-27-01.png)

### getElementById()和getElementsByTagName()
```

let a=document.getElementById('as1')        //id为as1
let b=document.getElementsByTagName('div')  //Tag名称div的

console.log(a.innerText);   
console.log(b);
```
![02](./Picture/2020-12-27-02.png)
![03](./Picture/2020-12-27-03.png)

### 更新DOM
```
var a =document.getElementById('as3')
// 设置文本
var geta=a.innerHTML='剑谱更改：第四页'
//设置HTML 更改其内部结构
a.innerHTML='剑谱更改:<dl style="color:red" id="as6"> 剑谱第一页 </dl>'

console.log(a);
```
![04](./Picture/2020-12-27-04.png)
### 插入DOM
```
<!-- HTML结构 -->
<p id="js">JavaScript</p>
<div id="list">
    <p id="java">Java</p>
    <p id="python">Python</p>
    <p id="scheme">Scheme</p>
</div>

var js = document.getElementById('js');
var list = document.getElementById('list');

//将 JavaScript （<p id="js">JavaScript</p>） 添加到<div id="list"> 里的最后一行：
list.appendChild(js);
```
![05](./Picture/2020-12-27-05.png)

```
var list = document.getElementById('list');

//添加一个节点p:
var add = document.createElement('p');

//设置id为'add':
add.id = 'add';

//设置文本：
add.innerText = '添加';

//在 ‘list’里添加
list.appendChild(add);

```
![06](./Picture/2020-12-27-06.png)

### 创建一个style节点 把它添加到<head>节点的末尾动态地给文档添加了新的CSS定义：
```
var d = document.createElement('style');

d.setAttribute('type', 'text/css');
d.innerHTML = 'p { color: red }';

document.getElementsByTagName('head')[0].appendChild(d);
```
```
var list = document.getElementById('list'); //list
var ref = document.getElementById('python');//python

//添加一个节点p:
var haskell = document.createElement('p');  

//设置id为haskell：
haskell.id = 'haskell';

//设置文本Haskell：
haskell.innerText = 'Haskell';

//在python的前面添加
list.insertBefore(haskell, ref);
```
![07](./Picture/2020-12-27-07.png)

### 删除DOM
```
//找到父节点
var parent = document.getElementById('list')

//删除  removeChild
parent.removeChild(parent.children[5]);

//例如：
let py =document.querySelector('#java')

let par=py.parentElement;

par.removeChild(py)
```
### querySelector()和querySelectorAll()
```
// 通过querySelector获取ID为q1的节点：
var q1 = document.querySelector('#q1');

// 通过querySelectorAll获取q1节点内的符合条件的所有节点：
var ps = q1.querySelectorAll('div.highlighted > p');
```