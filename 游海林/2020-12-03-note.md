# 课堂内容


## for...in
1. for...in是for循环的一个变体，它可以把对象的所有属性都遍历出来。
例如：
```
var a={

name:'小爱',
age:18,
sex:'女'

}
for(var item in a){
console.log(item);//name,age,sex
}
```

2. 要过滤掉对象继承的属性，用hasOwnProperty()来实现.
例如：
```
var o = {
    name: 'Jack',
    age: 20,
    city: 'Beijing'
};
for (var key in o) {
    if (o.hasOwnProperty(key)) {
        console.log(key); // 'name', 'age', 'city'
    }
}

```
3. for...in对数组的循环得到的是string不是number.

## Map和Set
### Map
1. Map是一组键值对的结构，具有极快的查找速度。
2. Map有两种初始化的方式。
+ 第一种
```
var m = new Map([['Michael', 95], ['Bob', 75], ['Tracy', 85]]);
```
+ 第二种
```
var m = new Map(); // 空Map
```
3. Map通过set来添加键值
例如：
```
m.set('小爱',99)
```
4. 由于一个key只能对应一个value，所以，多次对一个key放入value，后面的值会把前面的值冲掉。

### Set
1. Set和Map类似，但Set不能储存值，因为key不能重复，所以没有重复的key。
2. 重复的在Set中会自动被过滤掉。
3. 可以往Set中添加相同的元素，但是不会有效果。
4. 用add(key)往Set中添加元素。


## for...of
1. for...of循环用来遍历集合。
![server](./imgs/2020-12-3-15-30-31.JPG)
2. for...of和for...in的区别
+ for..of适用于集合，for...in适用于对象。for...of会遍历自身的属性，for...in遍历属性的名称。


## forEach
1. 遍历数组时，用forEach更合适。
2. Set与Array类似，但Set没有索引，因此回调函数的前两个参数都是元素本身：
```
var s = new Set(['A', 'B', 'C']);
s.forEach(function (element, sameElement, set) {
    console.log(element);
});
```

3. Map的回调函数参数依次为value、key和map本身。

![server](./imgs/2020-12-3-15-38-24.JPG)
