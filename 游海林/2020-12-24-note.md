# 课堂内容
## 原型继承
### js原型继承的几种方式

 #### 方式 1：原型链继承（不推荐）
 ```
function Programmer() {}

Programmer.prototype = new Person ()
Programmer.prototype.code = function () {
  console.log('coding')
}

let jon = new Programmer()
jon.code() // coding
jon.sleep() // sleeping

jon instanceof Person // true
jon instanceof Programmer // true

Object.getPrototypeOf(jon) // Person {age: 18, code: ƒ}
jon.__proto__ // Person {age: 18, code: ƒ}
``` 
缺点：
+ 无法向父类构造函数传参
+ 父类的所有属性被共享，只要一个实例修改了属性，其他所有的子类实例都会被影响

#### 方式 2：借用构造函数（经典继承）（不推荐）
复制父类构造函数内的属性
```
function Programmer(name) {
  Person.call(this)
  this.name = name
}
let jon = new Programmer('jon')
jon.name // jon
jon.age // 18

jon.sleep() // Uncaught TypeError: jon.sleep is not a function
jon instanceof Person // false
jon instanceof Programmer // true
```
优点：
+ 可以为父类传参
+ 避免了共享属性
缺点：
+ 只是子类的实例，不是父类的实例
+ 方法都在构造函数中定义，每次创建实例都会创建一遍方法

#### 方式 3：组合继承（推荐）
组合 原型链继承 和 借用构造函数继承 。
```
function Programmer(age, name) {
  Person.call(this, age)
  this.name = name
}

Programmer.prototype = new Person()
Programmer.prototype.constructor = Programmer // 修复构造函数指向

let jon = new Programmer(18, 'jon')
jon.age // 18
jon.name // jon

let flash = new Programmer(22, 'flash')
flash.age // 22
flash.name // flash

jon.age // 18

jon instanceof Person // true
jon instanceof Programmer // true
flash instanceof Person // true
flash instanceof Programmer // true
```
+ 优点：融合原型链继承和构造函数的优点，是 JavaScript 中最常用的继承模式
+ 缺点：调用了两次父类构造函数
#### 原型式继承（不推荐）
```function create(o) {
  function F() {}
  F.prototype = o
  return new F()
}

let obj = {
  gift: ['a', 'b']
}

let jon = create(obj)
let xiaoming = create(obj)

jon.gift.push('c')
xiaoming.gift // ['a', 'b', 'c']
```
缺点：共享了属性和方法
#### 方式 5：寄生式继承（不推荐）
创建一个仅用于封装继承过程的函数，该函数在内部以某种形式来做增强对象，最后返回对象
```
function createObj (o) {
  var clone = Object.create(o)
  clone.sayName = function () {
    console.log('hi')
  }
  return clone
}
```
缺点：跟借用构造函数模式一样，每次创建对象都会创建一遍方法
#### 方式 6：寄生组合继承（最佳）
子类构造函数复制父类的自身属性和方法，子类原型只接受父类的原型属性和方法：
```
function create(prototype) {
  function Super() {}
  Super.prototype = prototype
  return new Super()
}

function Programmer(age, name) {
  Person.call(this, age)
  this.name = name
}

Programmer.prototype = create(Person.prototype)
Programmer.prototype.constructor = Programmer // 修复构造函数指向

let jon = new Programmer(18, 'jon')
jon.name // jon
```
进阶封装：
```
function create(prototype) {
  function Super() {}
  Super.prototype = prototype
  return new Super()
}

function prototype(child, parent) {
  let prototype = create(parent.prototype)
  prototype.constructor = child // 修复构造函数指向
  child.prototype = prototype
}

function Person (age) {
  this.age = age || 18
}
Person.prototype.sleep = function () {
  console.log('sleeping')
}

function Programmer(age, name) {
  Person.call(this, age)
  this.name = name
}

prototype(Programmer, Person)

let jon = new Programmer(18, 'jon')
jon.name // jon
```
引用《JavaScript 高级程序设计》中对寄生组合式继承的夸赞就是：
这种方式的高效率体现它只调用了一次 Parent 构造函数，并且因此避免了在 Parent.prototype 上面创建不必要的、多余的属性。与此同时，原型链还能保持不变；因此，还能够正常使用 instanceof 和 isPrototypeOf。开发人员普遍认为寄生组合式继承是引用类型最理想的继承范式。

#### 方式 7：ES6 extends（最佳）
```
// 父类
class Person {
  constructor(age) {
    this.age = age
  }
  sleep () {
    console.log('sleeping')
  }
}

// 子类
class Programmer extends Person {
  constructor(age, name) {
    super(age)
    this.name = name
  }
  code () {
    console.log('coding')
  }
}

let jon = new Programmer(18, 'jon')
jon.name // jon
jon.age // 18

let flash = new Programmer(22, 'flash')
flash.age // 22
flash.name // flash

jon instanceof Person // true
jon instanceof Programmer // true
flash instanceof Person // true
flash instanceof Programmer // true
```
+ 优点：不用手动设置原型。
+ 缺点：新语法，只要部分浏览器支持，需要转为 ES5 代码。

## class继承
### 没有class继承之前的回忆
1. 定义 function father 构造函数，再通过 prototype 定义 father 类原型方法。

2. 定义 function child 构造函数，构造函数内部通过 father.call( this, arguments ) 的方式获得父类的实例属性。

3. 通过 child.prototype = new father() 的方式获取 father 的原型方法。

4. 最后还必须 child.prototype.constructor = child 的方式进行子类构造函数的还原。
先来看一个简单的例子：
```
class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}
class ColorPoint extends Point {
  constructor(x, y, color) {
    super(x, y);
    this.color = color;
  }
  toString() {
    return this.color + ' ' + super.toString();
  }
}
let ins = new ColorPoint(1,2,'red');
console.log( ins.toString() ); /* red [object Object] */
```
class 继承一个比较关键的引用 super 在上述例子中出现了两次。分别是在构造函数中和类方法中。需要注意的是 super 只能在子类中使用。super 在构造函数中的作用和在子类方法中的作用并不同。

1. 在构造函数中，super 是一个函数，它调用父类的构造函数，并隐式的返回一个 this。如果在 super(x,y) 前面执行 this.color 程序会报错。原因前一句已经说了，super 会隐式的返回一个 this，而子类的构造函数的初始化全部都是基于这个 this 的。这是 class 中的一种机制，关于构造函数的这部分内容无须记得很牢，因为在开发过程中，如果子类的构造函数不使用 super(...)，或在错误的地方调用它，子类都会报错，所以无论如何，你都会记得很牢（否则程序无法开发下去）。

2. 而在子类的方法中，super 作为一个引用指向父类的方法。这个特性在 function 中也可以实现，虽然需要写一些不那么好阅读和维护的代码，比如你可以先保存 father.toString 的引用（当然需要注意 this 的指向问题，可以采用 bind 或 call 等方式），然后再写 child.toString = function(){...},在这里面调用父类的方法。也许有更优雅的写法。但是能有 class 的写法更加优雅简洁易读和易维护吗，显然不能，那我们就必须往前看了。在成为资深前端之前，可以忘掉 function。

3. 在使用 super 调用父类的方法时，super 内部的 this 指向子类。逆向思维来想想，这个 this 也只能指向子类的实例，调用者就是它。除非用 bind call apply 这些东西。


ins 同时是 Point 和 ColorPoint 的实例，这个读者自行验证。这也符合 ES5 的规范。

上面的例子中，ins 的属性 x 是在原型上，还是在实例上？这里从 super 的理解上着手立马就有答案，x 属性是实例的，不是原型的。

这个例子讲解了 class 类的公有属性和公有方法的相关继承问题。公有属性全部都在实例上，而公有方法全部都在原型上。这个就不详细展开了。

下面是 class 的静态方法相关处理方式。
```
class A { static geta(){ console.log( 'A' ); } }
class B extends A { static getb(){ console.log( 'B' ); } }
B.geta(); /* A */
B.getb(); /* B */
下面来看看 class 中的私有属性在继承中的相关反应。先看一个例子：

var A = ( function(){
  var _name;
  class A {
    constructor() {}
	getName(){ console.log( _name ); }
	setName( name ){ _name = name; return this; }
  }
  return A;
} )();
class B extends A { }
let ins = new B();
ins.setName('nDos').getName(); // nDos
```
上例中 B 继承于 A，B 的实例 ins 可以通过 A 的公有方法使用 A 的私有变量 _name。

小tips：class 中可以让指定的类无法被实例化，通过判断 new.target === theClass（定义的类名）的真假抛出错误，使得 theClass 不能被实例化而只能被继承，这是一个很有用的 tips。不用担心继承的问题，子类实例化的时候，父类的构造函数中 new.target 指向的是子类。参见下例：
```
class A {
  constructor() {
    new.target === A || console.log('A 为抽象类，不能被实例化')
  }
}
class B extends A { }
let ins = new B(); /* A 为抽象类，不能被实例化 */
```
class 的继承还可以从原生构造函数中继承，我的另外一篇博文有提过。关于这方面的内容需要单独行文探讨，此处暂时不讨论，仅仅提一下。

class Mixin 混合继承。这个是为了解决多重继承的问题而来。什么是多重继承，多重继承是编程语言中的概念，多重继承指的是一个类可以继承另外一个类，而另外一个类又可以继承别的类，比如A类继承B类，而A类又可以继承C类，这就是多重继承。这里将要将的就是这个。但是 Mixin 更加的灵活，使得代码结构也更加清晰优雅。
```
var A = ( function(){
  var _name;
  class A {
    constructor() {}
	getName(){ console.log( _name ); }
	setName( name ){ _name = name; return this; }
  }
  return A;
} )();
function Mixin ( BaseClass ) {
	return class extends BaseClass
	{
		mixin(){ console.log('这是混合继承的类的方法'); }
	}
}
class C extends Mixin(A)
{
	getC(){ console.log('c'); }
}
let ins = new C();
ins.mixin(); // 这是混合继承的类的方法
ins.setName('nDos').getName(); // nDos
ins.getC(); // c
```
## 浏览器对象

### Window 对象
+ 所有浏览器都支持 window 对象。它表示浏览器窗口。
+ 所有 JavaScript 全局对象、函数以及变量均自动成为 window 对象的成员。

+ 全局变量是 window 对象的属性。

+ 全局函数是 window 对象的方法。

+ 甚至 HTML DOM 的 document 也是 window 对象的属性之一：
```
window.document.getElementById("header");
```
与此相同：
```
document.getElementById("header");
```
#### Window 尺寸
有三种方法能够确定浏览器窗口的尺寸（浏览器的视口，不包括工具栏和滚动条）。
对于Internet Explorer、Chrome、Firefox、Opera 以及 Safari：
+ window.innerHeight - 浏览器窗口的内部高度
+ window.innerWidth - 浏览器窗口的内部宽度
对于 Internet Explorer 8、7、6、5：
```
document.documentElement.clientHeight
document.documentElement.clientWidth
```
或者
```
document.body.clientHeight
document.body.clientWidth
```
实用的 JavaScript 方案（涵盖所有浏览器）：

实例
```
var w=window.innerWidth
|| document.documentElement.clientWidth
|| document.body.clientWidth;

var h=window.innerHeight
|| document.documentElement.clientHeight
|| document.body.clientHeight;
```

该例显示浏览器窗口的高度和宽度：（不包括工具栏/滚动条）
#### 其他 Window 方法
一些其他方法：
```
window.open() - 打开新窗口
window.close() - 关闭当前窗口
window.moveTo() - 移动当前窗口
window.resizeTo() - 调整当前窗口的尺寸
```
### Window Screen
window.screen 对象在编写时可以不使用 window 这个前缀。

一些属性：
```
screen.availWidth - 可用的屏幕宽度
screen.availHeight - 可用的屏幕高度
Window Screen 可用宽度
screen.availWidth 属性返回访问者屏幕的宽度，以像素计，减去界面特性，比如窗口任务栏。
```
实例
返回您的屏幕的可用宽度：
```
<script>

document.write("可用宽度：" + screen.availWidth);

</script>
```
以上代码输出为：
可用宽度：1920

1. Window Screen 可用高度
screen.availHeight 属性返回访问者屏幕的高度，以像素计，减去界面特性，比如窗口任务栏。

实例
返回您的屏幕的可用高度：
```
<script>

document.write("可用高度：" + screen.availHeight);

</script>
```
以上代码输出为：
可用高度：1050
#### Window Location
window.location 对象在编写时可不使用 window 这个前缀。

一些例子：
```
location.hostname 返回 web 主机的域名
location.pathname 返回当前页面的路径和文件名
location.port 返回 web 主机的端口 （80 或 443）
location.protocol 返回所使用的 web 协议（http:// 或 https://）
Window Location Href
location.href 属性返回当前页面的 URL。
```
实例
返回（当前页面的）整个 URL：
```
<script>

document.write(location.href);

</script>
```
以上代码输出为：

http://caibaojian.com/w3c/js/js_window_location.html
##### Window Location Pathname
location.pathname 属性返回 URL 的路径名。

实例
返回当前 URL 的路径名：
```
<script>

document.write(location.pathname);

</script>
以上代码输出为：

/w3c/js/js_window_location.html
Window Location Assign
location.assign() 方法加载新的文档。
```
实例
加载一个新的文档：
```
<html>
<head>
<script>
function newDoc()
  {
  window.location.assign("http://www.w3school.com.cn")
  }
</script>
</head>
<body>

<input type="button" value="加载新文档" onclick="newDoc()">

</body>
</html>
```
### navigator
navigator对象表示浏览器的信息，最常用的属性包括：
```
navigator.appName：浏览器名称；
navigator.appVersion：浏览器版本；
navigator.language：浏览器设置的语言；
navigator.platform：操作系统类型；
navigator.userAgent：浏览器设定的User-Agent字符串。
'use strict';
console.log('appName = ' + navigator.appName);
console.log('appVersion = ' + navigator.appVersion);
console.log('language = ' + navigator.language);
console.log('platform = ' + navigator.platform);
console.log('userAgent = ' + navigator.userAgent);
```