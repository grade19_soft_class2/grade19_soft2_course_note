# 课堂内容
## unshift和shift
1. 如果要往Array的头部添加若干元素，使用unshift()方法，shift()方法则把Array的第一个元素删掉
2. 空数组继续shift不会报错，而是返回undefined
```
arr; // ['B', 1, 2]
arr.shift(); arr.shift(); arr.shift(); // 连续shift 3次
arr; // []
arr.shift();
```


## short

1. sort()可以对当前Array进行排序，它会直接修改当前Array的元素位置，直接调用时，按照默认顺序排序。
2. short()也可以按自己指定的顺序排序
![server](./imgs/2020-11-27-15-14-15.JPG)



## reverse
1. 把整个数组调换顺序。
```
var arr = [1, 2,3];
arr.reverse(); 
arr; // [3,2,1]
```

## splice 

1. splice()方法是修改Array的“万能方法”，它可以从指定的索引开始删除若干元素，然后再从该位置添加若干元素.

## concat

1. concat()方法把当前的Array和另一个Array连接起来，并返回一个新的Array
例如：
```
var arr = ['A', 'B', 'C'];
var added = arr.concat([1, 2, 3]);
added; // ['A', 'B', 'C', 1, 2, 3]
arr; // ['A', 'B', 'C']
```

2. concat()方法并没有修改当前Array，而是返回了一个新的Array。
3. concat()方法可以接收任意个元素和Array，并且自动把Array拆开，然后全部添加到新的Array里。

## join

1. join()方法是一个非常实用的方法，它把当前Array的每个元素都用指定的字符串连接起来，然后返回连接后的字符串。
```
var arr = ['A', 'B', 'C', 1, 2, 3];
arr.join('-'); // 'A-B-C-1-2-3'
```

## 多维数组
1. 如果数组的某个元素又是一个Array，则可以形成多维数组。
2. 可以通过索引查询多维数组中的数组的元素。
```
var arr = [[1, 2, 3], [400, 500, 600], '-'];
arr[1][1]//500
```





