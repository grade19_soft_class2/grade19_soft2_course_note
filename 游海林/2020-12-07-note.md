# 课堂内容

## 全局作用域
1. 不在任何函数内定义的变量就具有全局作用域。
2. JavaScript默认有一个全局对象window，全局作用域的变量实际上被绑定到window的一个属性。
3. 
+  直接编写在 script 标签之中的JS代码，都是全局作用域；
+  或者是一个单独的 JS 文件中的。
+  全局作用域在页面打开时创建，页面关闭时销毁；
+  在全局作用域中有一个全局对象 window（代表的是一个浏览器的窗口，由浏览器创建），可以直接使用。
4. 在全局作用域中，所有创建的变量都会作为 window 对象的属性保存。
```
var a=10
console.log(a);
10
undefined
```
5. 所有创建的函数都会作为 window 对象的方法保存。

## 局部作用域（函数作用域）：

+ 　在函数内部就是局部作用域，这个代码的名字只在函数的内部起作用

+ 　调用函数时创建函数作用域，函数执行完毕之后，函数作用域销毁；

+ 　每调用一次函数就会创建一个新的函数作用域，它们之间是相互独立的。

实例分析：

在这个例子里面 un函数里面的 局部作用域中 有一个 num 变量，script 标签的全局作用域中也有一个 num变量。

（一个在全局作用域下，另一个在局部作用域下，虽然两个变量的变量名相冲突，但是并没有影响。）

所以，在不同的作用域下，变量名相同也不受影响，这样就很有效的减少了命名冲突。

```
<script>
    var num = 10;
    function nu(){
        var num = 20;
        console.log(num);
    }
    nu();
    console.log(num);
</script>
```

## 作用域链
只要是代码，就有一个作用域，写在函数内部的就叫做局部作用域；

如果函数中还有函数，那么在这个作用域中又可以诞生一个作用域；

当在函数作用域中操作一个变量的时候，会先在自身作用域中查找，如果有就直接使用，如果没有就向上级作用域中寻找。如果全局作用域中也没有，那么就报错。

根据内部函数可以访问可以访问外部函数变量的这种机制，用链式查找决定哪些数据能被内部函数访问，就称为函数作用域链。

```
var a = 1;
    function fn1(){
        var a=2;
        var b='22';
        fn2();
        function fn2(){
            var a =3;
            fn3();
            function fn3(){
                var a=4;
                console.log('a= ' + a);  //求 a的值
                console.log('b= ' + b);  //求 b的值
            }
        }
    }
    fn1();//a=4  b=22
```

## 作用域相关例子
```
// 全局作用域
var globalNumber = 1;

// 挂载在window上的变量或函数 -> 全局作用域
function InternalScope() {
    // 局部作用域
    // var internalNumber = 2;
    window.isTrue = 1;
}

// 先调用函数,释放出来.
InternalScope();
console.log('全局作用域：', isTrue,'全局作用域：',globalNumber);



// 局部作用域
function InternalScopes() {
    var internalNumber = 2;
    console.log('局部作用域：', internalNumber);
}

InternalScopes();


// 当全局作用域与浏览器自带产生冲突 -> 弹窗(alert())
function alert(){
    console.log('alert 失效了！ ha');
}

alert();
```

## 常量
1. js中一旦被定义就无法再被修改的变量，称之为常量
+ 常量:不能修改的值
+ 常量的关键字是const
+ 常量只能读取，不能修改
+ 常量通常用大写字母定义，多个单词之间用下划线_分隔

```
//重力加速度
	var G = 9.8; 
		G = 12; //可以修改值
	console.log(G);
		
	
	//圆周率
	//改成常量
	const PI = 3.14; //不可以修改值
	//修改常量会报错
// 		  PI = 4.66; //报错invalid assignment to const `PI'
	console.log(PI);
	
	//常量不支持声明前置
// 	console.log(MY_PI); //会报错
// 	const MY_PI = 3.14;
 
	//常量是一个块作用域
	if (true) {
		const COLOR = 'red';
	}
// 	console.log(COLOR); //报错COLOR is not defined
 
 
	//变量不是一个块作用域
	if (true) {
		var my_home = '我家在中国';
	}
	console.log(my_home); //结果为我家在中国

```





