# JavaScript的第九次课开始食用🥞
## 解构赋值
和传统的方法不同，解构赋值可以直接对多个变量同时赋值
```
'use strict';
var [x, y, z] = ['hello', 'JavaScript', 'ES6'];
// x, y, z分别被赋值为数组对应元素:
console.log(x,y,z);
```
+ 注意，对数组元素进行解构赋值时，多个变量要用[...]括起来，如果数组本身还有嵌套，也可以通过解构赋值，嵌套层次和位置要保持一致
```
let [x, [y, z]] = ['hello', ['JavaScript', 'ES6']];
console.log(x);     // 'hello'
console.log(y);     // 'JavaScript'
console.log(z);     // 'ES6'
```
解构赋值还可以忽略某些元素
```
let [, , z] = ['hello', 'JavaScript', 'ES6']; // 忽略前两个元素，只对z赋值第三个元素
console.log(z); // 'ES6'
```
从一个变量中获取指定属性也可以使用解构赋值
```
'use strict'
var asb={
    nickname:'小明',
    age:18,
    school:'墨尔本',
};
var {nickname,age,school}=asb;
console.log(nickname,age,school);
```
同样也可以对嵌套的对象进行赋值
```
'use strict'
var asb={
    nickname:'小明',
    age:18,
    school:'墨尔本理工学院',
    add:{
        city:'澳大利亚',
        zip:'墨尔本'
    }
};
var {nickname,age,school,add:{city,zip}}=asb;
console.log(nickname,age,school,city,zip);
```
可以将asb里的属性age赋值给一个变量id
```
let{nickname,age:id}=asb
```
