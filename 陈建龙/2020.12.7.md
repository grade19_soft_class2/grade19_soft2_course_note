### 变量作用域
+ 在JavaScript中，用var申明的变量实际上是有作用域的。

+ 如果一个变量在函数体内部申明，则该变量的作用域为整个函数体，在函数体外不可引用该变量：
```
'use strict';

function foo() {
    var x = 1;
    x = x + 1;
}

x = x + 2; // ReferenceError! 无法在函数体外引用变量x
```
+ 如果两个不同的函数各自申明了同一个变量，那么该变量只在各自的函数体内起作用。换句话说，不同函数内部的同名变量互相独立
+ 由于JavaScript的函数可以嵌套，此时，内部函数可以访问外部函数定义的变量，反过来则不行
+ 如果内部函数和外部函数的变量名重名,则从“内”向“外”查找；如果内部函数定义了与外部函数重名的变量，则内部函数的变量将“屏蔽”外部函数的变量
### 变量提升
+ JavaScript的函数定义有个特点，它会先扫描整个函数体的语句，把所有申明的变量“提升”到函数顶部，如果变量声明在输出后声明，那么输出前输出输出后的变量将输出undefined；
### 全局作用域
+ 不在任何函数内定义的变量就具有全局作用域。实际上，JavaScript默认有一个全局对象window，全局作用域的变量实际上被绑定到window的一个属性
```
function foo() {
    alert('foo');
}

foo(); // 直接调用foo()
window.foo(); // 通过window.foo()调用
```
+ 都为相同
+ alert()函数其实也是window的一个变量：
```
window.alert('调用window.alert()');
// 把alert保存到另一个变量:
var old_alert = window.alert;
// 给alert赋一个新函数:
window.alert = function () {}
alert('无法用alert()显示了!');

// 恢复alert:
window.alert = old_alert;
alert('又可以用alert()了!');
```
### 局部作用域
简单说明就是count和let都是块级作用域；