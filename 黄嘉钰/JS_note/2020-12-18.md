## 箭头函数

```
x => x*x;
相当于：
function (x){
    return x*x;
}
```
箭头函数相当于匿名函数，简化了函数的定义

两种格式
+ 只包含一个表达式，可以省略{...}和return
+ 可以包含多条语句，不可以省略{...}和return
```
//两个参数：
(x,y)=>....

//无参数：
()=>...

//可变参数：
(x,y,...rest)=>{
    ...
}
```

如果要返回一个对象
```
因为和函数体的{...}有语法冲突，所以正确语法是：
x => ({foo : x})
```

### this 
箭头函数和匿名函数实际上的区别：箭头函数内部的this是词法作用域，有上下文确定。

```
var obj = {
    birth: 1990,
    getAge: function () {
        var that = this;
        var fn = function () {
            return new Date().getFullYear() - that.birth;
        };
        return fn();
    }
};
console.log(obj.getAge());
```
相当于：
```
var obj = {
    birth: 1990,
    getAge: function () {
        var fn =  () => new Date().getFullYear() - this.birth;
        return fn();
    }
};
console.log(obj.getAge());
```

## 标准对象
在JS里一切都是对象。为了区分对象的类型，用typeof操作符获取对象的类型，返回字符串。

### 包装对象
