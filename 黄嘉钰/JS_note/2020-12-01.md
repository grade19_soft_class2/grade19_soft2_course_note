```
var arr = ['小明', '小红', '大军', '阿黄','练勇气','hhh','huanghu'];
arr.sort().join(',');
var str = '欢迎'+arr.slice(0,arr.length-1)+'和'+arr[arr.length-1]+'同学！';
console.log(str);
```

```
var arr = ['小明', '小红', '大军', '阿黄','连大胆'];
var str = arr.join(',');
//1.正则表达时，贪婪模式，.*会一直匹配到最后一个
var s = "欢迎"+str.replace(/(.*),/,'$1和')+"同学！";
console.log(s);
```