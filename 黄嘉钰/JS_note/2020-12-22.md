## 创建对象

JS对每个创建的对象都会设置一个原型，指向它的原型对象。

+ 原型

原型也是一个对象，通过原型可以实现对象的属性继承，JS的对象中都包含了一个"prototype"内部属性，这个属性对应的就是该对象的原型。

"prototype"作为对象内部属性，是不能直接被访问的。为了方便查看对象的原型，于是提供了__proto__这个非标准的访问器。

+ 原型链

创建一个Array对象：
```
var arr = [1,2,3];

console.log(arr.__proto__ === Array.prototype);// true
```
其原型链：
```
arr ----> Array.prototype ----> Object.prototype ----> null
```

![](../Picture/201622790102722.jpg)
1. 所有的对象都有__proto__属性，该属性对应该对象的原型.
2. 所有的函数对象都有prototype属性，该属性的值会被赋值给该函数创建的对3. 象的_proto_属性.
4. 所有的原型对象都有constructor属性，该属性对应创建所有指向该原型的实例的构造函数.
5. 函数对象和原型对象通过prototype和constructor属性进行相互关联.

### 构造函数
+ 在JS中，用new关键字来调用的函数，称之为构造函数。

+ 构造函数的使用规则：
1. 函数名首字母大写
2. 通过this来给对象添加属性和方法