## 对象

JavaScript的对象是一种无序的集合数据类型，它由若干键值对组成。

```
var hh = {
    name : "nihao",
    age : 10,

    ///属性名包含特殊字符，就必须用''括起来
    "high-school":"华罗庚中学",

    "class": '高一(2)班'
}

console.log(hh.name);//nihao
console.log(hh.age);//10
console.log(hh.class);//高一(2)班

///访问high-school属性无法使用.操作符，必须用['xxx']来访问
console.log(hh["high-school"]);//华罗庚中学


///JavaScript的对象是动态类型，可以任意给对象添加或删除属性

hh.book = '《东方快车上的谋杀案》';//给hh对象添加book属性
console.log(hh.book);//《东方快车上的谋杀案》

delete hh.age;//删除age属性
console.log(hh.age);//undefined

///in字符检测对象是否拥有某个属性
console.log('book' in hh);//true
console.log('9' in hh);//false

//[注意] in字符检测属性不一定是对象拥有的，可能是对象继承得到的
//要判断一个属性是否是xiaoming自身拥有的，而不是继承得到的，可以用hasOwnProperty()方法
```

## 条件判断

### JavaScript使用if () { ... } else { ... }来进行条件判断

```
var age = 20;
if(age<18){
    alert("我还是小孩");
}else if(18<age<50){
    alert("不是小孩了");
}else{
    alert("老了老了");
}
```
[注意] 永远都要写上{}

```
/*练习
小明身高1.75，体重80.5kg。请根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，并根据BMI指数：

var height = parseFloat(prompt('请输入身高(m):'));
var weight = parseFloat(prompt('请输入体重(kg):'));

var bmi =weight/(height*height);
console.log(bmi);

if(bmi < 18.5){
    console.log("过轻");
}else if(18.5 < bmi && bmi < 25){
    console.log("正常");
}else if(25 < bmi&& bmi  < 28){
    console.log("过重");
}else if(28 < bmi&& bmi  < 32){
    console.log("肥胖");
}else{
    console.log("严重肥胖");
}
```

## 循环

### for循环
```
var x = 0;
for (var i=1; i<=10000; i++) {
    x = x + i;
}
console.log(x);//50005000
```

### for...in 

for循环的一个变体是for ... in循环，它可以把一个对象的所有属性依次循环出来
```
var hh = {
    name : "nihao",
    age : 10,
    "high-school":"华罗庚中学",
    "class": '高一(2)班'
}
for(var arr in hh){
    console.log(arr);
}
```
![](../Picture/2020-11-30_111245.png)

要过滤掉对象继承的属性，用hasOwnProperty()来实现
```
var hh = {
    name : "nihao",
    age : 10,
    "high-school":"华罗庚中学",
    "class": '高一(2)班'
}
for(var arr in hh){
    if(hh.hasOwnProperty(arr)){
        console.log(arr);    
    }
}
```

[注意] for ... in对Array的循环得到的是String.

### while 

while循环只有一个判断条件，条件满足，就不断循环，条件不满足时则退出循环

```
var x = 0;
var n = 99;
while (n > 0) {
    x = x + n;
    n = n - 2;
}
console.log(x);//2500
```

### do...while

和while循环的唯一区别在于，不是在每次循环开始的时候判断条件，而是在每次循环完成的时候判断条件

```
var n = 0;
do {
    n = n + 1;
} while (n < 100);//100
```

[注意] 用do { ... } while()循环要小心，循环体会至少执行1次，而for和while循环则可能一次都不执行。

