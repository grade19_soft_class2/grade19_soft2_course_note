# 今天讲了解析赋值，今日天气22°，天气阴。

## 请永远不要放弃你真正想要的东西，人总会变，但唯一不变的是人的初心。

### 了解手写Json解析器的心得，还有可以试着练下Dapper使用。

#### 解析赋值也就是对赋值运符的扩展，主要对数组的解析和对象的结构等等。

```
var [a,b,c]=[1,2,3];
console.log(a,b,c);

```
+ 在进行解析的时候，还是要注意嵌套层次的位置要保持一致。

+ 当数组进行交换数值，此时就变得非常方便。

```
var a=2;
var b=3;
[a,b]=[b,a];

```
1. 在进行解析赋值时按照属性结构，与所在的顺序无关，也没有无法结构的赋值的。
```
var [a,b,c]=[b:1,a:2];
console.log(a,b);

```

```
let options={
    title:"mane",
    width:200,
    heigth:250
}
 let {width,title,heigth}=options;
 console.log(title);
 console.log(width);
 console.log(heigth);

```
```
var str={
    nickName:'阿黄',
    age:3,
    master:'大狗',
    address:{
        provinece:'长安',
        city:'东安',
        region:'附中'
    }

    var {nickName,master,age,address:{city,region}}=str;
    console.log(nickName);
    console.log(master);
    console.log(city);
    console.log(region);
}
```

+ 练习
```
function abs({name,age,sex='女',heigth=1.8}){
    console.log(name);
    console.log(age);
    console.log(sex);
    console.log(heigth);

    var strs=arguments[0];
    console.log(strs);
}

   abs('此乃非淑女也',22);
```