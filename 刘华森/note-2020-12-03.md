# 今天讲了循环和Map、Set、iterable，天气晴。

##  讲了路径的问题'.'需要注意。

## 循环 
1. for ...in循环 
```
var a={
    name:'李华',
    age:19,
    score:99
};
for(var str in a){
    console.log((str));
}

```
+ 如果需要过滤继续的属性的话，就需要使用hasOwnProperty();
```
var a={
    name:'李华',
    age:19,
    score:99
};
 for(var st in a){
   if(a.hasOwnProperty(Key)){
      console.log(key);
   }
 }
```
### while循环， 循环结构就是重复做一件事，其特点是在给定条件成立时，反复执行某程序段，直到条件不成立为止。

1. 先判断表达式，表达式成立执行循环语句。
2. 循环条件中使用的变量（也就是循环变量）需要经过初始化。
3. 循环体中，应有结束循环的条件，否则程序就会进入死循环。
4. 循环体中，写若干JS代码，也可以再加一套循环进来。

```
var i=1;
var sum=0;
while(i<=100){
   sum+=i;
   i++;
};
console.log(sum);

```

### do...while循环
1. do..while循环的一个循环体都会执行一遍，这是和while循环是不一样的。
```
var a=0;
do{
    n=n+1;
}while(n<100);

```
+ 练习 请利用循环遍历数组中的每个名字，并显示Hello, xxx!：
```
var arr=['Bart','Lisa','Adam'];
//for循环
for(i=0;i<arr.length;i++){
   console.log(`Hello`+arr[i]);
}

//while循环
i=0
while(i<3){
  console.log(`Hello`,+arr[i])
  i++;
}


```
## Map和Set
1. Map是类似Object的一种键值对集合，区别在于Map的键不仅是字符串，其它各种类型的值包括对象也都可以是Map的键。

+ Map
```
var a=new Map();
a.set('aaa',11);
a.set('bbb',22);
console.log(a.get('aaa'));

```
+ Set
```
var b=new Set();
b.set('ccc',33);
b.set('ddd',44);
console.log(b.get['ccc'];
```
1. add(value):添加某个值，返回Set结构本身。
2. delete(value):删除某个值，返回一个布尔值，表示删除是否成功。
3. has(value):返回一个布尔值，表示该值是否为Set的成员。
4. clear():清除所有成员，没有返回值。


