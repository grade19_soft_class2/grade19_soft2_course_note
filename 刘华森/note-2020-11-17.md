# 如何解决考试试卷合并冲突的问题

# 在考卷中不要乱动原文件，如果在发生冲突了，或是把原文件给改了，删除改过的文件把原文件fork下来再次提交，切勿修改。

# Lambda 表达式 其实就是匿名函数

# git 回顾和安装配置

# 委托 

1. 自定义委托
   '''
    deleget void Say();

    function void SayHello(){

    }

    Say s-SayHello;
    '''
 
2. 内置委托 func actino

   +func 代表有一个返回值的方法
   + action 代表没有返回值的方法
   
 ##   MarkDown添加图片的方式

 ![avatar](./imgs/2020-11-17-9-14-21.JPG)

 ![avatar](./imgs/2020-11-17-9-25-22.JPG)

 ![avatar](./imgs/2020-11-17-9-42-40.JPG)
 