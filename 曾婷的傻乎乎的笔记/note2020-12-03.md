# 补：请假第四天的笔记 🐷
## forEach
```
var arr = ['测试1', '测试2', '测试3'];
arr.forEach(function(item, index, arr){
    console.log('这里打印的是当前元素的值:' + item);
    console.log('这里打印的是当前元素的索引:' + index);
    console.log('这里打印的是Array对象本身:' + arr);
});

// Map的回调函数参数依次为value、key和map本身
var map = new Map([['a', 1], ['b', 2], ['c', 3]]);
map.forEach(function(value, key, arr){
    console.log('这里打印的是当前元素的值:' + value);
    console.log('这里打印的是当前元素的键:' + key);
    console.log('这里打印的是Array对象本身:' + arr);
});

// 因为Set没有索引，因此回调函数的前两个参数都是元素本身
var set = new Set(['x', 'y', 'z']);
set.forEach(function(key, rekey, arr){
    console.log('这里打印的是当前元素的键:' + key);
    console.log('这里打印的还是当前元素的键:' + rekey);
    console.log('这里打印的是Array对象本身:' + arr);
});
```
## iterable
```
遍历Array可以采用下标循环，遍历Map和Set就无法使用下标。为了统一集合类型，ES6标准引入了新的iterable类型，Array、Map和Set都属于iterable类型。
```
## for...of 遍历
```
var m = new Map([['HH','1'],['AA','2'],['DD','3'],['GG','4']]);
var s = new Set(['a','b','c','d']);


for(var item of s){
    console.log(item);
}

for(var item of m){
    console.log(item);
}
// 遍历数组
var arr = [1, 2, 3];
for(var item of arr){
    console.log(item); // 1 2 3
}

// 遍历Map
var m = new Map([['小明', 100], ['小红', 99], ['小黄', 98]]);
for(var item of m){
    console.log(item); // (2) ["小明", 100]   (2) ["小红", 99]   (2) ["小黄", 98]
}

for (var x of m) { // 遍历Map
    console.log(`${x[0]}的分数为${x[1]}`); // 小明的分数为100   小红的分数为99   小黄的分数为98
}

// 遍历Set
var s = new Set([1, 2, 3]);
for(var item of s){
    console.log(item); // 1 2 3
}
```