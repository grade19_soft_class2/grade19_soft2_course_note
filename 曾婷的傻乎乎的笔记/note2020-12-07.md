# 12月七日，天气：晴🌤  心情：不错👍 学习状态：差😔（主要是有点跟不上了，得回去自己恶补了）
## 变量作用域
```
var声明的变量是有作用域的。
1. 如果一个变量是在函数内定义的，则该变量的作用域在函数体内，在函数体外则不可使用
2. 不同函数内部的同名变量互相独立，互不影响
3. 内部函数可以访问外部函数定义的变量，反过来则不行
4. 当内部函数定义了与外部函数重名的变量时，内部函数的变量将“屏蔽”外部函数的变量
```
## 变量提升
```
函数定义会先扫描整个函数的语句，把所有声明的变量提升到函数顶部

function foo() {                        function foo() {
    var x = 'Hello, ' + y;                 var y; // 提升变量y的申明，此时y为undefined
    console.log(x);           <=>          var x = 'Hello, ' + y;
    var y = 'World!';                      console.log(x); 
                                           y = 'World!'; 
}                                       }

foo();
在函数内部定义变量时，请严格遵守“在函数内部首先申明所有变量”这一规则
```

## 全局作用域
```
不在任何函数内定义的变量就具有全局作用域

var word = 'Hello World!';
console.log(word);// 'Hello World!'
console.log(window.word);// 'Hello World!'
函数也是一个全局变量。

名字空间
全局变量会绑定到window上，不同的JS文件如果使用了相同的全局变量，或者定义了相同名字的顶层函数，都会造成命名冲突，并且很难被发现。 减少冲突的一个方法是把自己的所有变量和函数全部绑定到一个全局变量中。

// 唯一的全局变量Hello:
var Hello = {};

// 其他变量:
Hello.name = 'Hello World';

// 其他函数:
Hello.func = function () {
    return '你好';
};
```
## 局部作用域
```
块级作用域，用let代替var声明一个块级作用域

function foo() {
    var sum = 0;
    for (let i=0; i<10; i++) {
        sum += i;
        console.log(i);
    }
    i += 1; /// i is not defined;
}
foo() //调用函数
```
## 常量
```
const与let都具有块级作用域

const PI = 3.14;
PI = 3; 
console.log(PI);/// 报错;
const PI = 3.14;
console.log(PI);/// 3.14;
```