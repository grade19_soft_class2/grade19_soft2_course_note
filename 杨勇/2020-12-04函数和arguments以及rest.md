## 函数
1. 函数的定义
+ 第一种方法//function直接定义
```
function input(arr){
    return arr;
}
console.log(input(11))//得到结果11
```
+ 第二种方法//匿名函数
```
var arr=function(arr){
    return arr;
}
console.log(arr(11));
```
+ 第三种方法//箭头函数
```
console.log(arr(11));
var str=(arr)=>{
    return arr;
}
console.log(str(11))
```
2. 函数的调用
+ 函数的调用秩序要调用该函数时传入需要的参数即可;
+ 注意：调用函数时需要传入跟函数形参数量一样的实参，否则需要arguments和rest来对函数多余或少于形参数量的实参来进行操作;
+ 例如：
```
function vbs(as,bs,cs){
    console.log(as);
    console.log(bs);
    console.log(cs);
}
vbs(11,23,44,111);//此时会获得11，23，44
vbs(3,14);//此时会获得3，14
```
## arguments和rest
### arguments
1. arguments的实际操作;
```
function vbs(as,bs,cs,...rest){
    if(arguments.length<3){
         as=as,
         bs=bs,
         cs=null
    
    }console.log(as);
    console.log(bs);
    console.log(cs);  
}

vbs(11,23,44,111);//得到结果除111外都有
vbs(3,14);//得到结果3，14，null
vbs(3);//得到结果3，undefined,null
```
+ 得到结论,可以通过arguments来对与形参数量不同的调用方法来操作;无论实参是多于还是少于都有返回值;
2. rest的实际操作
```
function vbs(as,bs,cs,...rest){
    if(arguments.length<3){
         as=as,
         bs=bs,
         cs=null
    console.log(as);
    console.log(bs);
    console.log(cs);   
    }
    else if(arguments.length>3){
        console.log(as);
        console.log(bs);
        console.log(cs);
        console.log(rest);
    }
    else{
        console.log(as,bs,cs);
    }
}

vbs(11,23,44,111);//得到结果是11，23，44，[1111]
vbs(3,14);//得到结果是3，14，null
vbs(3);//得到结果是3，undifned,null
```
+ 得到结论，rest可以将多余的传入值整合为一个集合的形式来输出;

## 练习
+ 定义一个计算圆面积的函数area_of_circle()，它有两个参数：
+ r: 表示圆的半径；
+ pi: 表示π的值，如果不传，则默认3.14
```
function area_of_circle(r,pi){
    
    if(arguments.length==1){
       var pi=3.14;
       return r*r*pi;
    }
    else{
       return pi*r*r;
    }  
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               

console.log(area_of_circle(2),area_of_circle(2,3.1416))

if (area_of_circle(2) === 12.56 && area_of_circle(2, 3.1416) === 12.5664) {
    console.log('测试通过');
} else {
    console.log('测试失败');
}
```


