## 装饰器
+ 内置函数可以通过自我重新定义来覆盖掉原本的定义;
+ 例如:
```
'use strict'
var getlog=alert;
window.alert=function(x){
    console.log(x);
}
 alert('333');
```
+ 此时，原本alert的弹窗提示被替换成立console.log,而原本的alert则要通过getlog来调用.
## 高阶函数
+ 高阶函数是利用一个函数，函数所调用的形参中有一个或多个函数组成。
+ 举个例子：计算两个数字的绝对值的和
```
function num(x,y,fn) {
    return fn(x)+fn(y)
}

console.log(num(5,-5,Math.abs))
```
## map和reduce
+ map是使数组每个元素单独进行操作
+ reduce使数组每个元素跟下一个元素进行操作，直到遍历全部.
```
'use strict'
var arr=[1,2,3,4,5,6,7,8,9];
function scure1(x){
   return x*x;
} 
function num(x) {
    return x+x;
}
var res=arr.map(scure1).reduce(num); 

console.log(res)
```
## filter
+ filter 可以将不符合条件的元素去除，保留剩下的元素;
+ 练习：过滤掉首字母不为大写的元素;
```
var arr1=['adam', 'LISA', 'barT'];

var firstA=arr1.filter(function(x){  
    return x[0]==x[0].toUpperCase();
})

console.log(firstA);//LISA.

```

## 练习：请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。
```
'use strict'
var arr1=['adam', 'LISA', 'barT'];
// ## 练习：请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。
function low(arr) {
    var arr2=[];
    arr.forEach(function(arrItem){
        arr2.push(arrItem.toLowerCase())
    })
    return arr2
}//先全部化为小写
var arr3=low(arr1);

var arr = arr3.map(function(str){
    str = str[0].toUpperCase()+str.substring(1);
    return str;
})//在通过拼接的办法将首字母大写，在跟剩下的字母链接

console.log(arr)
```
## 练习 请尝试用filter()筛选出素数：
```
var arr=[];
for (var s = 1; s < 100; s++) {
    arr.push(s);
}
console.log(arr)

var ass=arr.filter(function(x){
    var count=0;
    for(var i=1;i<=x;i++){
        if(x%i===0){
            count++;           
        }    
    }
    return count==2;
    
})
console.log(ass.toString())
if (ass.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
    console.log('测试通过!');
} else {
    console.log('测试失败: ' + ass.toString());
}
```