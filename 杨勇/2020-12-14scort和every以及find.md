## sort
+ sort 是一个排序算法，可以将数字以首位数字大小来排序，也可以将英文以首位字母来排序，但是切记，多为数字也会出现以首位数字大小来排序的情况，而且英文也会默认将首字母大写的放在前面.
+ 解决方法就是自己建立一个算法来将这些情况在函数中统一比较.
```
var arr = ['Google', 'apple', 'Microsoft'];
var s= arr.sort(function (s1, s2) {
   var x1 = s1.toUpperCase();
   var x2 = s2.toUpperCase();//将传入值先全大写来统一比较.
    if (x1 < x2) {
        return -1;
    }
    if (x1 > x2) {
        return 1;
    }
    return 0;
});

console.log(s)
```
## every
+ every是将数组中的所有元素都进行操作后来返回是否满足，全都满足才返回正确，如果有不满足的，哪怕一个也会返回错误.
+ 练习 every是将数组中的所有元素都进行操作后来返回是否满足，全都满足才返回正确，如果有不满足的，哪怕一个也会返回错误.
```
var arr = ['Google', 'apple', 'Microsoft'];

 function ass(arr){
  console.log(arr);
  return arr[0].toUpperCase()===arr[0];//判断首字母大写后是否与原字母相同;
  
}

console.log(arr.every(ass))//判断为false
```
## find
+ find可以查找符合你条件的元素，找到的第一个返回这个元素.
```
// 返回首字母大写的第1个单词。
var arr = ['Google', 'apple', 'Microsoft'];
function FirstBig(ass){
    return ass[0].toUpperCase()==ass[0];
}

console.log(arr.find(FirstBig))
```
## findIndex
+ findIndex类似于find，但是不同的是findIndex是返回符合条件的元素的索引.
+ findIndex跟IndexOf的类似，但是区别是findIndex用来接受复杂的函数形式来进行测试，二IndexOf是用来接受原始数组的元素来进行查找.
```
'use strict';
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.findIndex(function (s) {
    return s.toLowerCase() === s;
})); // 1, 因为'pear'的索引是1
```
## forEach
+ forEach就是便利一个数组
+ 输出一下arr的全部元素
```
var arr = ['Google', 'apple', 'Microsoft'];
function ass(arr){
    arr.forEach(function (item){
         console.log(item)
    })
}

ass(arr);
```

