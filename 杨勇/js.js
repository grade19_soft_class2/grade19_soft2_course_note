'use strict'
//parseInt() 函数可解析一个字符串，并返回一个整数;
function Student(name,age){
    this.name=name;
    this.age=age;
    this.Hello=function(){
        return 'hello'+this.name
    }
}
Student.prototype.age1=function(){
       return  this.age;
}
var xiaoming=new Student('小明',14);
xiaoming.__proto__.age1===Student.prototype.age1;//判断xiaoming是否为student函数的子类对象，且age1是否为student的子函数
console.log(xiaoming.__proto__===Student.prototype);//true
//由此可知，xiaoming.__proto是完全等同于Student.prototype的

xiaoming.Hello();
console.log(xiaoming.Hello());//hello小明
console.log(xiaoming.age1());//14