# 课堂内容
## parseInt
+ parseInt函数可解析一个字符串，并返回一个整数;
```
var arr='21341';
console.log(parseInt(arr))//21341
var arr1='245na55';
console.log(parseInt(arr1))//245
```
+ 结论得到，parseInt可以将字符串转化为整数，但是在字符串中含有其它符号或英文或汉字时就会只返回这些元素之前的数字；

## number
+ number函数同样可以解析一个字符串
```
var arr2='21341';
console.log(parseInt(arr2));/21341
console.log(Number(arr2));/21341
var arr1="23a55";
console.log(parseInt(arr1));/23
console.log(Number(arr1));/Nan
```
+ number和parseInt的区别就是，number函数转化的字符串如果含有非数字，就会直接返回Nan
