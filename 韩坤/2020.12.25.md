### document
+ document对象表示当前页面。
+ 每个载入浏览器的 HTML 文档都会成为 Document 对象。
+ Document 对象使我们可以从脚本中对 HTML 页面中的所有元素进行访问。
找到对象
1. 通过id查找：
```
var a = document.getElementById("a");
```
括号内为元素的id；
2. 通过class查找：
```
var b = document.getElementsByClassName("a");
```
括号内为元素的class；如果在class名后面加上[0],则可找到其中第一个元素，[]内的数字和数组内数字的顺序相同；
3. 通过标签名查找：
```
var c = document.getElementsByTagName("div");
```
括号内为标签名；
4. 通过name查找：
```
var d = document.getElementsByName("uid");
```
这种查找方式适用于表单元素，括号内为表单元素的名字（name）。
其次是对内容进行操作，即获取和修改：
    获取：例如
```
var d = document.getElementById("a");
```
要想获取元素内文本的内容，可输入
```
alert(d.innerText);
```
不过这种方式只能输入文本，样式不显示；

如果要获取元素内所有的内容，包含标签，则需要使用
```
alert(d.innerHTML);。
```
修改，会把原有的内容覆盖：
修改元素里面的文本内容使用，例：
```
d.innerText = "<b>你好</b>";；
```
修改元素内包括HTML代码在内的所有元素，例：
```
d.innerHTML = "<b>你好</b>";
```

#### Cookie
##### 什么是 cookie？
Cookie 是在您的计算机上存储在小的文本文件中的数据。
当 web 服务器向浏览器发送网页后，连接被关闭，服务器会忘记用户的一切。
Cookie 是为了解决“如何记住用户信息”而发明的：
+ 当用户访问网页时，他的名字可以存储在 cookie 中。
+ 下次用户访问该页面时，cookie 会“记住”他的名字。
#### 设置cookie
每个cookie都是一个名/值对，可以把下面这样一个字符串赋值给document.cookie：
```
document.cookie="userId=828";
```
document.cookie看上去就像一个属性，可以赋不同的值。但它和一般的属性不一样，改变它的赋值并不意味着丢失原来的值，例如连续执行下面两条语句：
```
document.cookie="userId=828";
document.cookie="userName=hulk"; 
```
这时浏览器将维护两个cookie，分别是userId和userName，因此给document.cookie赋值更像执行类似这样的语句：
```
document.addCookie("userId=828");
document.addCookie("userName=hulk");
```
事实上，浏览器就是按照这样的方式来设置cookie的，如果要改变一个cookie的值，只需重新赋值，例如：
```
document.cookie="userId=929";
```
这样就将名为userId的cookie值设置为了929。
#### 获取cookie的值
下面介绍如何获取cookie的值。cookie的值可以由document.cookie直接获得：
```
var strCookie=document.cookie; 
```
这将获得以分号隔开的多个名/值对所组成的字符串，这些名/值对包括了该域名下的所有cookie。例如：
```
document.cookie="userId=828";
document.cookie="userName=hulk";
var strCookie=document.cookie;
console.log(strCookie); //userId=828; userName=hulk
```
当然这个要在环境下运行，因为是获取当前域名下的cookie。
由此可见，只能够一次获取所有的cookie值，而不能指定cookie名称来获得指定的值，这正是处理cookie值最麻烦的一部分。
用户必须自己分析这个字符串，来获取指定的cookie值，例如，要获取userId的值,可以这样实现:
```
document.cookie="userId=828";
document.cookie="userName=hulk";
var strCookie=document.cookie;
console.log(strCookie); //userId=828; userName=hulk

function getdescookie(strcookie,matchcookie){
    var getMatchCookie;
    var arrCookie=strcookie.split(";");
    for(var i=0;i<arrCookie.length;i++){
         var arr=arrCookie[i].split("=");
         if(matchcookie == arr[0]){
                getMatchCookie = arr[1];
                break;
         }
    }
    return getMatchCookie;
}
var resultCookie = getdescookie(strCookie,'userId');
console.log(resultCookie); //828
```
这样就得到了单个cookie的值。
如果在某个页面创建了一个cookie，那么该页面所在目录中的其他页面也可以访问该cookie。如果这个目录下还有子目录，则在子目录中也可以访问。
为了控制cookie可以访问的目录，需要使用path参数设置cookie，语法如下：
```
document.cookie="name=value; path=cookieDir";
```
其中cookieDir表示可访问cookie的目录。
例如：
```
document.cookie="userId=320; path=/shop";
```
就表示当前cookie仅能在shop目录下使用。
如果要使cookie在整个网站下可用，可以将cookie_dir指定为根目录，例如：
```
document.cookie="userId=320; path=/";
```
### history
+ History 对象包含用户（在浏览器窗口中）访问过的 URL。
+ History 对象是 window 对象的一部分，可通过 window.history 属性对其进行访问。
1. pushState
```
pushState只会在当前history中添加一条记录，并不会刷新浏览器
history.pushState({}, "my title", "/test.html")
```
2. replaceState
```
replaceState会替换当前的history中的记录，并且刷新浏览器
history.replaceState({}, "my title", "/test.html")
```
3. 跳转
```
history.go(1)   前进一个页面
history.go(-1)  后退一个页面
```
4. navigator
```
获取用户浏览器相关信息
console.log(navigator)
```
## 操作DOM
### `常用方法`
1. 获取节点：
    1. document.getElementById(idName)          //通过id号来获取元素，返回一个元素对象
    2. document.getElementsByName(name)       //通过name属性获取id号，返回元素对象数组
    3. document.getElementsByClassName(className)   //通过class来获取元素，返回元素对象数组（ie8以上才有）
    4. document.getElementsByTagName(tagName)       //通过标签名获取元素，返回元素对象数组

2. 获取/设置元素的属性值：
    1. element.getAttribute(attributeName)     //括号传入属性名，返回对应属性的属性值
    2. element.setAttribute(attributeName,attributeValue)    //传入属性名及设置的值

3. 创建节点Node：
    1. document.createElement("h3")       //创建一个html元素，这里以创建h3元素为例
    2. document.createTextNode(String); //创建一个文本节点；
    3. document.createAttribute("class"); //创建一个属性节点，这里以创建class属性为例

4. 增添节点：
    1. element.appendChild(Node);   //往element内部最后面添加一个节点，参数是节点类型
    2. elelment.insertBefore(newNode,existingNode); //在element内部的中在existingNode前面插入newNode

5. 删除节点：
    1. element.removeChild(Node)    //删除当前节点下指定的子节点，删除成功返回该被删除的节点，否则返回null

### `常用属性`

1. 获取当前元素的父节点 ：
    1. element.parentNode     //返回当前元素的父节点对象
2. 获取当前元素的子节点：
    1. element.chlidren        //返回当前元素所有子元素节点对象，只返回HTML节点
    2. element.chilidNodes   //返回当前元素多有子节点，包括文本，HTML，属性节点。（回车也会当做一个节点）
    3. element.firstChild      //返回当前元素的第一个子节点对象
    4. element.lastChild       //返回当前元素的最后一个子节点对象
3. 获取当前元素的同级元素：
    1. element.nextSibling          //返回当前元素的下一个同级元素 没有就返回null
    2. element.previousSibling   //返回当前元素上一个同级元素 没有就返回null
4. 获取当前元素的文本：
    1. element.innerHTML   //返回元素的所有文本，包括html代码
    2. element.innerText     //返回当前元素的自身及子代所有文本值，只是文本内容，不包括html代码
5. 获取当前节点的节点类型：node.nodeType   //返回节点的类型,数字形式（1-12）常见几个1：元素节点，2：属性节点，3：文本节点。
    1. 设置样式：element.style.color=“#eea”;      //设置元素的样式时使用style，这里以设置文字颜色为例。

