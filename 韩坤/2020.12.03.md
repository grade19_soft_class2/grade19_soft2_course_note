## for ... in
+ 它可以把一个对象的所有属性依次循环出来:
```
var song={
    name:'野狼disco',
    singer:'无名人士',
    hot:'yes'
}
for(var key in song){
    console.log(key);//name;singer;hot
}
```

+ 因为数组也是对象，数组每个元素的索引被视为对象的属性，因此，for ... in循环可以直接循环出Array的索引：
```
var a = ['A', 'B', 'C'];
for (var i in a) {
    console.log(i); // '0', '1', '2'
    console.log(a[i]); // 'A', 'B', 'C'
}
```

## while 
+ 只有一个判断条件，条件满足就不断循环
```
var x=0;
var y=0;
while(x<100){
    x=x+1;
    y=x+y;
    
}console.log(y);//1加到100
```

## do while
+ 和while的区别:不是在每次循环开始的时候判断条件，而是在每次循环完成的时候判断条件：

# `Map和Set`
## Map
Map是一组键值对的结构，根据键查找值的方法；
```
var m=new Map([['坤',100],['韩',99],['kun',90]]);
var m1=m.get('坤');
console.log(m1);
```
或者直接初始化一个空Map;
```
var mm=new Map();
mm.set('春',20);添加新的键值对
mm.set('夏',38);
mm.set('秋',20);
mm.set('冬',0)
mm.set('南极',-60)
mm.delete('南极')//删除键值对
var mmm=mm.has('南极')//是否存在键南极
console.log(mmm);//false
console.log(mm);
```
一个key只能对应一个value，多次对一个key放入value会把前面的value替换掉
## Set
Set和Map类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在Set中，没有重复的key。

要创建一个Set，需要提供一个Array作为输入，或者直接创建一个空Set：
```
var s1 = new Set(); // 空Set
var s2 = new Set([1, 2, 3]); // 含1, 2, 3
```
重复元素在Set中自动被过滤：
```
var s = new Set([1, 2, 3, 3, '3']);
s; // Set {1, 2, 3, "3"}
```

# iterable
## for ... of
+ 具有iterable(Array、Map、Set)类型的集合可以通过新的for ... of循环来遍历。
+ 不能应用于对象