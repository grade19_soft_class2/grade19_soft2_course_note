# 2020年12月24日课堂笔记

## 原型继承
```
//构造函数
function Student(props) {
    this.name = props.name || 'Unnamed';
}

Student.prototype.hello = function () {
    console.log('Hello, ' + this.name + '!');
}

// PrimaryStudent构造函数:
function PrimaryStudent(props) {
    Student.call(this, props);
    this.grade = props.grade || 1;
}

// 空函数F:
function F() {
}

// 把F的原型指向Student.prototype:
F.prototype = Student.prototype;

// 把PrimaryStudent的原型指向一个新的F对象，F对象的原型正好指向Student.prototype:
PrimaryStudent.prototype = new F();

// 把PrimaryStudent原型的构造函数修复为PrimaryStudent:
PrimaryStudent.prototype.constructor = PrimaryStudent;

// 继续在PrimaryStudent原型（就是new F()对象）上定义方法：
PrimaryStudent.prototype.getGrade = function () {
    return this.grade;
};

// 创建xiaoming:
var xiaoming = new PrimaryStudent({
    name: '小明',
    grade: 2
});
xiaoming.name; // '小明'
xiaoming.grade; // 2

// 验证原型:
xiaoming.__proto__ === PrimaryStudent.prototype; // true
xiaoming.__proto__.__proto__ === Student.prototype; // true

// 验证继承关系:
xiaoming instanceof PrimaryStudent; // true
xiaoming instanceof Student; // true
```

注:prototype:原型；__proto__:查找到父对象；constructor：函数。

## class
例:  

构造函数：  
```
function Student(props) {
    this.name = props.name || 'Unnamed';
}

Student.prototype.hello = function () {
    console.log('Hello, ' + this.name + '!');
}

let xm = new Student("小明");
console.log(xm.hello());
```
class定义：  
```
class Student{
    constructor(name){
        this.name=name;
    }

    hello(){
        console.log('Hello, ' + this.name + '!');
    }
}
//调用方法一致
let xm = new Student("小明");
console.log(xm.hello());
```

## class继承
```
//原class
class Student{
    constructor(name){
        this.name=name;
    }

    hello(){
        console.log('Hello, ' + this.name + '!');
    }
}
//继承
class PrimaryStudent extends Student{
    constructor(name,grade){
        super(name);//必须用super调用父类的构造方法
        this.grade = grade;
    }

    myGrade() {
        console.log(this.grade);
    }
}

let xm = new PrimaryStudent("小明",2);

//打印
console.log(xm.hello());
console.log(xm.myGrade());
```