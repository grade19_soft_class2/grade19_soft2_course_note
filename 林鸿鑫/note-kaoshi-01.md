## 考试内容
### 数组


1.题目描述 找出元素 item 在给定数组 arr 中的位置 输出描述: 如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1

// 示例
// 输入  [ 1, 2, 3, 4 ], 3
// 输出   2

    function indexOf(arr,item){
    return arr.indexOf(item)
    }

    console.log(indexOf([1,2,3,4],3));


2.题目描述 计算给定数组 arr 中所有元素的总和 输入描述: 数组中的元素均为 Number 类型

// 示例
// 输入  [ 1, 2, 3, 4 ]
// 输出  10


    function total (arr){
        var sum = 0;
        for(let i =0;i<arr.length;i++){
            sum += arr[i]
        }
        return sum
    }

    console.log(total([1,2,3,4]));

3.题目描述 移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组

// 示例
// 输入  [1, 2, 3, 4, 2], 2
// 输出  [1, 3, 4]

    function  remove (arr,item){
        var aa =[];
        arr.forEach(function(arritem) {
            if(item != arritem){
              aa.push(arritem);
            }
        })
        return aa;
    }

    console.log(remove([1,2,3,4,2],2));

4.题目描述 移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回

// 示例
// 输入  [1, 2, 2, 3, 4, 2, 2], 2
// 输出  [1, 3, 4]


    function remove2 (arr,item){
        while(arr.indexOf(item)!= -1){
            arr.splice(arr.indexOf(item),1)
        }
        return arr;
    }

    console.log(remove2([1,2,2,3,4,2,2],2));


5.题目描述 在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组

// 示例
// 输入  [1, 2, 3, 4],  10
// 输出  [1, 2, 3, 4, 10]

    function add(arr,item){
        return arr.concat(item);
    }

    console.log(add([1,2,3,4],10));

6.题目描述 删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组

// 示例
// 输入  [1, 2, 3, 4]
// 输出  [1, 2, 3]

    function shanchu(arr,item){
        return arr.splice(0,arr.length-1);
    }

    console.log(shanchu([1,2,3,4]));

7.题目描述 在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组

// 示例
// 输入  [1, 2, 3, 4], 10
// 输出  [10, 1, 2, 3, 4]

    function add2(arr,item){
        return [item].concat(arr)
    }

    console.log(add2([1,2,3,4],10));

8.题目描述 一删除数组 arr 第个元素。不要直接修改数组 arr，结果返回新的数组

// 示例
// 输入  [1, 2, 3, 4]
// 输出  [2, 3, 4]

    function shanchu2(arr){
        var aa= arr.slice(0);
        aa.shift();
        return aa;
    }

    console.log(shanchu2([1,2,3,4]));


9.题目描述 合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组

// 示例
// 输入  [1, 2, 3, 4], ['a', 'b', 'c', 1]
// 输出  [1, 2, 3, 4, 'a', 'b', 'c', 1]

    function concat1 (arr1,arr2){
        var arr = arr1.slice(0);
        arr = arr.concat(arr2);
        return arr;
    }

    console.log(concat1([1,2,3,4],['a','b','c',1]));


10.题目描述 在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组

// 示例
// 输入  [1, 2, 3, 4], 'z', 2
// 输出  [1, 2, 'z', 3, 4]


    function insert (arr,item,index){
        var result = arr.slice(0);
        result.splice(index,0,item);
        return result;
    }

    console.log(insert([1,2,3,4],'z',2));

11.题目描述 统计数组 arr 中值等于 item 的元素出现的次数

// 示例
// 输入  [1, 2, 4, 4, 3, 4, 3], 4
// 输出  3

    function countFrequency(arr, item) {
        var s = 0;
        for(var i = 0; i<arr.length; i++){
            if(arr[i] == item){
                s++;  
            }    
        }
        return s;    
    }
    console.log(countFrequency([1, 2, 4, 4, 3, 4, 3], 4))

12.题目描述 找出数组 arr 中重复出现过的元素

// 示例
// 输入  [1, 2, 4, 4, 3, 3, 1, 5, 3]
// 输出  [1, 3, 4]

    function chongfu(arr) {
        var result = [];
        arr.forEach(function(item){
            if((arr.indexOf(item) != -1)&&(arr.indexOf(item) != arr.lastIndexOf(item))){
                if(result.indexOf(item) == -1){
                    result.push(item)
                }
            }
        });
        return result;
    }

    console.log(chongfu([1,2,4,4,3,3,1,5,3]));

13.题目描述 为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组

// 示例
// 输入  [1, 2, 3, 4]
// 输出  [1, 4, 9, 16]

    function square(arr) {
        var result = arr.slice(0);
        result.forEach(function(item,index){
            result[index] = item * item;
        })
        return result;
    }

    console.log(square([1,2,3,4]));


14.题目描述 在数组 arr 中，查找值与 item 相等的元素出现的所有位置

// 示例
// 输入  'abcdefabc'
// 输出  [0, 6]


    function findAllOccurrences3(arr, target) {
        var result = [];
        
        var index = arr.lastIndexOf(target);
        
        while(index > -1) {
            result.push(index);
            arr.splice(index,1);
            index = arr.lastIndexOf(target);
        }
        return result;
    } 

    console.log(findAllOccurrences3('abcdefabc'),'f')

### jQ


1.使用jQuery选择器分别选出指定元素：

 + 仅选择JavaScript

 + 仅选择Erlang

 + 选择JavaScript和Erlang

 + 选择所有编程语言

 + 选择名字input

 + 选择邮件和名字input
// ```
// <!-- HTML结构 -->
// <div id="test-jquery">
//     <p id="para-1" class="color-red">JavaScript</p>
//     <p id="para-2" class="color-green">Haskell</p>
//     <p class="color-red color-green">Erlang</p>
//     <p name="name" class="  ">Python</p>
//     <form class="test-form" target="_blank" action="#0" onsubmit="return false;">
//         <legend>注册新用户</legend>
//         <fieldset>
//             <p><label>名字: <input name="name"></label></p>
//             <p><label>邮件: <input name="email"></label></p>
//             <p><label>口令: <input name="password" type="password"></label></p>
//             <p><button type="submit">注册</button></p>
//         </fieldset>
//     </form>
// </div>
// 运行查看结果：

            'use strict';

            var selected = $('#para-1');//仅选择JavaScript
            var selected = $('.color-red.color-green');//仅选择Erlang
            var selected = $('.color-red');//选择JavaScript和Erlang 
            var selected = $('[class^="color-"]');//选择所有编程语言
            var selected = $('input[name = name]');//选择名字input
            var selected = $('input[name=name],input[name=email]'); //选择邮件和名字input


            if (!(selected instanceof jQuery)) {
                console.log('不是有效的jQuery对象!');
            }
            $('#test-jquery').find('*').css('background-color', '');
            selected.css('background-color', '#ffd351');


2. 针对如下HTML结构：
// ```
// <!-- HTML结构 -->

// <div class="test-selector">
//     <ul class="test-lang">
//         <li class="lang-javascript">JavaScript</li>
//         <li class="lang-python">Python</li>
//         <li class="lang-lua">Lua</li>
//     </ul>
//     <ol class="test-lang">
//         <li class="lang-swift">Swift</li>
//         <li class="lang-java">Java</li>
//         <li class="lang-c">C</li>
//     </ol>
// </div>
// ```
// 选出相应的内容并观察效果：

        'use strict';
        var selected = null;
        // 分别选择所有语言，所有动态语言，所有静态语言，JavaScript，Lua，C等:
        //选择所有语言(属性选择器)
        selected = $("[class^='lang-']");
        //选择所有动态语言
        selected=$("ul.test-lang li")
        //选择所有静态语言
        selected=$("ol.test-lang li")
        //选择JavaScript
        selected=$("ul.test-lang li:first-child");
        //选择lua
        selected=$("ul.test-lang li:nth-child(3)");
        //选择C
        selected=$("ol.test-lang li:nth-child(3)")

        // 高亮结果:
        if (!(selected instanceof jQuery)) {
            console.log('不是有效的jQuery对象!');
        }
        $('#test-jquery').find('*').css('background-color', '');
        selected.css('background-color', '#ffd351');



3. 对于下面的表单：
```
<form id="test-form" action="#0" onsubmit="return false;">
    <p><label>Name: <input name="name"></label></p>
    <p><label>Email: <input name="email"></label></p>
    <p><label>Password: <input name="password" type="password"></label></p>
    <p>Gender: <label><input name="gender" type="radio" value="m" checked> Male</label> <label><input name="gender" type="radio" value="f"> Female</label></p>
    <p><label>City: <select name="city">
    	<option value="BJ" selected>Beijing</option>
    	<option value="SH">Shanghai</option>
    	<option value="CD">Chengdu</option>
    	<option value="XM">Xiamen</option>
    </select></label></p>
    <p><button type="submit">Submit</button></p>
</form>
```
输入值后，用jQuery获取表单的JSON字符串，key和value分别对应每个输入的name和相应的value，例如：`{"name":"Michael","email":...}`
```
'use strict';
var json = null;
var obj={};
var form = $("#test-form :input");
var ff=form.filter(function(){
if (this.type==='radio' && !this.checked || this.type==='submit')
    return false;
else
    return true;})
ff.map(function(){
return obj[this.name]=this.value;
})
json=JSON.stringify(obj,null,'  ');
// 显示结果:
if (typeof(json) === 'string') {
    console.log(json);
}
else {
    console.log('json变量不是string!');
}

```






4. 练习：分别用css()方法和addClass()方法高亮显示JavaScript：

// <!-- HTML结构 -->
{/* <style>
.highlight {
    color: #dd1144;
    background-color: #ffd351;
} */}
// </style>

{/* <div id="test-highlight-css">
    <ul>
        <li class="py"><span>Python</span></li>
        <li class="js"><span>JavaScript</span></li>
        <li class="sw"><span>Swift</span></li>
        <li class="hk"><span>Haskell</span></li>
    </ul>
</div> */}

            var div = $('#test-highlight-css');
            // TODO:
            var js = $('.js');

            js.addClass('highlight');

 
Python
JavaScript
Swift
Haskell

5. 对如下的Form表单：
```
<!-- HTML结构 -->
<form id="test-form" action="test">
    <legend>请选择想要学习的编程语言：</legend>
    <fieldset>
        <p><label class="selectAll"><input type="checkbox"> <span class="selectAll">全选</span><span class="deselectAll">全不选</span></label> <a href="#0" class="invertSelect">反选</a></p>
        <p><label><input type="checkbox" name="lang" value="javascript"> JavaScript</label></p>
        <p><label><input type="checkbox" name="lang" value="python"> Python</label></p>
        <p><label><input type="checkbox" name="lang" value="ruby"> Ruby</label></p>
        <p><label><input type="checkbox" name="lang" value="haskell"> Haskell</label></p>
        <p><label><input type="checkbox" name="lang" value="scheme"> Scheme</label></p>
		<p><button type="submit">Submit</button></p>
    </fieldset>
</form>
```
+ 绑定合适的事件处理函数，实现以下逻辑：

+ 当用户勾上“全选”时，自动选中所有语言，并把“全选”变成“全不选”；

+ 当用户去掉“全不选”时，自动不选中所有语言；

+ 当用户点击“反选”时，自动把所有语言状态反转（选中的变为未选，未选的变为选中）；

+ 当用户把所有语言都手动勾上时，“全选”被自动勾上，并变为“全不选”；

+ 当用户手动去掉选中至少一种语言时，“全不选”自动被去掉选中，并变为“全选”。
```
'use strict';

var
    form = $('#test-form'),
    langs = form.find('[name=lang]'),
    selectAll = form.find('label.selectAll :checkbox'),
    selectAllLabel = form.find('label.selectAll span.selectAll'),
    deselectAllLabel = form.find('label.selectAll span.deselectAll'),
    invertSelect = form.find('a.invertSelect');

// 重置初始化状态:
form.find('*').show().off();
form.find(':checkbox').prop('checked', false).off();
deselectAllLabel.hide();
// 拦截form提交事件:
form.off().submit(function (e) {
    e.preventDefault();
    alert(form.serialize());
});
// TODO:绑定事件:
 selectAll.change(function(){
    if(this.checked){
      langs.prop('checked',true);
      deselectAllLabel.show();
      selectAllLabel.hide();
    }else{
      langs.prop('checked',false);
      deselectAllLabel.hide();
      selectAllLabel.show();
    }
   })
 
   invertSelect.click(function(){
    langs.each(function(){
      if(this.checked){
        $(this).prop('checked',false);
      }else{
        this.checked = true;
      }
    })
   })
 
   
   var count = 0,
       len = langs.length;
   langs.click(function(){
    count = 0;
    langs.each(function(){
      if(this.checked)
        count++;
    })
    if(count == len){
      selectAll.prop('checked',true);
      deselectAllLabel.show();
      selectAllLabel.hide();
    }else{
        selectAllLabel.show();
        deselectAllLabel.hide();
        selectAll.prop('checked',false);
    }
   });

// 测试:
console.log('请测试功能是否正常。');
```

6. /* <!-- 第三题大题 --> */
<!-- <h3>
        <div id="test-div">
            <div class="c-red">
                <p id="test-p">JavaScript</p>
                <p>Java</p>
            </div>
            <div class="c-red c-green">
                <p>Python</p>
                <p>Ruby</p>
                <p>Swift</p>
            </div>
            <div class="c-green">
                <p>Scheme</p>
                <p>Haskell</p>
            </div>
        </div>
    </h3> -->


        // 选择<p>JavaScript</p>:
        var js = document.getElementById('test-p');

        // 选择<p>Python</p>,<p>Ruby</p>,<p>Swift</p>:
        var arr = document.getElementsByClassName('c-red c-green')[0].children;

        // 选择<p>Haskell</p>:
        var haskell = document.getElementsByClassName('c-green')[1].lastElementChild;
        // 测试:
        if (!js || js.innerText !== 'JavaScript') {
            alert('选择JavaScript失败!');
        } else if (!arr || arr.length !== 3 || !arr[0] || !arr[1] || !arr[2] || arr[0].innerText !== 'Python' || arr[1].innerText !== 'Ruby' || arr[2].innerText !== 'Swift') {
            console.log('选择Python,Ruby,Swift失败!');
        } else if (!haskell || haskell.innerText !== 'Haskell') {
            console.log('选择Haskell失败!');
        } else {
            console.log('测试通过!');
        } 


7. {/* <h3>
<ul id="test-list">
    <li>JavaScript</li>
    <li>Swift</li>
    <li>HTML</li>
    <li>ANSI C</li>
    <li>CSS</li>
    <li>DirectX</li>
</ul>
</h3> */}


        //按字符串顺序重新删除DOM节点：
        let parent = document.querySelector('#test-list');
        for(let i = 1 ;i<=3;i++){
            parent.removeChild[i];
        }
        // 测试:
        ; (function () {
            var
                arr, i,
                t = document.getElementById('test-list');
            if (t && t.children && t.children.length === 3) {
                arr = [];
                for (i = 0; i < t.children.length; i++) {
                    arr.push(t.children[i].innerText);
                }
                if (arr.toString() === ['JavaScript', 'HTML', 'CSS'].toString()) {
                    console.log('测试通过!');
                }
                else {
                    console.log('测试失败: ' + arr.toString());
                }
            }
            else {
                console.log('测试失败!');
            }
        })(); 



 
8. 请利用构造函数定义Cat，并让所有的Cat对象有一个name属性，并共享一个方法say()，返回字符串'Hello, xxx!'：

        function Cat(name) {
            this.name = name;
        }

        Cat.prototype.say = function () {
            return ('Hello, ' + this.name + '!');
        }
        // 测试:
        var kitty = new Cat('Kitty');
        var doraemon = new Cat('哆啦A梦');
        if (kitty && kitty.name === 'Kitty'
            && kitty.say
            && typeof kitty.say === 'function'
            && kitty.say() === 'Hello, Kitty!'
            && kitty.say === doraemon.say
        ) {
            console.log('测试通过!');
        } else {
            console.log('测试失败!');
        }
        console.log(kitty.say());


9. 除了列出的3种语言外，请再添加Pascal、Lua和Ruby，然后按字母顺序排序节点：
```
<!-- HTML结构 -->
<div id="test-div">
    <ul>
        <li><span>JavaScript</span></li>
        <li><span>Python</span></li>
        <li><span>Swift</span></li>
    </ul>
</div>
'use strict';
var ul =$('#test-div>ul');
var lan = ['Pascal','Lua','Ruby'];

//获取原来的内容
var n = ul.find('span').map(function(){
    return $(this).text();
}).get();

//把原来的内容和新内容结合起来，然后排序
lan = lan.concat(n).sort();

//加入html标签,转化成字符串

var strlan = lan.map(function(){
    return '<li><span>'+ a +'</span></li>';
}).join('');

//更改ul的结构
ul.html(strlan);


// 测试:
;(function () {
    var s = $('#test-div>ul>li').map(function () {
        return $(this).text();
    }).get().join(',');
    if (s === 'JavaScript,Lua,Pascal,Python,Ruby,Swift') {
        console.log('测试通过!');
    } else {
        console.log('测试失败: ' + s);
    }
})();
 
JavaScript
Python
Swift
```


3. 请使用相关数学函数在1 ~ 10000随机取一个数，并在控制台打印。

        console.log(Math.floor(Math.random()*10000));

2. 请编写一个程序，可以求出1到100之间所有偶数的和，并在控制台打印。

        let sum = 0;
        for (let i = 2;i<=100;i += 2){



5. 请声明一个函数，传入参数为正整数n，函数内计算n!，并返回结果。
 //提示：请注意对参数进行校验。
  //提示：n! = 1 * 2 * 3 * ... * n


    var cj = function(n){
        let r=1;
        for(let i=1;i<=n;i++){
            r *=i;
        }
        return r
    }

    console.log(cj(3));


4. 请声明一个函数，传入参数为绩效分数score，根据如下规则进行判断并返回相关字符串：
90~100：返回 考核等级S，奖励3倍工资
80~89：返回 考核等级A，奖励2倍工资
70~79：返回 考核等级B，奖励1倍工资
60~69：返回 等级C，不奖励，需要继续努力
0~59：返回 考核等级D，考核较差，需要再观察或者劝退
            // let w = parseInt(window.prompt("请输入绩效分数："));
            // if (w >=90){
            //     console.log("考核等级S，奖励3倍工资");
            // }else if(w >=80){
            //     console.log("考核等级A，奖励2倍工资");
            // }else if (w >=70){
            //     console.log("考核等级B，奖励1倍工资");
            // }else if (w >=60){
            //     console.log("等级C，不奖励，需要继续努力");
            // }else if (w<60){
            //     console.log("考核等级D，考核较差，需要再观察或者劝退");
            // }


6. 请声明一个函数，传入参数为字符串str，在函数内移除str首尾空格，然后计算长度，并返回长度

        /* function f2(str) {
            return str.trim().length;

        }


8. 已知一个数组为：[ 1, 4, 3, 2 ]，
//  请使用数组相关的方法检测元素 3 在数组中是否存在，
//  存在则在控制打印出相应的下标，否则打印不存在。


    let q = [1, 4, 3, 2];
    if (q.indexOf(3) !== -1) {
        console.log(q.indexOf(3));
    } else {
        console.log("不存在");
    }



9. 已知一个用户对象为：{id: 1, name: "庄周", desc: "辅助"}，
//  请尝试声明一个变量表示上面这个对象，
//  然后给这个对象增加一个性别属性，属性名称为：sex，属性值为：男。
//  最后在控制台打印这个对象。

    let user = { id: 1, name: "庄周", desc: "辅助" };
    user.sex = "男";
    console.log(user); 

10. 声明一个学生类，包含：姓名（name）、性别（gender）、生日字段（birthday）
//  实例化一个学生对象，学生相关属性为：
//      姓名：鬼谷子
//      性别：男
//      生日：2002-07-28
//  实例化完成之后，在控制台打印这个对象。

    function Stundet(name, gender, birthday) {
        this.name = name;
        this.gender = gender;
        this.birthday = birthday;

    }
    console.log(new Stundet("鬼谷子", "男", "2002-07-28"));
 