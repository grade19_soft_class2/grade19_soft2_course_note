## 高阶函数

+ 高阶函数就是，高级一点的函数。
+ JS的函数其实都指向某个变量。既然变量可以指向函数，函数的参数能接收变量，那么一个函数就可以接收另一个函数作为参数，这种函数就称之为高阶函数。

下面我就来给你举个例子吧：

    function   add(x,y,f){
        return f(x)  + f(y);
    }

    var x=add(-99,101,Math.abs);
    console.log(x);//200


使用高阶函数，就是让函数的参数能够接收别的函数。

## map/reduce


### map
+ map()传入的参数是pow，即函数对象本身。

比如：

    function pow (x){
        return x*x*2;

    }

    var arr =[1,2,3,4,5,6,7,8,9];
    var results = arr.map(pow);//[2,8,18,32,50,72,98,128,162]
    console.log(results);

所以，map()作为高阶函数，事实上它把运算规则抽象了，因此，我们不但可以计算简单的f(x)=x*x*2，还可以计算任意复杂的函数，比如，把Array的所有数字转为字符串：

    var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    var str =  arr.map(String); // ['1', '2', '3', '4', '5', '6', '7', '8', '9']
    console.log(str);

### reduce
再看reduce的用法。Array的reduce()把一个函数作用在这个Array的[x1, x2, x3...]上，这个函数必须接收两个参数，reduce()把结果继续和序列的下一个元素做累积计算，其效果就是：

[x1, x2, x3, x4].reduce(f) = f(f(f(x1, x2), x3), x4)


举个例子吧，不然你也看不懂:


var arr=[1,6,46,2,4];


var add =arr.reduce(function (x,y){
    return x+y;
});

console.log(add);


### 练习

利用reduce()求积：
```
'use strict';

function product(arr) {
   var result= arr.reduce(function(x,y){
      return x*y;  
   });
 
   return result;

}
// 测试:
if (product([1, 2, 3, 4]) === 24 && product([0, 1, 2]) === 0 && product([99, 88, 77, 66]) === 44274384) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}
 ```


练习：不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数：
```
'use strict';

function string2int(s) {
    var arr = s.split('');
    var num = arr.map(function (x){
        return  x-'0';
});
    var sum = num.reduce(function (x,y){
        return x*10 + y;
});
  return sum;
}
 
// 测试:
if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
    if (string2int.toString().indexOf('parseInt') !== -1) {
        console.log('请勿使用parseInt()!');
    } else if (string2int.toString().indexOf('Number') !== -1) {
        console.log('请勿使用Number()!');
    } else {
        console.log('测试通过!');
    }
}
else {
    console.log('测试失败!');
}
```
练习
请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。
```
'use strict';

function normalize(arr) {
   return arr.map(function(s){
		return s.slice(0,1).toUpperCase()+s.substring(1).toLowerCase();
	})


}

// 测试:
if (normalize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}
``` 
小明希望利用map()把字符串变成整数，他写的代码很简洁：
```
'use strict';

var arr = ['1', '2', '3'];
var r;
r = arr.map(Number);

console.log(r);
```


第二种:

    var arr = ['1', '2', '3'];
    var r = arr.map(function (x,y){
        return x%10 ;
    });
    

    console.log(r);
    
结果竟然是1, NaN, NaN，小明百思不得其解，请帮他找到原因并修正代码。


## filter

+ filter也是一个常用的操作，它用于把Array的某些元素过滤掉，然后返回剩下的元素。
+ 和map()类似，Array的filter()也接收一个函数。和map()不同的是，filter()把传入的函数依次作用于每个元素，然后根据返回值是true还是false决定保留还是丢弃该元素。


例子：

    var arr = [1, 2, 4, 5, 6, 9, 10, 15];
    var r = arr.filter(function (x) {
        return x % 2 !== 0;
    });
    r; // [1, 5, 9, 15]


    把一个Array中的空字符串删掉，可以这么写：
```
var arr = ['A', '', 'B', null, undefined, 'C', '  '];
var r = arr.filter(function (s) {
    return s && s.trim(); // 注意：IE9以下的版本没有trim()方法
});
r; // ['A', 'B', 'C']
```
可见用filter()这个高阶函数，关键在于正确实现一个“筛选”函数。


### 回调函数


filter()接收的回调函数，其实可以有多个参数。通常我们仅使用第一个参数，表示Array的某个元素。回调函数还可以接收另外两个参数，表示元素的位置和数组本身：
```
var arr = ['A', 'B', 'C'];
var r = arr.filter(function (element, index, self) {
    console.log(element); // 依次打印'A', 'B', 'C'
    console.log(index); // 依次打印0, 1, 2
    console.log(self); // self就是变量arr
    return true;
});
```
利用filter，可以巧妙地去除Array的重复元素：
```
'use strict';

var
    r,
    arr = ['apple', 'strawberry', 'banana', 'pear', 'apple', 'orange', 'orange', 'strawberry'];
r = arr.filter(function (element, index, self) {
    return self.indexOf(element) === index;
});

console.log(r.toString());
``` 
去除重复元素依靠的是indexOf总是返回第一个元素的位置，后续的重复元素位置与indexOf返回的位置不相等，因此被filter滤掉了。

    function get_primes(arr) {

    return    arr.filter(function(element,index,self){
            if(element<=1) return false;
            var flag = true;
            for(let i=2;i<element;i++){
                if(element%i===0) {flag = false;break;}
            }
            return flag;
            
    });

    }

    // 测试:
    var
        x,
        r,
        arr = [];
    for (x = 1; x < 100; x++) {
        arr.push(x);
    }
    r = get_primes(arr);
    if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
        console.log('测试通过!');
    } else {
        console.log('测试失败: ' + r.toString());
    }


