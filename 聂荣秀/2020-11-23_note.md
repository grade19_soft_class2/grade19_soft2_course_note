 # JavaScript笔记
+ JS是弱类型语言,不能用int；
+ JS是解释型的编程语言可以不用准确指定数据类型，不区分整数和浮点数,常用var let ,统一用Number类型；
+ JS缺点无法判断方法还是语句；
+ 快捷键 "Ctrl+/"多行注释；
+ 大小写敏感，注意大小写；
+ % 求余，！取反；
+ "==="类型和值都要相等；
+ undefined表示未定义：
```
var i;//undenfined
console.log(i);
var i=null;//null
console.log(i);
```