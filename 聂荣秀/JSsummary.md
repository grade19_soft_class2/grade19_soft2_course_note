## 数组
+ push():  把里面的内容添加到数组末尾，并返回修改后的长度。
+ pop()：移除数组最后一项，返回移除的那个值，减少数组的length。
+ some()：判断数组中是否存在满足条件的项，只要有一项满足条件，就会返回true。
+ indexOf()：接收两个参数：要查找的项和（可选的）表示查找起点位置的索引;其中， 从数组的开头（位置 0）开始向后查找。
+ slice()截取Array的部分元素，然后返回一个新的Array;
+ unshift()往Array的头部添加若干元素;
+ shift()把Array的第一个元素删掉;
+ sort()可以对当前Array进行排序，它会直接修改当前Array的元素位置，直接调用时，按照从小到大排序。
+ reverse()把整个Array的元素给调个个,反转;
+ splice()是修改Array的“万能方法”，它可以从指定的索引开始删除若干元素，然后再从该位置添加若干元素;
+ concat()把当前的Array和另一个Array连接起来，并返回一个新的Array;
+ join()是一个非常实用的方法，它把当前Array的每个元素都用指定的字符串连接起来，然后返回连接后的字符串;
+ Map()是一组键值对的结构，具有极快的查找速度

```
 var arr = ["Lily", "lucy", "Tom", "Tony", "Lihua"];
        arr.length;
        console.log(arr);
        var count = arr.push("Jack", "Sean");
        console.log(count);
        console.log(arr);
        var item = arr.pop();
        console.log(item);
        console.log(arr);

        var arr1= [1, 2, 3, 4, 5];
        var arr2 = arr1.some(function (x) {
            return x < 3;
        });
        console.log(arr2);
        console.log(arr1.sort());
        console.log(arr1.indexOf(3));
```
### 多维数组
```
var arr = [[1, 2, 3], [4, 5, 6]];
arr[0][0] ;
arr[1][0] ;
console.log(arr);
```

####  练习
请尝试for循环和while循环，并以正序、倒序两种方式遍历。请利用循环遍历数组中的每个名字，并显示Hello, xxx!：

```
'use strict';
var arr = ['Bart', 'Lisa', 'Adam'];
var i, x;
for (i=0; i<arr.length; i++) {
    x = arr[i];
    console.log(`Hello,${x}`);
}
```
#### 练习：在新生欢迎会上，你已经拿到了新同学的名单，请排序后显示：欢迎XXX，XXX，XXX和XXX同学！：
```
`use strict;`
var arr = ['小明', '小红', '大军', '阿黄'];
arr.sort();
arr.join(',');
console.log('欢迎' + arr.slice(0, 3) + '和' + arr[3] + '同学')


var arr = ['小明', '小红', '大军', '阿黄'];
function SayHello(arr) {
    let str = '';
    for (let i = 0; i < arr.length; i++) {
        if (i === arr.length - 1) {
          console.log( str += `和${arr[i]}同学！`);
        } else if (i === 0) {
          console.log(str += `欢迎${arr[i]}`);
        } else {
            console.log(str += `,${arr[i]}`);
        }
    }
    return str;
    console.log(str);
}
```
## 函数
+ arguments使用方式跟数组一样，有length属性，可以通过arguments[]来获取函数传入的参数值，但不是数组，在箭头函数里是不能使用的。
+ rest参数可以把函数里多余的参数用数组表示出来，写法"...rest"。

#### 练习
请用rest参数编写一个sum()函数，接收任意个参数并返回它们的和：
```
'use strict';
function sum(...rest) {
    var result = 0;
    for (var value of rest) {
        result += value;
    };
    return result;
}
// 测试:
var i, args = [];
for (i = 1; i <= 100; i++) {
    args.push(i);
}
if (sum() !== 0) {
    console.log('测试失败: sum() = ' + sum());
} else if (sum(1) !== 1) {
    console.log('测试失败: sum(1) = ' + sum(1));
} else if (sum(2, 3) !== 5) {
    console.log('测试失败: sum(2, 3) = ' + sum(2, 3));
} else if (sum.apply(null, args) !== 5050) {
    console.log('测试失败: sum(1, 2, 3, ..., 100) = ' + sum.apply(null, args));
} else {
    console.log('测试通过!');
}
```
定义一个计算圆面积的函数area_of_circle()，它有两个参数：
r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14
```
'use strict';
function area_of_circle(r, pi) {
    var S=3.14;
    if(arguments.length>1){
        S=pi;
    }
    return S*r*r;
}
// 测试:
if (area_of_circle(2) === 12.56 && area_of_circle(2, 3.1416) === 12.5664) {
    console.log('测试通过');
} else {
    console.log('测试失败');
}
```
## 高阶函数
可以接收别的函数里的参数的函数叫做高阶函数。

```
function fun(x,y,z) {
    return z( x)*z(y);
}
var arr=fun(-2,2,Math.abs);//math.abs为系统里的函数,相当于绝对值
console.log(arr);
```
+ map()对数组里的每一个元素执行操作,返回一个新的数组;
+ reduce()对数组里的所有元素执行操作,返回一个新的元素;
+ filter()筛选掉重复的元素，返回一个新的数组;
+ sort()也是一个高阶函数，它还可以接收一个比较函数来实现自定义的排序;
+ every()可以判断数组的所有元素是否满足测试条件;
+ find()用于查找符合条件的第一个元素，如果找到了，返回这个元素，否则，返回undefined
+ findIndex()用于查找符合条件的第一个元素,返回这个元素的索引，如果没有找到，返回-1;
+ forEach()把每个元素依次作用于传入的函数，但不会返回新的数组,常用于遍历数组，因此，传入的函数不需要返回值
### 回调函数
练习
请尝试用filter()筛选出素数：
```
'use strict';

function get_primes(arr) {
    return arr.filter(function isPrime(number) {
        if (typeof number !== 'number' || number < 2) {
            // 不是数字或者数字小于2
            return false;
        }if (number === 2) {//2是质数
            return true;
        } else if (number % 2 === 0) {//排除偶数
            return false;
        }
        var squareRoot = Math.sqrt(number);//返回数字的平方根。

        //因为2已经验证过，所以从3开始；且已经排除偶数，所以每次加2
        for (var i = 3; i <= squareRoot; i += 2) {
            if (number % i === 0) {
                return false;
            }
        }
        return true;
    })
}

// 测试:
var
    x,
    r,
    arr = [];
for (x = 1; x < 100; x++) {
    arr.push(x);
}
r = get_primes(arr);
if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
    console.log('测试通过!');
} else {
    console.log('测试失败: ' + r.toString());
}
``` 
### generator

练习:要生成一个自增的ID，可以编写一个next_id()函数：
```
'use strict'

var current_id = 0;
function next_id() {
     current_id ++;
     return current_id;
 }
 console.log(next_id());
 console.log(next_id());
 console.log(next_id());
 console.log(next_id());
 console.log(next_id()); 
 ```
 由于函数无法保存状态，故需要一个全局变量current_id来保存数字。
 不用闭包，试用generator改写：
 ```
function* next_id() {
    var current_id = 0;
    
    while (pass) {
        current_id++;
        yield current_id;
    }
}
var  g = next_id();
for(var i=0;i<5;i++){
    console.log(g.next().value);
}
// 测试:
var
    x,
    pass = true,
    g = next_id();
for (x = 1; x < 100; x ++) {
    if (g.next().value !== x) {
        pass = false;
        console.log('测试失败!');
        break;
    }
}
if (pass) {
    console.log('测试通过!');
}
```