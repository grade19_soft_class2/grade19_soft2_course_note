## 全局作用域 若当前作用域没找到会向上 window内部寻找 window是系统自带的全局变量想alert 悬浮窗口提示 等
1.  return;结束语句后面的语句不执行
2. 函数的两种定义方法的顶部变量也可以作为全局变量如:
```
'use strict'

var abc=function(){
    console.log('打印一个字');
}



function aa(){
    console.log('打印一个字母');
}



另外一个js文件：

'use strict'



window.abc();//window可以省略 更好理解与调用的是window的全局变量

window.aa();

结果：打印一个字  
    打印一个字母

```
3. 若在方法内部在嵌套一个方法只有最外面那个方法可以作为全局变量被调用
```
'use strict'

var abc=function(x){
    console.log('打印一个字');
    function aa(){
        console.log('打印一个字母');
    }
    aa();
    console.log(x);
}

abc(6);


另外一个js文件：

'use strict'


abc();

aa();

aa会提示undefined并报错
abc作为全局变量会被调用 也可以写作 window.abc();

```
4. 通过赋值方法替换掉window属性对象
```
'use strict'

var appTop=window.alert;

var alert=function (x){
    console.log(x);
}

alert(6);


window.alert=appTop;

alert(6);

结果 6 
    悬浮框提示 6
```

# 名字空间 创建一个新的全局变量并与其它的全局变量互不干扰 若不创建新的全局变量所有定义的变量都默认为window对象的一个属性
```
'use strict'

var app={};

app.name="名字";

app.age=18;

console.log(app.age);


另外一个js文件：

    console.log(app.name);

    结果 18 
        名字

```

# 局部定义域   只能在当前的一个循环体内部使用 不能再方法体内部进行调用  称他们这种为局部定义块 
1. var 与let 区别
2. 常量定义 const 也为局部变量
```
function a(){
    for(var i=0;i<10;i++){

    }
    console.log(i);
}

a();/10


function a(x){
    for(let i=0;i<10;i++){
        console.log(i);
    }
    // console.log(i);
    console.log(x);
}

a(6);// 0 1 2 3 4 5 6 7 8 9 
    123行 undifined  
    注意 只要有一行报错下面的代码均不执行跟return有点类似


    function a(x){
    for(let i=0;i<1;i++){
        console.log(i);
        const PI=3.14;
        console.log(PI);
    }
    // console.log(i);
    //console.log(PI);
    console.log(x);
}

a(6);// 结果0  
    3.14
        6
     
```