# sort 排序
1. 数组的简单排序 
```
'use strict'

var arr=[1,5,22,2,11,14,23];


console.log(arr.sort());/1, 11, 14, 2, 22, 23, 5//(排列的方式按照字符串排列顺序)
```
2. 数组的从小到大 和倒序
```
var arr=[1,5,22,2,11,14,23];

function ab(x,y){
    if(x>y){
        return 1;
    }
    if(x==y){
        return 0;
    }
    if(x<y){
        return -1;
    }
}
console.log(arr.sort(ab));

console.log(arr.reverse());//快速倒序或者修改方法体

```

```
var arr=[1,5,22,2,11,14,23];

function ab(x,y){
      if(x<y){
        return 1;
    }
    if(x==y){
        return 0;
    }
      if(x>y){
        return -1;
    }
  
}
console.log(arr.sort(ab));
```
# every() 对于数组的每个元素进行条件判断 每个条件都满足才会返回true反之false;
```
var arr=[1,5,22,2,11,14,23];

function ab(x){
    return x>0;
}

console.log(arr.every(ab));//true

var arr=['Apple','ac','c'];


console.log(arr.every(function (x){
    return x.toLowerCase()===x;
}));//false

console.log(arr.find(function (y){
    return y.toLowerCase()===y
}));//ac
```
# find() 只返回符合条件的第一个元素
```
var arr=[1,5,22,2,11,14,23];

function ab(x){
    return x>0;
}

console.log(arr.find(ab));//1
```
# findIndex() 只返回符合条件的第一个元素的索引坐标
```
var arr=[1,5,22,2,11,14,23];

function ab(x){
    return x>10;
}

console.log(arr.findIndex(ab));// 2
```
# foreach() 遍历数组 不需要返回值
```
var arr=['Apple','ac','c'];


arr.forEach(console.log);//'Apple','ac','c'
```

# 箭头函数 常用于嵌套函数 方法的缩写
```
无参数

x=>x*x ==   function (x){ return x*x}


有参数的情况下 不可以省略function{} 和ruturn

(x,y,...rest)=>function{
    return x+y+rest;
}

对象的箭头函数缩写

x=> ({pig:age}) //(调用对象里面的属性值)  () 用这个方便识别对象
```

# this 在之前的嵌套函数中this 只能调用当前作用域的函数属性值或者通过 赋值来承接这个对象的属性值 但是箭头函数可以根据上线文的词法作用域进行调用 
1.   注意：打印方法或者返回方法都要加()  而对象则是要({}) 要跟参数形式一一对应  

2.   注意:方法的输出形式  无参有返回值可以直接调用函数否则返回undefined 

```
var add={
    age:23,
    name: function (){
    
        var a=this.age;
        var that=this;
          var ab=function (){
          return that.age;//undenfined或者寻找window中这样的属性值   
        };
 
       return ab();
      
}
    }

    console.log(add.name());






var add={
    age:23,
    name:function (){
    var a=this.age;
     var b=()=>{
        return  this.age;
     }   
    return b();
 }
}

console.log(add.name());

```
3. 练习
```
请使用箭头函数简化排序时传入的函数：

'use strict'
var arr = [10, 20, 1, 2];

console.log(arr.sort((x,y)=>{
    if(x>y){
        return 1;
    }
    if(x===y){
        return 0;
    }
    if(x<y){
        return -1;
    }

}));// [1, 2, 10, 20]
```

