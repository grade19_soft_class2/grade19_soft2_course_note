# Dom的插入 Js 获取节点后利用docement调用更改HTML   父节点直接利用函数 直接输入节点 不用'节点'
```
1. appendChild 通过删除原有的位置的元素 并将该元素插入到父类节点的最后一个位置

JS:

var js=document.getElementById('js');

var list=document.querySelector('#list');

list.appendChild(js);

HTML:

<p id="js">JavaScript</p>
    <div id="list">
        <p id="java">Java</p>
        <p id="python">Python</p>
        <p id="scheme">Scheme</p>
    </div>

结果如下
Java

Python

Scheme

JavaScript
```
```

2. 通过新建元素再通过appendChild插入
Js:
var p=document.createElement('p');

var list=document.querySelector('#list');

p.id='big';

p.innerText='Big';

list.appendChild(p);

HTML:                                           

<p id="js">JavaScript</p>
    <div id="list">
        <p id="java">Java</p>
        <p id="python">Python</p>
        <p id="scheme">Scheme</p>
    </div>

结果如下
JavaScript

Java

Python

Scheme

Big
```
```
3. 通过新建元素插入到指定元素之后 parentElement.insertBefore(newElement, referenceElement);

Js:

var p=document.createElement('p');//新建元素

var list=document.querySelector('#list')//获取父类节点

var java=document.querySelector('#java')//获取子节点

p.id='small';

p.innerText='Small';

list.insertBefore(p,java);

HTML:

<p id="js">JavaScript</p>
    <div id="list">
        <p id="java">Java</p>
        <p id="python">Python</p>
        <p id="scheme">Scheme</p>
    </div>

result:

JavaScript

Java

Haskell

Python

Scheme
```

# Dom 删除节点
```
JS：

var happy=document.getElementById('happy');//获取节点

var parent=happy.parentElement;//获取子节点的父节点

var remove=parent.removeChild(happy);//利用removeChild() 移除父节点内部的子节点 但是并不代表没有了 只是重父节点移除 而且他内部实时更新 删除之后查询内部元素会减少

HTML:
    <p id="js">JavaScript</p>
    <div id="list">
        <p id="java">Java</p>
        <p id="python">Python</p>
        <p id="scheme">Scheme</p>
        <p id="happy">我的快乐 你的面具</p>
    </div>

JavaScript

Java

Python

Scheme
```
