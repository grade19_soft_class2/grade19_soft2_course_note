### 1. 通过递归的方式输出斐波那契函数
```
 Console.Write("请输入一个数展示斐波那契函数集合:");
            double a = double.Parse(Console.ReadLine());

            List<int> per = new List<int>();

            for (int i = 1; i <= a; i++)
            {
                var result = Factorial.recursion(i);
                per.Add(result);
            }

            Print(per);

             static  void  Print(List<int> newlist)
            {
                foreach (var item in newlist)
                {
                    Console.Write(item + " ");
                }
                Console.WriteLine();
                
            }
``` 
### 2. 通过输入的值来展示 Celling 和 Floor的区别
```

            //2.  ceiling 有余数直接进一取整数 返回大于或等于指定的双精度浮点数的最小整数值.//
            Console.Write("请输入第一个Ceiling数：");

            double num1 = double.Parse(Console.ReadLine());

            Console.WriteLine(Math.Ceiling(num1));

            //floor 有余数直接取整数  返回小于或等于指定的双精度浮点数的最大整数值.//
            Console.Write("请输入第一个Floor数：");

            double num2 = double.Parse(Console.ReadLine());

            Console.WriteLine(Math.Floor(num2));
```
### 3. 给出任意的日期值，计算当月第一天、当月最后一天是多少号，并且计算给出日期值是当月第几周.
```
//获取现在时间
            DateTime dt = DateTime.Now;
           Console.WriteLine(dt.ToShortDateString());

            //自定义时间
            //DateTime dt1 = new DateTime(2020, 2, 10);
            //Console.WriteLine(dt1.ToShortDateString());

            //获取当月的第一天
            DateTime dt2 = new DateTime(dt.Year, dt.Month, 1);
            Console.WriteLine(dt2.ToShortDateString()); 
            
            //获取当月的最后一天

            Console.WriteLine(dt2.AddMonths(1).AddDays(-1).ToShortDateString());

            //输出当月的第几周
            double a = dt.Day;
            Console.WriteLine(Math.Ceiling(a/7));
```