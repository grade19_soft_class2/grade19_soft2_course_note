# 闭包 保存return 的参数和数组值等
```
var arr=[1,3,7,9];


function sum(arr){
    var fn=function(){
       return arr.reduce(function (x,y){
            return x+y;
        });
    };
    return fn;
 
}

var f1=sum(arr);
var f2=sum(arr);

console.log(f1===f2);//false(每次调用这个方法结果相同到是每次调用的都是一个新的函数)


console.log(f1());
```

```
var 命名 定义的全局变量可以在方法体内部都能调用所以当for循环执行停止后i=4 而返回数组arr时会调用arr.push里面的方法 所以i=4被执行了三次传入数组 但是用let时局部变量只能在for循环内部调用 外部无法调用i 只能到内部去找i的值 所以会根据i的三次循环值调用方法体调用arr.push方法体 而执行传入数组

function add(){
    var arr=[];
    for(let i=1;i<=3;i++){
        arr.push(function (){
            return i*i;
        })
    }

    return arr;
}

var n=add();

console.log(add());

console.log(n[0]());//1
console.log(n[1]());//4
console.log(n[2]());//9


function add(){
    var arr=[];
    for(var i=1;i<=3;i++){
        arr.push(function (){
            return i*i;
        })
    }

    return arr;
}

var n=add();

console.log(add());

console.log(n[0]());//16
console.log(n[1]());//16
console.log(n[2]());//16


```
# 返回值函数尽量不要用for循环变量或者后续会改变的量 若要使用则再创建一个函数，用该函数的参数绑定循环变量当前的值

```
function sum(){
    var arr=[];
    for(var i=1;i<=3;i++){
        arr.push((function(n){
            return function(){
                return n*n;
            }
        })(i));
    }

    return arr;
}

var newarr=sum();

console.log(newarr[0]());//1
console.log(newarr[1]());//4
console.log(newarr[2]());//9
```


