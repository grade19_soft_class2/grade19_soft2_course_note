 尽量让<script src="xxxx"></script> 在body后面放置 以防出错

# JQuery
```
使用jQuery只需要在页面的  引入jQuery文件即可： <script src="xxxx"></script>加上引入Jquery文件

$是JQuery的简称 而jQuery把所有功能全部封装在一个全局变量jQuery中
```
# 选择器

通过ID查找<p id='new'>
```
var a =$('#new');
```
通过Class查找<p class='pig'><p class='dog'>
```
var class=$('.pig');//包含class='pig'都将返回等同于模糊查找将class名称中含有pig的结果都返回

var class1=$('.dog.pig')//将class的名称中含有dog和pig的名称返回
```
通过tag查找<p>
```
var p=$('p')//查找出所有<p>tag
```

 属性查找 通过class，且不受class包含多个名称的影响：

```
var a=$('[class=java]');//只会返回[class=java]

var b=$('[pig='a b']')//只会返回[pig='a b']

var c=$('[class=$java]');//只返回以java结尾的如 class=aasdfadjava

var d=$('[class=^java]');//只返回以java开头的如 class=javasadfdsaf

```
组合查找
```
var e=$('p[class=java]')//只会返回<p class='java'>不会返回其他tag像<div class='java'>
```
多项选择器 即多个选择器一起使用
``` 
var e=$('p[class=java]')//只会返回<p class='java'>不会返回其他tag像<div class='java'>

var f=$('p[class=java],div#new')//将含有class=java都返回<p class='java'>和<div id='new'>id含有new的都返回
```

# 层级选择器 层级之间用空格隔开
```
HTML:
<div class="a">
    <ul class="b">
        <li class="lang-javascript">JavaScript</li>
        <li class="lang-python">Python</li>
        <li class="lang-lua">Lua</li>
    </ul>
</div>
JS:
$('ul.b li.lang-javascript'); // [<li class="lang-javascript">JavaScript</li>]
$('div.a li.lang-javascript'); // [<li class="lang-javascript">JavaScript</li>]

由于div和ul都是 li 的父节点所以导致结果一致

如$('div p input[name=qq]')返回的是<div> <p> 中的<input>
     
```
# 子选择器 只能用于父节点于直系子节点 父节点与子节点之间用 > 连接
```
HTML:
<div class="a">
    <ul class="b">
        <li class="lang-javascript">JavaScript</li>
        <li class="lang-python">Python</li>
        <li class="lang-lua">Lua</li>
    </ul>
</div>
JS:

$('ul.b>li.lang-javascript'); // [<li class="lang-javascript">JavaScript</li>]

$('div.a>li.lang-javascript'); // []

```
# 过滤器
```
$('ul.lang li'); // 选出JavaScript、Python和Lua 3个节点

$('ul.lang li:first-child'); // 仅选出JavaScript
$('ul.lang li:last-child'); // 仅选出Lua
$('ul.lang li:nth-child(2)'); // 选出第N个元素，N从1开始
$('ul.lang li:nth-child(even)'); // 选出序号为偶数的元素
$('ul.lang li:nth-child(odd)'); // 选出序号为奇数的元素
```
# 表单相关结构
```
:input：可以选择<input>，<textarea>，<select>和<button>；

:file：可以选择<input type="file">，和input[type=file]一样；

:checkbox：可以选择复选框，和input[type=checkbox]一样；

:radio：可以选择单选框，和input[type=radio]一样；

:focus：可以选择当前输入焦点的元素，例如把光标放到一个<input>上，用$('input:focus')就可以选出；

:checked：选择当前勾上的单选框和复选框，用这个选择器可以立刻获得用户选择的项目，如$('input[type=radio]:checked')；

:enabled：可以选择可以正常输入的<input>、<select> 等，也就是没有灰掉的输入；

:disabled：和:enabled正好相反，选择那些不能输入的。

$('div:visible'); // 所有可见的div

$('div:hidden'); // 所有隐藏的div
```
# 查找 find()  parent() next() prev()
```
<!-- HTML结构 -->
<ul class="lang">
    <li class="js dy">JavaScript</li>
    <li class="dy">Python</li>
    <li id="swift">Swift</li>
    <li class="dy">Scheme</li>
    <li name="haskell">Haskell</li>
</ul>
JS:
var ul = $('ul.lang'); // 获得<ul>
var dy = ul.find('.dy'); // 获得JavaScript, Python, Scheme
var swf = ul.find('#swift'); // 获得Swift
var hsk = ul.find('[name=haskell]'); // 获得Haskell

也可以返回去查找通过parent

var dy=$('#swift')获取id="swift"

var ul=dy.parent();获取id的父节点ul

位于同一层级的节点，可以通过next()和prev()方法

var dy=$('#swift')获取id="swift"

dy.next();//Scheme

swift.next('[name=haskell]'); // 空的jQuery对象

swift.prev(); // Python

swift.prev('.dy'); // Python，因为Python同时符合过滤器条件.dy同时满足 swift上面的节点 且class='dy' 
```
# 过滤 filter() map() slice(a,b)a-b之间的元素
```
var langs = $('ul.lang li'); // 拿到JavaScript, Python, Swift, Scheme和Haskell
var a = langs.filter('.dy'); // 拿到JavaScript, Python, Scheme


var langs = $('ul.lang li'); // 拿到JavaScript, Python, Swift, Scheme和Haskell
langs.filter(function () {
    return this.innerHTML.indexOf('S') === 0; // 返回S开头的节点
}); // 拿到Swift, Scheme
///this是langs内部对象

map()方法把一个jQuery对象包含的若干DOM节点转化为其他对象：

var langs = $('ul.lang li'); // 拿到JavaScript, Python, Swift, Scheme和Haskell

var arr = langs.map(function () {
    return this.innerHTML;
}).get(); // 用get()拿到包含string的Array：['JavaScript', 'Python', 'Swift', 'Scheme', 'Haskell']
```
# 修改Text和HTML
```
<!-- HTML结构 -->
<ul id="test-ul">
    <li class="js">JavaScript</li>
    <li name="book">Java &amp; JavaScript</li>
</ul>
JS:
var a =$('ul#test-ul li.js')
var b=$('ul#test-ul li[name=book]')
a.text('a')//a

b.html('<span style="color: red">JavaScript</span>')//字体颜色变红

```
# 修改Css jQuery对象的css('name', 'value')方法

```
var div = $('#test-div');
div.css('color'); // '#000033', 获取CSS属性
div.css('color', '#336699'); // 设置CSS属性
div.css('color', ''); // 清除CSS属性

var div = $('#test-div');
div.hasClass('highlight'); // false， class是否包含highlight
div.addClass('highlight'); // 添加highlight这个class
div.removeClass('highlight'); // 删除highlight这个class
```
# 添加Dom  append() 和删除Dom remove() 与之前相比省去了一个child
```
Html:
<div id="test-div">
    <ul>
        <li><span>JavaScript</span></li>
        <li><span>Python</span></li>
        <li><span>Swift</span></li>
    </ul>
</div>
JS:

var ul = $('#test-div>ul');//获取<ul>节点

ul.append('<li><span>Haskell</span></li>');//调用append()传入HTML片段

// 创建DOM对象:
var ps = document.createElement('li');
ps.innerHTML = '<span>Pascal</span>';
// 添加DOM对象:
ul.append(ps);

// 添加jQuery对象:
ul.append($('#scheme'));

append()把DOM添加到最后，prepend()则把DOM添加到最前。


如果要把新节点插入到指定位置，例如，JavaScript和Python之间，那么，可以先定位到JavaScript，然后用after()方法：

var js = $('#test-div>ul>li:first-child');
js.after('<li><span>Lua</span></li>');
也就是说，同级节点可以用after()或者before()方法。
```
# 事件绑定定义
```
var a=$('#tian');

a.click(()=>{
    alert("你好呀")
})

a.on("click",()=>{
    alert('怎么安排')
})
```

# 事件参数

$(function () {
    $('#testMouseMoveDiv').mousemove(function (e) {
        $('#testMouseMoveSpan').text('pageX = ' + e.pageX + ', pageY = ' + e.pageY);
    });
});//$(function) 是$(document).ready(function){}的简化

# 事件绑定
```
var input=$('input[type=text]');


input.change(function(){
    console.log('发生了改变');
});//绑定change()事件


input.val('change方法已改动');//直接修改事件内部的定义

input.change();//直接触发事件 相当于trigger();


```
# 事件绑定 off("click",function(){})//注意off无法移除已绑定的第一个事件需要将function给他一个非匿名函数

var input=$('input[type=text]');

function hello(){
    console.log('更改了内容');
}//

input.change(hello);

setTimeout(() => {
    input.off('change')//解除input绑定的所有change处理函数
    input.off()//解除input绑定的所有类型的事件
}, 1000);


# 浏览器安全限制 如window.open()
```
var button=$('button#btn1');

var button1=$('button#btn2');

function open(){
    window.open('/');
}
button.click(function(){
    open();//click事件的方法内部直接执行方法不会被拦截
})

button.click(function(){
    setTimeout(open,2000)//clickfunction内部又增方法体再进行调用会被拦截
})
```

```
练习
 对如下的Form表单：

<!-- HTML结构 -->
<form id="test-form" action="test">
    <legend>请选择想要学习的编程语言：</legend>
    <fieldset>
        <p><label class="selectAll"><input type="checkbox"> <span class="selectAll">全选</span><span class="deselectAll">全不选</span></label> <a href="#0" class="invertSelect">反选</a></p>
        <p><label><input type="checkbox" name="lang" value="javascript"> JavaScript</label></p>
        <p><label><input type="checkbox" name="lang" value="python"> Python</label></p>
        <p><label><input type="checkbox" name="lang" value="ruby"> Ruby</label></p>
        <p><label><input type="checkbox" name="lang" value="haskell"> Haskell</label></p>
        <p><label><input type="checkbox" name="lang" value="scheme"> Scheme</label></p>
        <p><button type="submit">Submit</button></p>
    </fieldset>
</form>
 绑定合适的事件处理函数，实现以下逻辑：

 当用户勾上“全选”时，自动选中所有语言，并把“全选”变成“全不选”；

 当用户去掉“全不选”时，自动不选中所有语言；

 当用户点击“反选”时，自动把所有语言状态反转（选中的变为未选，未选的变为选中）；

 当用户把所有语言都手动勾上时，“全选”被自动勾上，并变为“全不选”；

 当用户手动去掉选中至少一种语言时，“全不选”自动被去掉选中，并变为“全选”。

 
 

 js代码

 

'use strict'
 
$(function(){
  var
    form = $('#test-form'),
    langs = form.find('[name=lang]'),
    selectAll = form.find('label.selectAll :checkbox'),
    selectAllLabel = form.find('label.selectAll span.selectAll'),
    deselectAllLabel = form.find('label.selectAll span.deselectAll'),
    invertSelect = form.find('a.invertSelect');
 
// 重置初始化状态:
form.find('*').show().off();
form.find(':checkbox').prop('checked', false).off();
deselectAllLabel.hide();
// 拦截form提交事件:
form.off().submit(function (e) {
    e.preventDefault();
    alert(form.serialize());
});
   
   selectAll.change(function(){
    if(this.checked){
      langs.prop('checked',true);
      deselectAllLabel.show();
      selectAllLabel.hide();
    }else{
      langs.prop('checked',false);
      deselectAllLabel.hide();
      selectAllLabel.show();
    }
   })
 
   invertSelect.click(function(){
    langs.each(function(){
      if(this.checked){
        $(this).prop('checked',false);
      }else{
        this.checked = true;
      }
    })
   })
 
   //alert(langs.length);
   var count = 0,
       len = langs.length;
   langs.click(function(){
    count = 0;
    langs.each(function(){
      if(this.checked)
        count++;
    })
    if(count == len){
      selectAll.prop('checked',true);
      deselectAllLabel.show();
      selectAllLabel.hide();
    }else{
        selectAllLabel.show();
        deselectAllLabel.hide();
        selectAll.prop('checked',false);
    }
   })
})

```

