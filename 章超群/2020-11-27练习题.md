1.找出元素 item 在给定数组 arr 中的位置
```
//方法一

function indexOfFn(arr, item) {
    if (arr.indexOf(item) > 0) {
        return arr.indexOf(item)
    }else{
        return -1
    }
}
console.log(indexOfFn([ 1, 2, 3, 4 ], 3));  


var arr = ['小明', '小红', '大军', '阿黄'];
console.log('欢迎'+arr.slice(0,arr.length-1).join('、')+'和'+arr[arr.length-1]+'同学')
//方法二

function indexOf(arr, item) {
    if (Array.prototype.indexOf){// 判断浏览器是否支持indexOf方法
        return arr.indexOf(item);
    } else {
        for (var i = 0; i < arr.length; i++){
            if (arr[i] === item){
                return i;
            }
        }
    }     
    return -1;
  }

  console.log(indexOfFn([ 1, 2, 3, 4 ], 3));
//方法三

  function indexOf(arr, item) {
    flag=true;
    for(var i in arr){
        if(arr[i]===item){
            flag=false;
            return i;  
        }
    }
    if(flag){  
        return -1;
    }
}
console.log(indexOfFn([ 1, 2, 3, 4 ], 3));

```
2.计算给定数组 arr 中所有元素的总和
```
//方法一

var arr = [1,2,3,4];
function sum(arr){
    var totel = 0;
    for(i=0;i<arr.length;i++){
        totel += arr[i];
    }
    console.log(totel);
    return totel;
}
 sum(arr)
//方法二

function sum1(arr) {
    var sum = 0;
    arr.forEach(function(item,index){
    sum = sum + item;
})
return sum
}

console.log(sum1(arr));
//方法三

function sumFn(arr) {
    var sum = 0;
    arr.forEach((item) => {
        sum += item;
    })
    return sum;
}
console.log(sumFn([ 1, 2, 3, 4 ]));
//方法四递归

function sum2(arr) {
    var len = arr.length;
    if(len == 0){
      return 0;
    } else if (len == 1){
      return arr[0];
    } else {
      return arr[0] + sum2(arr.slice(1));
    }
  }
  console.log(sum2(arr))
//方法五

  function sum3(arr) {
    return arr.reduce(function(a, b, c, d){
      return a + b;
    });
  }
  console.log(sum3(arr))
//方法六

  function sum4(arr) {
    return eval(arr.join("+"));
  };

  console.log(sum4(arr))

  ```
3.移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
```
//方法一

  function remove(arr, item) {
    return arr.filter(function(value) {
        return value !== item     
    })
}
console.log(remove([1, 2, 3, 4, 2], 2))
//方法二

function remove1(arr, item) {
    var a = []
    for(var i=0; i<arr.length; i++) {
        if(arr[i] !== item) {
            a.push(arr[i])
        }
    }
    return a
}
console.log(remove1([1, 2, 3, 4, 2], 2))
//方法三

function remove2(arr, item) {
    var a = arr.slice(0)
    //不能用 var a = arr,  因为a和arr引用的是同一个地址，改变a的值，arr也会改变
    for(var i=0; i<a.length; i++) {
        if(a[i] === item) {
            a.splice(i, 1)
            i--;
        }
    }
    
    return a
}
console.log(remove2([1, 2, 3, 4, 2], 2))
//方法四

function remove3(arr, item) {
    var result = [];
    arr.forEach(function(arrItem){
        if(item != arrItem){
            result.push(arrItem);
        }
    })
    return result;
}
console.log(remove3([1, 2, 3, 4, 2], 2))
```
4.移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
```
//方法一

function removes(arr, item) {
    for(var i=0;i<arr.length;i++)
    {
    if(arr[i]==item)
    {
    arr.splice(i,1);
    i=0;
    }
    }
    return arr;
    }
    console.log(removes([1, 2, 2, 3, 4, 2, 2], 2))
//方法二
    function removes1(arr, item) {
        for(var i=arr.length;i>=0;i--)
        {
        if(arr[i]==item)
        {
        arr.splice(i,1);
        }
        }
        return arr;
        }

        console.log(removes1([1, 2, 2, 3, 4, 2, 2], 2))
//方法三

    function removes2(arr, item) {
        while(arr.indexOf(item) != -1){
            arr.splice(arr.indexOf(item),1)
        }
        return arr;
    }

    console.log(removes2([1, 2, 2, 3, 4, 2, 2], 2))
    ```
5.在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
```
//方法一
    function append(arr, item) {
        return arr.concat(item);
      }

      console.log(append([1, 2, 3, 4],  10))
  //方法二
      function append1(arr, item) {
        var arr2=new Array();
        for(var i=0;i<arr.length;i++){
            arr2.push(arr[i]);
        }
        arr2.push(item);
        return arr2;
     
    }
    console.log(append1([1, 2, 3, 4],  10))
//方法三

    function append2(arr, item) {
        var arr1 = arr.slice(0);
        arr1[arr1.length]=item;
        
        return arr1;
    }
    console.log(append2([1, 2, 3, 4],  10))
//方法四

    function append3(arr, item) {
    var newArr = JSON.parse(JSON.stringify(arr))
    newArr.push(item);
    return newArr;
}
console.log(append3([1, 2, 3, 4],  10))
```
6.删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
```
//方法一

function truncate(arr) {
    return arr.slice(0,arr.length-1)
  }
 console.log(truncate([1, 2, 3, 4]))  
//方法二（concat()）

function  truncate1(arr){
     
        var newarr=arr.concat();
        newarr.pop();
        return newarr;
}

console.log(truncate1([1, 2, 3, 4])) 
//方法三（迭代）

function truncate2(arr) {
    var a = new Array();
    for (var i = 0;i < arr.length-1;i++){
        a.push(arr[i]);
    }
    return a;
}
console.log(truncate2([1, 2, 3, 4])) 
//方法四

function truncate3(arr) {
    //val:当前元素值  i:当前元素索引  arr1:当前数组
    return arr.filter(function (val,i,arr1) {
        return i!=arr1.length-1;
    })
}

console.log(truncate3([1, 2, 3, 4]))
//方法五

function truncate4(arr) {
    var arr1 = arr.join().split(',');
     
    arr1.pop();
    return arr1;
}
console.log(truncate4([1, 2, 3, 4])) 
```
7.在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
```
//方法一：最普通的循环赋值

function prepend(arr, item) {
    var a = new Array();
    a.push(item);
    for (var i =0;i<arr.length;i++){
        a.push(arr[i]);
    }
    return a;
}

console.log(prepend([1, 2, 3, 4], 10))
//方法二：使用concat

function prepend1(arr, item) {
    return [item].concat(arr);
}
console.log(prepend1([1, 2, 3, 4], 10)) 
//方法三：使用push.apply

function prepend2(arr, item) {
    var arr1 = [item];
     
    [].push.apply(arr1,arr);
    return arr1;
}
console.log(prepend2([1, 2, 3, 4], 10))
//方法四：使用unshift

function prepend3(arr, item) {
    // var arr1 = arr.slice(0);
    var arr1 = arr.join().split(',')
    arr1.unshift(item);
    return arr1;
} 
console.log(prepend3([1, 2, 3, 4], 10))
```
8.删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
```
//方法一

function curtail(arr) {
    　var m = arr.slice(0);
      m.shift(arr[0]);
      return m;
    }
console.log(curtail([1, 2, 3, 4]))
//方法二

function curtail1(arr) {
    var m1 = arr.slice(1);
    return m1;
    }
    console.log(curtail1([1, 2, 3, 4]))
//方法三（filter过滤下标，返回满足不等0的下标的元素）

function curtail2(arr) {
    return arr.filter(function(a,b,arr){
    return b !== 0;
    });
    }

    console.log(curtail2([1, 2, 3, 4]))
//方法四

function curtail3(arr) {
    var m2 = arr.slice(0);
    m2.splice(0,1);
    return m2;
    }

    console.log(curtail3([1, 2, 3, 4]))
//方法五

function curtail4(arr) {
    var m3 = arr.join().split(',');
    m3.shift();
    return m3;
    }

    console.log(curtail4([1, 2, 3, 4]))
// 第六种：：apply数组参数化后放入m数组

function curtail5(arr) {
var m4 = [];
[].push.apply(m4,arr);
m4.shift();
return m4;
}
console.log(curtail5([1, 2, 3, 4]))
//第七种：：concat数组链接出新数组。

function curtail6(arr) {
var m5 = arr.concat();
m5.shift();
return m5;
}
console.log(curtail6([1, 2, 3, 4]))
```
9.合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
```
//方法一：使用concat

function concat(arr1, arr2) {
    // var arr = [];
    // arr = arr.concat(arr1);
    // arr = arr.concat(arr2);
    // return arr;

    return arr1.concat(arr2);
}

console.log(concat([1, 2, 3, 4], ['a', 'b', 'c', 1]))
//方法二：slice+concat

function concat1(arr1, arr2) {
    var arr = arr1.slice(0);
    arr = arr.concat(arr2);
    return arr;
}

console.log(concat1([1, 2, 3, 4], ['a', 'b', 'c', 1]))
//方法三：slice+push.apply

function concat2(arr1, arr2){
    var arr = arr1.slice(0);
    [].push.apply(arr,arr2);
    return arr;
}
console.log(concat2([1, 2, 3, 4], ['a', 'b', 'c', 1]))
//方法四

function concat3(arr1, arr2) {
    var currentArr1 = arr1.slice(0);
    var currentArr2 = arr2.slice(0);
    return currentArr1.concat(currentArr2)
}

console.log(concat3([1, 2, 3, 4], ['a', 'b', 'c', 1]))
```
10.在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
```
//方法一.利用slice+concat

function insert(arr, item, index) {
return arr.slice(0,index).concat(item,arr.slice(index));
}

console.log(insert([1, 2, 3, 4], 'z', 2))
//方法二.利用concat +splice function insert1(arr, item, index) { var newArr=arr.concat(); newArr.splice(index,0,item); return newArr; }

console.log(insert1([1, 2, 3, 4], 'z', 2))


//方法三.利用slice+splice
function insert2(arr, item, index) {
var newArr=arr.slice(0);
newArr.splice(index,0,item);
return newArr;
}

console.log(insert2([1, 2, 3, 4], 'z', 2))
//方法四。利用push.apply+splice

function insert3(arr, item, index) {
var newArr=[];
[].push.apply(newArr, arr);
newArr.splice(index,0,item);
return newArr;
}
console.log(insert3([1, 2, 3, 4], 'z', 2))
```
11.统计数组 arr 中值等于 item 的元素出现的次数
```
//方法一

function countFrequency(arr, item) {
    var s = 0;
    for(var i = 0; i<arr.length; i++){
        if(arr[i] == item){
            s++;  
        }    
    }
    return s;    
}
console.log(countFrequency([1, 2, 4, 4, 3, 4, 3], 4))
//方法二

function countFrequency1(arr, item) {
    var num = 0;
    arr.forEach(function(arrItem){
        if(item === arrItem){
            num ++;
        }
    })
    return num;
}

console.log(countFrequency1([1, 2, 4, 4, 3, 4, 3], 4))
```
12。找出数组 arr 中重复出现过的元素
```
//方法一

function duplicates(arr){
    var newArr = [];  //  不重复的元素存放
    var arrRepeat = [];  // 重复的元素存放
    arr.forEach((item)=>{
        if(newArr.indexOf(item) == -1){
            newArr.push(item);
    }else{
        arrRepeat.push(item);
    }
})
    return [...new Set(arrRepeat)].sort();
}
console.log(duplicates([1, 2, 4, 4, 3, 3, 1, 5, 3]));
//方法二

function duplicates2(arr){
    var obj = {};
    arr.forEach((item)=>{    // 将数组存入对象
        obj[item] = 0;
    })
    arr.forEach((item)=>{    //  对象的值增加
        for(var y in obj){
            if(y == item){
                ++obj[item]
            }
        }
    })
          
    var newArr = []   // 用来存储重复的元素
    for(var x in obj){
        if(obj[x] > 1){
            newArr.push(Number(x));
        }
    }
    return newArr
}
      
console.log(duplicates2([1, 2, 4, 4, 3, 3, 1, 5, 3]));
```
13.为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
```
//方法一：使用map方法

function square(arr) {
    return arr.map(function (ele) {
        return ele*ele;
    });
}
console.log(square([1, 2, 3, 4])) 
//方法二：for循环遍历

function square1(arr) {
   var arr1 = arr.slice(0);
   for (var i = 0;i<arr1.length; i++){
       arr1[i]=arr[i]*arr[i];
   }
   return arr1;
}        
console.log(square1([1, 2, 3, 4]))
//方法三：forEach

function square2(arr) {
    var a = [];
    arr.forEach(function (ele) {
        return a.push(ele*ele)
    })
    return a;
}    
console.log(square2([1, 2, 3, 4])) 
```
14.在数组 arr 中，查找值与 item 相等的元素出现的所有位置
```
//第一种方法

function findAllOccurrences(arr, target) {
    var result = [];
    for(var i =0;i<arr.length;i++){
        if(arr[i] === target){
            result.push(i);
        }
    }
    return result;
}

console.log(findAllOccurrences('abcdefabc'),'f')
//第二种方法：forEach

function findAllOccurrences1(arr, target) {
    var result = [];
    arr.forEach(function (item,index) {
        if (item===target) {
            result.push(index);
        }
    });
    return result;
}    
console.log(findAllOccurrences1('abcdefabc'),'f')
//第三种方法

function findAllOccurrences3(arr, target) {
    var result = [];
     
    var index = arr.lastIndexOf(target);
     
    while(index > -1) {
        result.push(index);
        arr.splice(index,1);
        index = arr.lastIndexOf(target);
    }
    return result;
} 

console.log(findAllOccurrences3('abcdefabc'),'f')
```